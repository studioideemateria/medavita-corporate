<?php

/*
	Template Name: COLUMN LEFT
*/

get_header();
get_header();
echo '<div class="page__wrap page__wrap--colLeft container">';
	include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/page-sidebar.php');
	include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/page-content.php');
echo '</div>';

include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/newsletter-block.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/social-block.php');

//include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-content.php');

get_footer(); 

//Includo chiusura documento
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/__HTML-doc-close.php');
?>