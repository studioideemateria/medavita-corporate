<?php

class social_widget extends WP_Widget {
 
function __construct() {
    parent::__construct(
    // Base ID of your widget
    'social_widget', 
 
    // Widget name will appear in UI
    __('Dati social', 'MM_social[widget]'), 
 
    // Widget description
    array( 'description' => __( 'Widget per compilare i dati relativi ai social network', 'MM_social[widget]' ), ) 
    );
}//Construct
 
// Widget front-end
public function widget( $args, $instance ) {
 
//Variabili Widget
$social_facebook = apply_filters( 'widget_social_facebook', $instance['social_facebook'] );
$social_twitter = apply_filters( 'widget_social_twitter', $instance['social_twitter'] );
$social_google_plus = apply_filters( 'widget_social_google_plus', $instance['social_google_plus'] );
$social_youtube = apply_filters( 'widget_social_youtube', $instance['social_youtube'] );
$social_vimeo = apply_filters( 'widget_social_vimeo', $instance['social_vimeo'] );
$social_linkedin = apply_filters( 'widget_social_linkedin', $instance['social_linkedin'] );
$social_behance = apply_filters( 'widget_social_behance', $instance['social_behance'] );
$social_flickr = apply_filters( 'widget_social_flickr', $instance['social_flickr'] );
$social_pinterest = apply_filters( 'widget_social_pinterest', $instance['social_pinterest'] );
$social_instagram = apply_filters( 'widget_social_instagram', $instance['social_instagram'] );
$social_tumblr = apply_filters( 'widget_social_tumblr', $instance['social_tumblr'] );
$social_skype = apply_filters( 'widget_social_skype', $instance['social_skype'] );
$social_dribbble = apply_filters( 'widget_social_dribbble', $instance['social_dribbble'] );
$social_soundcloud = apply_filters( 'widget_social_soundcloud', $instance['social_soundcloud'] );
}
 
// Widget Backend 
public function form( $instance ) {
 
   /* Impostazioni di default del widget */
   $defaults = array(
            'social_facebook'     => 'Facebook',
            'social_twitter'     => 'Twitter',
            'social_google_plus'     => 'Google Plus',
            'social_youtube'     => 'Youtube',
            'social_vimeo'     => 'Vimeo',
            'social_linkedin'     => 'Linkedin',
            'social_behance'     => 'Behance',
            'social_flickr'     => 'Flickr',
            'social_pinterest'     => 'Pinterest',
            'social_instagram'     => 'Instagram',
            'social_tumblr'     => 'Tumblr',
            'social_skype'     => 'Skype',
            'social_dribbble'     => 'Dribbble',
            'social_soundcloud'     => 'Soundcloud',
        );
 
    $instance = wp_parse_args( (array) $instance, $defaults );
    $social_image_url = get_template_directory() . '/widgets/social/social-icons/';

// Widget admin form
?>

    <p>Inserire l'URL dei social newtork</p>
 
    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_facebook' ); ?>">
        <?php echo file_get_contents( $social_image_url.'facebook-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_facebook' ); ?>" name="<?php echo $this->get_field_name( 'social_facebook' ); ?>" value="<?php $val = ($instance['social_facebook'] == "Facebook") ? '' : $instance['social_facebook']; echo $val; ?>" placeholder="Facebook" />
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_twitter' ); ?>">
        <?php echo file_get_contents( $social_image_url.'twitter-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_twitter' ); ?>" name="<?php echo $this->get_field_name( 'social_twitter' ); ?>" value="<?php $val = ($instance['social_twitter'] == "Twitter") ? '' : $instance['social_twitter'];  echo $val;  ?>" placeholder="Twitter" />
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_google_plus' ); ?>">
        <?php echo file_get_contents( $social_image_url.'google-plus-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_google_plus' ); ?>" name="<?php echo $this->get_field_name( 'social_google_plus' ); ?>" value="<?php $val = ($instance['social_google_plus'] == "Google Plus") ? '' : $instance['social_google_plus']; echo $val; ?>" placeholder="Google Plus" />
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_youtube' ); ?>">
        <?php echo file_get_contents( $social_image_url.'youtube-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_youtube' ); ?>" name="<?php echo $this->get_field_name( 'social_youtube' ); ?>" value="<?php $val = ($instance['social_youtube'] == "Youtube") ? '' : $instance['social_youtube']; echo $val; ?>" placeholder="Youtube"/>
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_vimeo' ); ?>">
        <?php echo file_get_contents( $social_image_url.'vimeo-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_vimeo' ); ?>" name="<?php echo $this->get_field_name( 'social_vimeo' ); ?>" value="<?php $val = ($instance['social_vimeo'] == "Vimeo") ? '' : $instance['social_vimeo']; echo $val; ?>" placeholder="Vimeo"/>
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_linkedin' ); ?>">
        <?php echo file_get_contents( $social_image_url.'linkedin-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_linkedin' ); ?>" name="<?php echo $this->get_field_name( 'social_linkedin' ); ?>" value="<?php $val = ($instance['social_linkedin'] == "Linkedin") ? '' : $instance['social_linkedin']; echo $val; ?>" placeholder="Linkedin"/>
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_behance' ); ?>">
        <?php echo file_get_contents( $social_image_url.'behance-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_behance' ); ?>" name="<?php echo $this->get_field_name( 'social_behance' ); ?>" value="<?php $val = ($instance['social_behance'] == "Behance") ? '' : $instance['social_behance']; echo $val; ?>" placeholder="Behance"/>
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_flickr' ); ?>">
        <?php echo file_get_contents( $social_image_url.'flickr-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_flickr' ); ?>" name="<?php echo $this->get_field_name( 'social_flickr' ); ?>" value="<?php $val = ($instance['social_flickr'] == "Flickr") ? '' : $instance['social_flickr']; echo $val; ?>" placeholder="Flickr"/>
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_pinterest' ); ?>">
        <?php echo file_get_contents( $social_image_url.'pinterest-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_pinterest' ); ?>" name="<?php echo $this->get_field_name( 'social_pinterest' ); ?>" value="<?php $val = ($instance['social_pinterest'] == "Pinterest") ? '' : $instance['social_pinterest']; echo $val; ?>" placeholder="Pinterest" />
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_instagram' ); ?>">
        <?php echo file_get_contents( $social_image_url.'instagram-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_instagram' ); ?>" name="<?php echo $this->get_field_name( 'social_instagram' ); ?>" value="<?php $val = ($instance['social_instagram'] == "Instagram") ? '' : $instance['social_instagram']; echo $val; ?>" placeholder="Instagram"/>
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_tumblr' ); ?>">
        <?php echo file_get_contents( $social_image_url.'tumblr-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_tumblr' ); ?>" name="<?php echo $this->get_field_name( 'social_tumblr' ); ?>" value="<?php $val = ($instance['social_tumblr'] == "Tumblr") ? '' : $instance['social_tumblr']; echo $val; ?>" placeholder="Tumblr"/>
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_skype' ); ?>">
        <?php echo file_get_contents( $social_image_url.'skype-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_skype' ); ?>" name="<?php echo $this->get_field_name( 'social_skype' ); ?>" value="<?php $val = ($instance['social_skype'] == "Skype") ? '' : $instance['social_skype']; echo $val; ?>" placeholder="Skype" />
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_dribbble' ); ?>">
        <?php echo file_get_contents( $social_image_url.'dribbble-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_dribbble' ); ?>" name="<?php echo $this->get_field_name( 'social_dribbble' ); ?>" value="<?php $val = ($instance['social_dribbble'] == "Dribbble") ? '' : $instance['social_dribbble']; echo $val; ?>" placeholder="Dribble" />
    <div class="clearfix"></div>
    <br />

    <span style="margin-bottom:2px; float: left; display: inline-block; margin-top: 12px;" for="<?php echo $this->get_field_id( 'social_soundcloud' ); ?>">
        <?php echo file_get_contents( $social_image_url.'soundcloud-icon.svg') ?>
    </span>
    <input type="text" style="width:86%; margin-top:8px; float: right; line-height: 35px;" id="<?php echo $this->get_field_id( 'social_soundcloud' ); ?>" name="<?php echo $this->get_field_name( 'social_soundcloud' ); ?>" value="<?php $val = ($instance['social_soundcloud'] == "Soundcloud") ? '' : $instance['social_soundcloud']; echo $val; ?>" placeholder="Soundcloud"/>
    <div class="clearfix"></div>
    <br />


    <hr />
 
<?php 
}
    // Widget Update
    public function update( $new_instance, $old_instance ) {
 
        $instance = $old_instance;
 
        $instance['social_facebook'] = strip_tags( $new_instance['social_facebook'] );
        $instance['social_twitter'] = strip_tags( $new_instance['social_twitter'] );
        $instance['social_google_plus'] = strip_tags( $new_instance['social_google_plus'] );
        $instance['social_youtube'] = strip_tags( $new_instance['social_youtube'] );
        $instance['social_vimeo'] = strip_tags( $new_instance['social_vimeo'] );
        $instance['social_linkedin'] = strip_tags( $new_instance['social_linkedin'] );
        $instance['social_behance'] = strip_tags( $new_instance['social_behance'] );
        $instance['social_flickr'] = strip_tags( $new_instance['social_flickr'] );
        $instance['social_pinterest'] = strip_tags( $new_instance['social_pinterest'] );
        $instance['social_instagram'] = strip_tags( $new_instance['social_instagram'] );
        $instance['social_tumblr'] = strip_tags( $new_instance['social_tumblr'] );
        $instance['social_skype'] = strip_tags( $new_instance['social_skype'] );
        $instance['social_dribbble'] = strip_tags( $new_instance['social_dribbble'] );
        $instance['social_soundcloud'] = strip_tags( $new_instance['social_soundcloud'] );
 
        return $instance;
    }
 
} // Class 
 
// Registra e Carica Widget nel backend WP
function social_load_widget() {
    register_widget( 'social_widget' );
}
add_action( 'widgets_init', 'social_load_widget' );