<?php

class footer_widget extends WP_Widget {
 
function __construct() {
    parent::__construct(
    // Base ID of your widget
    'footer_widget', 
 
    // Widget name will appear in UI
    __('Dati footer', 'MM[widget]'), 
 
    // Widget description
    array( 'description' => __( 'Widget per compilare i dati del footer', 'MM[widget]' ), ) 
    );
}//Construct
 
// Widget front-end
public function widget( $args, $instance ) {
 
    //Variabili Widget
    $siteOwner = apply_filters( 'widget_siteOwner', $instance['siteOwner'] );
    $siteAddress = apply_filters( 'widget_siteAddress', $instance['siteAddress'] );
    $siteEmail = apply_filters( 'widget_siteEmail', $instance['siteEmail'] );
    $sitePhone = apply_filters( 'widget_sitePhone', $instance['sitePhone'] );
    $siteFax = apply_filters( 'widget_siteFax', $instance['siteFax'] );
    $sitePiva = apply_filters( 'widget_sitePiva', $instance['sitePiva'] );
    $capitaleSociale = apply_filters( 'widget_capitaleSociale', $instance['capitaleSociale'] );
    $rea = apply_filters( 'widget_rea', $instance['rea'] );
}
 
// Widget Backend 
public function form( $instance ) {
 
   /* Impostazioni di default del widget */
   $defaults = array(
            'siteOwner'     => 'Proprietario',
            'siteAddress'   => 'Indirizzo',
            'siteEmail'     => 'Email',
            'sitePhone'     => 'Telefono',
            'siteFax'       => 'Fax',
            'sitePiva'      => 'P.IVA',
            'capitaleSociale'      => 'Capitale Sociale',
            'rea'      => 'REA',
        );
 
    $instance = wp_parse_args( (array) $instance, $defaults );
 
// Widget admin form
?>
 
    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'siteOwner' ); ?>"> Proprietario:</p>
    <input type="text" style="width:100%;" id="<?php echo $this->get_field_id( 'siteOwner' ); ?>" name="<?php echo $this->get_field_name( 'siteOwner' ); ?>" value="<?php echo $instance['siteOwner']; ?>" />
    <br />
 
    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'siteAddress' ); ?>">Indirizzo:</p>
    <textarea style="width:100%; height:60px;" id="<?php echo $this->get_field_id( 'siteAddress' ); ?>" name="<?php echo $this->get_field_name( 'siteAddress' ); ?>"><?php echo $instance['siteAddress']; ?></textarea>
    <br />

    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'siteEmail' ); ?>">Email:</p>
    <input type="text" style="width:100%;" id="<?php echo $this->get_field_id( 'siteEmail' ); ?>" name="<?php echo $this->get_field_name( 'siteEmail' ); ?>" value="<?php echo $instance['siteEmail']; ?>" />
    <br />

    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'sitePhone' ); ?>">Telefono:</p>
    <input type="text" style="width:100%;" id="<?php echo $this->get_field_id( 'sitePhone' ); ?>" name="<?php echo $this->get_field_name( 'sitePhone' ); ?>" value="<?php echo $instance['sitePhone']; ?>">
    <br />

    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'siteFax' ); ?>">Fax:</p>
    <input type="text" style="width:100%;" id="<?php echo $this->get_field_id( 'siteFax' ); ?>" name="<?php echo $this->get_field_name( 'siteFax' ); ?>" value="<?php echo $instance['siteFax']; ?>">
    <br />

    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'sitePiva' ); ?>">P. IVA:</p>
    <input type="text" style="width:100%;" id="<?php echo $this->get_field_id( 'sitePiva' ); ?>" name="<?php echo $this->get_field_name( 'sitePiva' ); ?>" value="<?php echo $instance['sitePiva']; ?>">
    <br />

    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'capitaleSociale' ); ?>">Capitale sociale:</p>
    <input type="text" style="width:100%;" id="<?php echo $this->get_field_id( 'capitaleSociale' ); ?>" name="<?php echo $this->get_field_name( 'capitaleSociale' ); ?>" value="<?php echo $instance['capitaleSociale']; ?>">
    <br />

    <p style="margin-bottom:2px;" for="<?php echo $this->get_field_id( 'rea' ); ?>">REA:</p>
    <input type="text" style="width:100%;" id="<?php echo $this->get_field_id( 'rea' ); ?>" name="<?php echo $this->get_field_name( 'rea' ); ?>" value="<?php echo $instance['rea']; ?>">
    <br />

    <hr />

    
 
<?php 
}
    // Widget Update
    public function update( $new_instance, $old_instance ) {
 
        $instance = $old_instance;
 
        $instance['siteOwner'] = strip_tags( $new_instance['siteOwner'] );

        $instance['siteAddress'] = strip_tags( $new_instance['siteAddress'] );

        $instance['siteEmail'] = strip_tags( $new_instance['siteEmail'] );

        $instance['sitePhone'] = strip_tags( $new_instance['sitePhone'] );

        $instance['siteFax'] = strip_tags( $new_instance['siteFax'] );

        $instance['sitePiva'] = strip_tags( $new_instance['sitePiva'] );

        $instance['capitaleSociale'] = strip_tags( $new_instance['capitaleSociale'] );

        $instance['rea'] = strip_tags( $new_instance['rea'] );

        return $instance;
 
    }
 
} // Class 
 
// Registra e Carica Widget nel backend WP
function adv_load_widget() {
    register_widget( 'footer_widget' );
}
add_action( 'widgets_init', 'adv_load_widget' );