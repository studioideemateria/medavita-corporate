(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (global){
'use strict';

var lazyload = require('jquery-lazy'),
    lazyloadPlugin = require('../../../node_modules/jquery-lazy/jquery.lazy.plugins'),
    imgLoader = require('../modules/lazy');

var siteGlobal = {
  initReady: function () {
    this.menuInit();
  },
  initLoad: function () {
    this.lazyLoad();
    this.linkAll();
  },
  initResize: function () {},
  initResizeEnd: function () {},
  initScroll: function () {
    this.headerClass();
  },
  onScroll: function () {},
  linkAll: function () {
    $('.linkFrom').each(function () {
      var elemToLink = $(this).parents('.linkTo');
      var pageLink = $(this).attr('href');
      var linkTarget = typeof $(this).attr('target') !== 'undefined' ? $(this).attr('target') : '';
      if (pageLink !== undefined && pageLink !== 'javascript:;') {
        elemToLink.addClass('pointer').click({ myLink: pageLink, myTarget: linkTarget }, function (e) {
          e.preventDefault();
          if (e.data.myTarget === '_blank') {
            window.open(e.data.myLink);
          } else {
            window.location.href = e.data.myLink;
          }
        });
      }
    });
  },
  lazyLoad: function () {
    imgLoader.lazyInit();
  },
  headerClass: function () {
    if ($(window).scrollTop() > 0) {
      $('.header').addClass('scrolled');
    } else {
      $('.header').removeClass('scrolled');
    }
  },
  menuInit: function () {
    $('.header__hamburgerWrap').on('click', function () {
      $('.header__hamburger, .header__menuWrap').toggleClass('is-active open');
      $('html').toggleClass('menu-open');
    });
  },
  rebuildAllEvents: function () {
    this.initLoad();
  }
};

global.siteGlobal = siteGlobal;

(function () {

  siteGlobal.initReady();

  $(window).on('load', function () {
    $('body, #preloader').addClass('loaded');
    siteGlobal.initLoad();
  });

  $(window).resize(function () {
    siteGlobal.initResize();
  });

  if (typeof $(window).resizeend === 'function') {
    $(window).resizeend(function () {
      siteGlobal.initResizeEnd();
    });
  } else {
    $(window).resize(function () {
      siteGlobal.initResizeEnd();
    });
  }
  $(window).scroll(function () {
    siteGlobal.initScroll();
  });

  $(window).on('scroll', function () {
    siteGlobal.onScroll();
  });
})();

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../../../node_modules/jquery-lazy/jquery.lazy.plugins":4,"../modules/lazy":2,"jquery-lazy":3}],2:[function(require,module,exports){
module.exports = {
	lazyInit: function () {
		$('.lazy').Lazy({
			threshold: 600,
			effect: 'fadeIn',
			effectTime: 500,
			throttle: 150,
			bind: 'event',
			placeholder: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAARgAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABAMDAwMDBAMDBAYEAwQGBwUEBAUHCAYGBwYGCAoICQkJCQgKCgwMDAwMCgwMDQ0MDBERERERFBQUFBQUFBQUFAEEBQUIBwgPCgoPFA4ODhQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgACgAKAwERAAIRAQMRAf/EAEsAAQEAAAAAAAAAAAAAAAAAAAAIAQEAAAAAAAAAAAAAAAAAAAAAEAEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwCygAAAf//Z",
			onError: function (element) {
				console.log('error loading ' + element.data('src'));
			}
		});
	}
};

},{}],3:[function(require,module,exports){
/*!
 * jQuery & Zepto Lazy - v1.7.10
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * $("img.lazy").lazy();
 */

;(function(window, undefined) {
    "use strict";

    // noinspection JSUnresolvedVariable
    /**
     * library instance - here and not in construct to be shorter in minimization
     * @return void
     */
    var $ = window.jQuery || window.Zepto,

    /**
     * unique plugin instance id counter
     * @type {number}
     */
    lazyInstanceId = 0,

    /**
     * helper to register window load for jQuery 3
     * @type {boolean}
     */    
    windowLoaded = false;

    /**
     * make lazy available to jquery - and make it a bit more case-insensitive :)
     * @access public
     * @type {function}
     * @param {object} settings
     * @return {LazyPlugin}
     */
    $.fn.Lazy = $.fn.lazy = function(settings) {
        return new LazyPlugin(this, settings);
    };

    /**
     * helper to add plugins to lazy prototype configuration
     * @access public
     * @type {function}
     * @param {string|Array} names
     * @param {string|Array|function} [elements]
     * @param {function} loader
     * @return void
     */
    $.Lazy = $.lazy = function(names, elements, loader) {
        // make second parameter optional
        if ($.isFunction(elements)) {
            loader = elements;
            elements = [];
        }

        // exit here if parameter is not a callable function
        if (!$.isFunction(loader)) {
            return;
        }

        // make parameters an array of names to be sure
        names = $.isArray(names) ? names : [names];
        elements = $.isArray(elements) ? elements : [elements];

        var config = LazyPlugin.prototype.config,
            forced = config._f || (config._f = {});

        // add the loader plugin for every name
        for (var i = 0, l = names.length; i < l; i++) {
            if (config[names[i]] === undefined || $.isFunction(config[names[i]])) {
                config[names[i]] = loader;
            }
        }

        // add forced elements loader
        for (var c = 0, a = elements.length; c < a; c++) {
            forced[elements[c]] = names[0];
        }
    };

    /**
     * contains all logic and the whole element handling
     * is packed in a private function outside class to reduce memory usage, because it will not be created on every plugin instance
     * @access private
     * @type {function}
     * @param {LazyPlugin} instance
     * @param {object} config
     * @param {object|Array} items
     * @param {object} events
     * @param {string} namespace
     * @return void
     */
    function _executeLazy(instance, config, items, events, namespace) {
        /**
         * a helper to trigger the 'onFinishedAll' callback after all other events
         * @access private
         * @type {number}
         */
        var _awaitingAfterLoad = 0,

        /**
         * visible content width
         * @access private
         * @type {number}
         */
        _actualWidth = -1,

        /**
         * visible content height
         * @access private
         * @type {number}
         */
        _actualHeight = -1,

        /**
         * determine possibly detected high pixel density
         * @access private
         * @type {boolean}
         */
        _isRetinaDisplay = false, 

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _afterLoad = 'afterLoad',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _load = 'load',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _error = 'error',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _img = 'img',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _src = 'src',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _srcset = 'srcset',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _sizes = 'sizes',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _backgroundImage = 'background-image';

        /**
         * initialize plugin
         * bind loading to events or set delay time to load all items at once
         * @access private
         * @return void
         */
        function _initialize() {
            // detect actual device pixel ratio
            // noinspection JSUnresolvedVariable
            _isRetinaDisplay = window.devicePixelRatio > 1;

            // prepare all initial items
            items = _prepareItems(items);

            // if delay time is set load all items at once after delay time
            if (config.delay >= 0) {
                setTimeout(function() {
                    _lazyLoadItems(true);
                }, config.delay);
            }

            // if no delay is set or combine usage is active bind events
            if (config.delay < 0 || config.combined) {
                // create unique event function
                events.e = _throttle(config.throttle, function(event) {
                    // reset detected window size on resize event
                    if (event.type === 'resize') {
                        _actualWidth = _actualHeight = -1;
                    }

                    // execute 'lazy magic'
                    _lazyLoadItems(event.all);
                });

                // create function to add new items to instance
                events.a = function(additionalItems) {
                    additionalItems = _prepareItems(additionalItems);
                    items.push.apply(items, additionalItems);
                };

                // create function to get all instance items left
                events.g = function() {
                    // filter loaded items before return in case internal filter was not running until now
                    return (items = $(items).filter(function() {
                        return !$(this).data(config.loadedName);
                    }));
                };

                // create function to force loading elements
                events.f = function(forcedItems) {
                    for (var i = 0; i < forcedItems.length; i++) {
                        // only handle item if available in current instance
                        // use a compare function, because Zepto can't handle object parameter for filter
                        // var item = items.filter(forcedItems[i]);
                        /* jshint loopfunc: true */
                        var item = items.filter(function() {
                            return this === forcedItems[i];
                        });

                        if (item.length) {
                            _lazyLoadItems(false, item);   
                        }
                    }
                };

                // load initial items
                _lazyLoadItems();

                // bind lazy load functions to scroll and resize event
                // noinspection JSUnresolvedVariable
                $(config.appendScroll).on('scroll.' + namespace + ' resize.' + namespace, events.e);
            }
        }

        /**
         * prepare items before handle them
         * @access private
         * @param {Array|object|jQuery} items
         * @return {Array|object|jQuery}
         */
        function _prepareItems(items) {
            // fetch used configurations before loops
            var defaultImage = config.defaultImage,
                placeholder = config.placeholder,
                imageBase = config.imageBase,
                srcsetAttribute = config.srcsetAttribute,
                loaderAttribute = config.loaderAttribute,
                forcedTags = config._f || {};

            // filter items and only add those who not handled yet and got needed attributes available
            items = $(items).filter(function() {
                var element = $(this),
                    tag = _getElementTagName(this);

                return !element.data(config.handledName) && 
                       (element.attr(config.attribute) || element.attr(srcsetAttribute) || element.attr(loaderAttribute) || forcedTags[tag] !== undefined);
            })

            // append plugin instance to all elements
            .data('plugin_' + config.name, instance);

            for (var i = 0, l = items.length; i < l; i++) {
                var element = $(items[i]),
                    tag = _getElementTagName(items[i]),
                    elementImageBase = element.attr(config.imageBaseAttribute) || imageBase;

                // generate and update source set if an image base is set
                if (tag === _img && elementImageBase && element.attr(srcsetAttribute)) {
                    element.attr(srcsetAttribute, _getCorrectedSrcSet(element.attr(srcsetAttribute), elementImageBase));
                }

                // add loader to forced element types
                if (forcedTags[tag] !== undefined && !element.attr(loaderAttribute)) {
                    element.attr(loaderAttribute, forcedTags[tag]);
                }

                // set default image on every element without source
                if (tag === _img && defaultImage && !element.attr(_src)) {
                    element.attr(_src, defaultImage);
                }

                // set placeholder on every element without background image
                else if (tag !== _img && placeholder && (!element.css(_backgroundImage) || element.css(_backgroundImage) === 'none')) {
                    element.css(_backgroundImage, "url('" + placeholder + "')");
                }
            }

            return items;
        }

        /**
         * the 'lazy magic' - check all items
         * @access private
         * @param {boolean} [allItems]
         * @param {object} [forced]
         * @return void
         */
        function _lazyLoadItems(allItems, forced) {
            // skip if no items where left
            if (!items.length) {
                // destroy instance if option is enabled
                if (config.autoDestroy) {
                    // noinspection JSUnresolvedFunction
                    instance.destroy();
                }

                return;
            }

            var elements = forced || items,
                loadTriggered = false,
                imageBase = config.imageBase || '',
                srcsetAttribute = config.srcsetAttribute,
                handledName = config.handledName;

            // loop all available items
            for (var i = 0; i < elements.length; i++) {
                // item is at least in loadable area
                if (allItems || forced || _isInLoadableArea(elements[i])) {
                    var element = $(elements[i]),
                        tag = _getElementTagName(elements[i]),
                        attribute = element.attr(config.attribute),
                        elementImageBase = element.attr(config.imageBaseAttribute) || imageBase,
                        customLoader = element.attr(config.loaderAttribute);

                        // is not already handled 
                    if (!element.data(handledName) &&
                        // and is visible or visibility doesn't matter
                        (!config.visibleOnly || element.is(':visible')) && (
                        // and image source or source set attribute is available
                        (attribute || element.attr(srcsetAttribute)) && (
                            // and is image tag where attribute is not equal source or source set
                            (tag === _img && (elementImageBase + attribute !== element.attr(_src) || element.attr(srcsetAttribute) !== element.attr(_srcset))) ||
                            // or is non image tag where attribute is not equal background
                            (tag !== _img && elementImageBase + attribute !== element.css(_backgroundImage))
                        ) ||
                        // or custom loader is available
                        customLoader))
                    {
                        // mark element always as handled as this point to prevent double handling
                        loadTriggered = true;
                        element.data(handledName, true);

                        // load item
                        _handleItem(element, tag, elementImageBase, customLoader);
                    }
                }
            }

            // when something was loaded remove them from remaining items
            if (loadTriggered) {
                items = $(items).filter(function() {
                    return !$(this).data(handledName);
                });
            }
        }

        /**
         * load the given element the lazy way
         * @access private
         * @param {object} element
         * @param {string} tag
         * @param {string} imageBase
         * @param {function} [customLoader]
         * @return void
         */
        function _handleItem(element, tag, imageBase, customLoader) {
            // increment count of items waiting for after load
            ++_awaitingAfterLoad;

            // extended error callback for correct 'onFinishedAll' handling
            var errorCallback = function() {
                _triggerCallback('onError', element);
                _reduceAwaiting();

                // prevent further callback calls
                errorCallback = $.noop;
            };

            // trigger function before loading image
            _triggerCallback('beforeLoad', element);

            // fetch all double used data here for better code minimization
            var srcAttribute = config.attribute,
                srcsetAttribute = config.srcsetAttribute,
                sizesAttribute = config.sizesAttribute,
                retinaAttribute = config.retinaAttribute,
                removeAttribute = config.removeAttribute,
                loadedName = config.loadedName,
                elementRetina = element.attr(retinaAttribute);

            // handle custom loader
            if (customLoader) {
                // on load callback
                var loadCallback = function() {
                    // remove attribute from element
                    if (removeAttribute) {
                        element.removeAttr(config.loaderAttribute);
                    }

                    // mark element as loaded
                    element.data(loadedName, true);

                    // call after load event
                    _triggerCallback(_afterLoad, element);

                    // remove item from waiting queue and possibly trigger finished event
                    // it's needed to be asynchronous to run after filter was in _lazyLoadItems
                    setTimeout(_reduceAwaiting, 1);

                    // prevent further callback calls
                    loadCallback = $.noop;
                };

                // bind error event to trigger callback and reduce waiting amount
                element.off(_error).one(_error, errorCallback)

                // bind after load callback to element
                .one(_load, loadCallback);

                // trigger custom loader and handle response
                if (!_triggerCallback(customLoader, element, function(response) {
                    if(response) {
                        element.off(_load);
                        loadCallback();
                    }
                    else {
                        element.off(_error);
                        errorCallback();
                    }
                })) {
                    element.trigger(_error);
                }
            }

            // handle images
            else {
                // create image object
                var imageObj = $(new Image());

                // bind error event to trigger callback and reduce waiting amount
                imageObj.one(_error, errorCallback)

                // bind after load callback to image
                .one(_load, function() {
                    // remove element from view
                    element.hide();

                    // set image back to element
                    // do it as single 'attr' calls, to be sure 'src' is set after 'srcset'
                    if (tag === _img) {
                        element.attr(_sizes, imageObj.attr(_sizes))
                               .attr(_srcset, imageObj.attr(_srcset))
                               .attr(_src, imageObj.attr(_src));
                    }
                    else {
                        element.css(_backgroundImage, "url('" + imageObj.attr(_src) + "')");
                    }

                    // bring it back with some effect!
                    element[config.effect](config.effectTime);

                    // remove attribute from element
                    if (removeAttribute) {
                        element.removeAttr(srcAttribute + ' ' + srcsetAttribute + ' ' + retinaAttribute + ' ' + config.imageBaseAttribute);

                        // only remove 'sizes' attribute, if it was a custom one
                        if (sizesAttribute !== _sizes) {
                            element.removeAttr(sizesAttribute);
                        }
                    }

                    // mark element as loaded
                    element.data(loadedName, true);

                    // call after load event
                    _triggerCallback(_afterLoad, element);

                    // cleanup image object
                    imageObj.remove();

                    // remove item from waiting queue and possibly trigger finished event
                    _reduceAwaiting();
                });

                // set sources
                // do it as single 'attr' calls, to be sure 'src' is set after 'srcset'
                var imageSrc = (_isRetinaDisplay && elementRetina ? elementRetina : element.attr(srcAttribute)) || '';
                imageObj.attr(_sizes, element.attr(sizesAttribute))
                        .attr(_srcset, element.attr(srcsetAttribute))
                        .attr(_src, imageSrc ? imageBase + imageSrc : null);

                // call after load even on cached image
                imageObj.complete && imageObj.trigger(_load); // jshint ignore : line
            }
        }

        /**
         * check if the given element is inside the current viewport or threshold
         * @access private
         * @param {object} element
         * @return {boolean}
         */
        function _isInLoadableArea(element) {
            var elementBound = element.getBoundingClientRect(),
                direction    = config.scrollDirection,
                threshold    = config.threshold,
                vertical     = // check if element is in loadable area from top
                               ((_getActualHeight() + threshold) > elementBound.top) &&
                               // check if element is even in loadable are from bottom
                               (-threshold < elementBound.bottom),
                horizontal   = // check if element is in loadable area from left
                               ((_getActualWidth() + threshold) > elementBound.left) &&
                               // check if element is even in loadable area from right
                               (-threshold < elementBound.right);

            if (direction === 'vertical') {
                return vertical;
            }
            else if (direction === 'horizontal') {
                return horizontal;
            }

            return vertical && horizontal;
        }

        /**
         * receive the current viewed width of the browser
         * @access private
         * @return {number}
         */
        function _getActualWidth() {
            return _actualWidth >= 0 ? _actualWidth : (_actualWidth = $(window).width());
        }

        /**
         * receive the current viewed height of the browser
         * @access private
         * @return {number}
         */
        function _getActualHeight() {
            return _actualHeight >= 0 ? _actualHeight : (_actualHeight = $(window).height());
        }

        /**
         * get lowercase tag name of an element
         * @access private
         * @param {object} element
         * @returns {string}
         */
        function _getElementTagName(element) {
            return element.tagName.toLowerCase();
        }

        /**
         * prepend image base to all srcset entries
         * @access private
         * @param {string} srcset
         * @param {string} imageBase
         * @returns {string}
         */
        function _getCorrectedSrcSet(srcset, imageBase) {
            if (imageBase) {
                // trim, remove unnecessary spaces and split entries
                var entries = srcset.split(',');
                srcset = '';

                for (var i = 0, l = entries.length; i < l; i++) {
                    srcset += imageBase + entries[i].trim() + (i !== l - 1 ? ',' : '');
                }
            }

            return srcset;
        }

        /**
         * helper function to throttle down event triggering
         * @access private
         * @param {number} delay
         * @param {function} callback
         * @return {function}
         */
        function _throttle(delay, callback) {
            var timeout,
                lastExecute = 0;

            return function(event, ignoreThrottle) {
                var elapsed = +new Date() - lastExecute;

                function run() {
                    lastExecute = +new Date();
                    // noinspection JSUnresolvedFunction
                    callback.call(instance, event);
                }

                timeout && clearTimeout(timeout); // jshint ignore : line

                if (elapsed > delay || !config.enableThrottle || ignoreThrottle) {
                    run();
                }
                else {
                    timeout = setTimeout(run, delay - elapsed);
                }
            };
        }

        /**
         * reduce count of awaiting elements to 'afterLoad' event and fire 'onFinishedAll' if reached zero
         * @access private
         * @return void
         */
        function _reduceAwaiting() {
            --_awaitingAfterLoad;

            // if no items were left trigger finished event
            if (!items.length && !_awaitingAfterLoad) {
                _triggerCallback('onFinishedAll');
            }
        }

        /**
         * single implementation to handle callbacks, pass element and set 'this' to current instance
         * @access private
         * @param {string|function} callback
         * @param {object} [element]
         * @param {*} [args]
         * @return {boolean}
         */
        function _triggerCallback(callback, element, args) {
            if ((callback = config[callback])) {
                // jQuery's internal '$(arguments).slice(1)' are causing problems at least on old iPads
                // below is shorthand of 'Array.prototype.slice.call(arguments, 1)'
                callback.apply(instance, [].slice.call(arguments, 1));
                return true;
            }

            return false;
        }

        // if event driven or window is already loaded don't wait for page loading
        if (config.bind === 'event' || windowLoaded) {
            _initialize();
        }

        // otherwise load initial items and start lazy after page load
        else {
            // noinspection JSUnresolvedVariable
            $(window).on(_load + '.' + namespace, _initialize);
        }  
    }

    /**
     * lazy plugin class constructor
     * @constructor
     * @access private
     * @param {object} elements
     * @param {object} settings
     * @return {object|LazyPlugin}
     */
    function LazyPlugin(elements, settings) {
        /**
         * this lazy plugin instance
         * @access private
         * @type {object|LazyPlugin|LazyPlugin.prototype}
         */
        var _instance = this,

        /**
         * this lazy plugin instance configuration
         * @access private
         * @type {object}
         */
        _config = $.extend({}, _instance.config, settings),

        /**
         * instance generated event executed on container scroll or resize
         * packed in an object to be referenceable and short named because properties will not be minified
         * @access private
         * @type {object}
         */
        _events = {},

        /**
         * unique namespace for instance related events
         * @access private
         * @type {string}
         */
        _namespace = _config.name + '-' + (++lazyInstanceId);

        // noinspection JSUndefinedPropertyAssignment
        /**
         * wrapper to get or set an entry from plugin instance configuration
         * much smaller on minify as direct access
         * @access public
         * @type {function}
         * @param {string} entryName
         * @param {*} [value]
         * @return {LazyPlugin|*}
         */
        _instance.config = function(entryName, value) {
            if (value === undefined) {
                return _config[entryName];
            }

            _config[entryName] = value;
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * add additional items to current instance
         * @access public
         * @param {Array|object|string} items
         * @return {LazyPlugin}
         */
        _instance.addItems = function(items) {
            _events.a && _events.a($.type(items) === 'string' ? $(items) : items); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * get all left items of this instance
         * @access public
         * @returns {object}
         */
        _instance.getItems = function() {
            return _events.g ? _events.g() : {};
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force lazy to load all items in loadable area right now
         * by default without throttle
         * @access public
         * @type {function}
         * @param {boolean} [useThrottle]
         * @return {LazyPlugin}
         */
        _instance.update = function(useThrottle) {
            _events.e && _events.e({}, !useThrottle); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force element(s) to load directly, ignoring the viewport
         * @access public
         * @param {Array|object|string} items
         * @return {LazyPlugin}
         */
        _instance.force = function(items) {
            _events.f && _events.f($.type(items) === 'string' ? $(items) : items); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force lazy to load all available items right now
         * this call ignores throttling
         * @access public
         * @type {function}
         * @return {LazyPlugin}
         */
        _instance.loadAll = function() {
            _events.e && _events.e({all: true}, true); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * destroy this plugin instance
         * @access public
         * @type {function}
         * @return undefined
         */
        _instance.destroy = function() {
            // unbind instance generated events
            // noinspection JSUnresolvedFunction, JSUnresolvedVariable
            $(_config.appendScroll).off('.' + _namespace, _events.e);
            // noinspection JSUnresolvedVariable
            $(window).off('.' + _namespace);

            // clear events
            _events = {};

            return undefined;
        };

        // start using lazy and return all elements to be chainable or instance for further use
        // noinspection JSUnresolvedVariable
        _executeLazy(_instance, _config, elements, _events, _namespace);
        return _config.chainable ? elements : _instance;
    }

    /**
     * settings and configuration data
     * @access public
     * @type {object|*}
     */
    LazyPlugin.prototype.config = {
        // general
        name               : 'lazy',
        chainable          : true,
        autoDestroy        : true,
        bind               : 'load',
        threshold          : 500,
        visibleOnly        : false,
        appendScroll       : window,
        scrollDirection    : 'both',
        imageBase          : null,
        defaultImage       : 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
        placeholder        : null,
        delay              : -1,
        combined           : false,

        // attributes
        attribute          : 'data-src',
        srcsetAttribute    : 'data-srcset',
        sizesAttribute     : 'data-sizes',
        retinaAttribute    : 'data-retina',
        loaderAttribute    : 'data-loader',
        imageBaseAttribute : 'data-imagebase',
        removeAttribute    : true,
        handledName        : 'handled',
        loadedName         : 'loaded',

        // effect
        effect             : 'show',
        effectTime         : 0,

        // throttle
        enableThrottle     : true,
        throttle           : 250,

        // callbacks
        beforeLoad         : undefined,
        afterLoad          : undefined,
        onError            : undefined,
        onFinishedAll      : undefined
    };

    // register window load event globally to prevent not loading elements
    // since jQuery 3.X ready state is fully async and may be executed after 'load' 
    $(window).on('load', function() {
        windowLoaded = true;
    });
})(window);
},{}],4:[function(require,module,exports){
/*!
 * jQuery & Zepto Lazy - AJAX Plugin - v1.4
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // load data by ajax request and pass them to elements inner html, like:
    // <div data-loader="ajax" data-src="url.html" data-method="post" data-type="html"></div>
    $.lazy('ajax', function(element, response) {
        ajaxRequest(this, element, response, element.attr('data-method'));
    });

    // load data by ajax get request and pass them to elements inner html, like:
    // <div data-loader="get" data-src="url.html" data-type="html"></div>
    $.lazy('get', function(element, response) {
        ajaxRequest(this, element, response, 'GET');
    });

    // load data by ajax post request and pass them to elements inner html, like:
    // <div data-loader="post" data-src="url.html" data-type="html"></div>
    $.lazy('post', function(element, response) {
        ajaxRequest(this, element, response, 'POST');
    });

    // load data by ajax put request and pass them to elements inner html, like:
    // <div data-loader="put" data-src="url.html" data-type="html"></div>
    $.lazy('put', function(element, response) {
        ajaxRequest(this, element, response, 'PUT');
    });

    /**
     * execute ajax request and handle response
     * @param {object} instance
     * @param {jQuery|object} element
     * @param {function} response
     * @param {string} [method]
     */
    function ajaxRequest(instance, element, response, method) {
        method = method ? method.toUpperCase() : 'GET';

        var data;
        if ((method === 'POST' || method === 'PUT') && instance.config('ajaxCreateData')) {
            data = instance.config('ajaxCreateData').apply(instance, [element]);
        }

        $.ajax({
            url: element.attr('data-src'),
            type: method === 'POST' || method === 'PUT' ? method : 'GET',
            data: data,
            dataType: element.attr('data-type') || 'html',

            /**
             * success callback
             * @access private
             * @param {*} content
             * @return {void}
             */
            success: function(content) {
                // set responded data to element's inner html
                element.html(content);

                // use response function for Zepto
                response(true);

                // remove attributes
                if (instance.config('removeAttribute')) {
                    element.removeAttr('data-src data-method data-type');
                }
            },

            /**
             * error callback
             * @access private
             * @return {void}
             */
            error: function() {
                // pass error state to lazy
                // use response function for Zepto
                response(false);
            }
        });
    }
})(window.jQuery || window.Zepto);

/*!
 * jQuery & Zepto Lazy - AV Plugin - v1.4
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // loads audio and video tags including tracks by two ways, like:
    // <audio>
    //   <data-src src="audio.ogg" type="video/ogg"></data-src>
    //   <data-src src="audio.mp3" type="video/mp3"></data-src>
    // </audio>
    // <video data-poster="poster.jpg">
    //   <data-src src="video.ogv" type="video/ogv"></data-src>
    //   <data-src src="video.webm" type="video/webm"></data-src>
    //   <data-src src="video.mp4" type="video/mp4"></data-src>
    //   <data-track kind="captions" src="captions.vtt" srclang="en"></data-track>
    //   <data-track kind="descriptions" src="descriptions.vtt" srclang="en"></data-track>
    //   <data-track kind="subtitles" src="subtitles.vtt" srclang="de"></data-track>
    // </video>
    //
    // or:
    // <audio data-src="audio.ogg|video/ogg,video.mp3|video/mp3"></video>
    // <video data-poster="poster.jpg" data-src="video.ogv|video/ogv,video.webm|video/webm,video.mp4|video/mp4">
    //   <data-track kind="captions" src="captions.vtt" srclang="en"></data-track>
    //   <data-track kind="descriptions" src="descriptions.vtt" srclang="en"></data-track>
    //   <data-track kind="subtitles" src="subtitles.vtt" srclang="de"></data-track>
    // </video>
    $.lazy(['av', 'audio', 'video'], ['audio', 'video'], function(element, response) {
        var elementTagName = element[0].tagName.toLowerCase();

        if (elementTagName === 'audio' || elementTagName === 'video') {
            var srcAttr = 'data-src',
                sources = element.find(srcAttr),
                tracks = element.find('data-track'),
                sourcesInError = 0,

            // create on error callback for sources
            onError = function() {
                if (++sourcesInError === sources.length) {
                    response(false);
                }
            },

            // create callback to handle a source or track entry
            handleSource = function() {
                var source = $(this),
                    type = source[0].tagName.toLowerCase(),
                    attributes = source.prop('attributes'),
                    target = $(type === srcAttr ? '<source>' : '<track>');

                if (type === srcAttr) {
                    target.one('error', onError);
                }

                $.each(attributes, function(index, attribute) {
                    target.attr(attribute.name, attribute.value);
                });

                source.replaceWith(target);
            };

            // create event for successfull load
            element.one('loadedmetadata', function() {
                response(true);
            })

            // remove default callbacks to ignore loading poster image
            .off('load error')

            // load poster image
            .attr('poster', element.attr('data-poster'));

            // load by child tags
            if (sources.length) {
                sources.each(handleSource);
            }

            // load by attribute
            else if (element.attr(srcAttr)) {
                // split for every entry by comma
                $.each(element.attr(srcAttr).split(','), function(index, value) {
                    // split again for file and file type
                    var parts = value.split('|');

                    // create a source entry
                    element.append($('<source>')
                           .one('error', onError)
                           .attr({src: parts[0].trim(), type: parts[1].trim()}));
                });

                // remove now obsolete attribute
                if (this.config('removeAttribute')) {
                    element.removeAttr(srcAttr);
                }
            }

            else {
                // pass error state
                // use response function for Zepto
                response(false);
            }

            // load optional tracks
            if (tracks.length) {
                tracks.each(handleSource);
            }
        }

        else {
            // pass error state
            // use response function for Zepto
            response(false);
        }
    });
})(window.jQuery || window.Zepto);

/*!
 * jQuery & Zepto Lazy - iFrame Plugin - v1.5
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // load iframe content, like:
    // <iframe data-src="iframe.html"></iframe>
    //
    // enable content error check with:
    // <iframe data-src="iframe.html" data-error-detect="true"></iframe>
    $.lazy(['frame', 'iframe'], 'iframe', function(element, response) {
        var instance = this;

        if (element[0].tagName.toLowerCase() === 'iframe') {
            var srcAttr = 'data-src',
                errorDetectAttr = 'data-error-detect',
                errorDetect = element.attr(errorDetectAttr);

            // default way, just replace the 'src' attribute
            if (errorDetect !== 'true' && errorDetect !== '1') {
                // set iframe source
                element.attr('src', element.attr(srcAttr));

                // remove attributes
                if (instance.config('removeAttribute')) {
                    element.removeAttr(srcAttr + ' ' + errorDetectAttr);
                }
            }

            // extended way, even check if the document is available
            else {
                $.ajax({
                    url: element.attr(srcAttr),
                    dataType: 'html',
                    crossDomain: true,
                    xhrFields: {withCredentials: true},

                    /**
                     * success callback
                     * @access private
                     * @param {*} content
                     * @return {void}
                     */
                    success: function(content) {
                        // set responded data to element's inner html
                        element.html(content)

                        // change iframe src
                        .attr('src', element.attr(srcAttr));

                        // remove attributes
                        if (instance.config('removeAttribute')) {
                            element.removeAttr(srcAttr + ' ' + errorDetectAttr);
                        }
                    },

                    /**
                     * error callback
                     * @access private
                     * @return {void}
                     */
                    error: function() {
                        // pass error state to lazy
                        // use response function for Zepto
                        response(false);
                    }
                });
            }
        }

        else {
            // pass error state to lazy
            // use response function for Zepto
            response(false);
        }
    });
})(window.jQuery || window.Zepto);

/*!
 * jQuery & Zepto Lazy - NOOP Plugin - v1.2
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // will do nothing, used to disable elements or for development
    // use like:
    // <div data-loader="noop"></div>

    // does not do anything, just a 'no-operation' helper ;)
    $.lazy('noop', function() {});

    // does nothing, but response a successfull loading
    $.lazy('noop-success', function(element, response) {
        // use response function for Zepto
        response(true);
    });

    // does nothing, but response a failed loading
    $.lazy('noop-error', function(element, response) {
        // use response function for Zepto
        response(false);
    });
})(window.jQuery || window.Zepto);

/*!
 * jQuery & Zepto Lazy - Picture Plugin - v1.3
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    var srcAttr = 'data-src',
        srcsetAttr = 'data-srcset',
        mediaAttr = 'data-media',
        sizesAttr = 'data-sizes',
        typeAttr = 'data-type';

    // loads picture elements like:
    // <picture>
    //   <data-src srcset="1x.jpg 1x, 2x.jpg 2x, 3x.jpg 3x" media="(min-width: 600px)" type="image/jpeg"></data-src>
    //   <data-src srcset="1x.jpg 1x, 2x.jpg 2x, 3x.jpg 3x" media="(min-width: 400px)" type="image/jpeg"></data-src>
    //   <data-img src="default.jpg" >
    // </picture>
    //
    // or:
    // <picture data-src="default.jpg">
    //   <data-src srcset="1x.jpg 1x, 2x.jpg 2x, 3x.jpg 3x" media="(min-width: 600px)" type="image/jpeg"></data-src>
    //   <data-src srcset="1x.jpg 1x, 2x.jpg 2x, 3x.jpg 3x" media="(min-width: 400px)" type="image/jpeg"></data-src>
    // </picture>
    //
    // or just with attributes in one line:
    // <picture data-src="default.jpg" data-srcset="1x.jpg 1x, 2x.jpg 2x, 3x.jpg 3x" data-media="(min-width: 600px)" data-sizes="" data-type="image/jpeg" />
    $.lazy(['pic', 'picture'], ['picture'], function(element, response) {
        var elementTagName = element[0].tagName.toLowerCase();

        if (elementTagName === 'picture') {
            var sources = element.find(srcAttr),
                image = element.find('data-img'),
                imageBase = this.config('imageBase') || '';

            // handle as child elements
            if (sources.length) {
                sources.each(function() {
                    renameElementTag($(this), 'source', imageBase);
                });

                // create img tag from child
                if (image.length === 1) {
                    image = renameElementTag(image, 'img', imageBase);

                    // bind event callbacks to new image tag
                    image.on('load', function() {
                        response(true);
                    }).on('error', function() {
                        response(false);
                    });

                    image.attr('src', image.attr(srcAttr));

                    if (this.config('removeAttribute')) {
                        image.removeAttr(srcAttr);
                    }
                }

                // create img tag from attribute
                else if (element.attr(srcAttr)) {
                    // create image tag
                    createImageObject(element, imageBase + element.attr(srcAttr), response);

                    if (this.config('removeAttribute')) {
                        element.removeAttr(srcAttr);
                    }
                }

                // pass error state
                else {
                    // use response function for Zepto
                    response(false);
                }
            }

            // handle as attributes
            else if( element.attr(srcsetAttr) ) {
                // create source elements before img tag
                $('<source>').attr({
                    media: element.attr(mediaAttr),
                    sizes: element.attr(sizesAttr),
                    type: element.attr(typeAttr),
                    srcset: getCorrectedSrcSet(element.attr(srcsetAttr), imageBase)
                })
                .appendTo(element);

                // create image tag
                createImageObject(element, imageBase + element.attr(srcAttr), response);

                // remove attributes from parent picture element
                if (this.config('removeAttribute')) {
                    element.removeAttr(srcAttr + ' ' + srcsetAttr + ' ' + mediaAttr + ' ' + sizesAttr + ' ' + typeAttr);
                }
            }

            // pass error state
            else {
                // use response function for Zepto
                response(false);
            }
        }

        else {
            // pass error state
            // use response function for Zepto
            response(false);
        }
    });

    /**
     * create a new child element and copy attributes
     * @param {jQuery|object} element
     * @param {string} toType
     * @param {string} imageBase
     * @return {jQuery|object}
     */
    function renameElementTag(element, toType, imageBase) {
        var attributes = element.prop('attributes'),
            target = $('<' + toType + '>');

        $.each(attributes, function(index, attribute) {
            // build srcset with image base
            if (attribute.name === 'srcset' || attribute.name === srcAttr) {
                attribute.value = getCorrectedSrcSet(attribute.value, imageBase);
            }

            target.attr(attribute.name, attribute.value);
        });

        element.replaceWith(target);
        return target;
    }

    /**
     * create a new image element inside parent element
     * @param {jQuery|object} parent
     * @param {string} src
     * @param {function} response
     * @return void
     */
    function createImageObject(parent, src, response) {
        // create image tag
        var imageObj = $('<img>')

        // create image tag an bind callbacks for correct response
        .one('load', function() {
            response(true);
        })
        .one('error', function() {
            response(false);
        })

        // set into picture element
        .appendTo(parent)

        // set src attribute at last to prevent early kick-in
        .attr('src', src);

        // call after load even on cached image
        imageObj.complete && imageObj.load(); // jshint ignore : line
    }

    /**
     * prepend image base to all srcset entries
     * @param {string} srcset
     * @param {string} imageBase
     * @returns {string}
     */
    function getCorrectedSrcSet(srcset, imageBase) {
        if (imageBase) {
            // trim, remove unnecessary spaces and split entries
            var entries = srcset.split(',');
            srcset = '';

            for (var i = 0, l = entries.length; i < l; i++) {
                srcset += imageBase + entries[i].trim() + (i !== l - 1 ? ',' : '');
            }
        }

        return srcset;
    }
})(window.jQuery || window.Zepto);

/*!
 * jQuery & Zepto Lazy - Script Plugin - v1.2
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // loads javascript files for script tags, like:
    // <script data-src="file.js" type="text/javascript"></script>
    $.lazy(['js', 'javascript', 'script'], 'script', function(element, response) {
        if (element[0].tagName.toLowerCase() === 'script') {
            element.attr('src', element.attr('data-src'));

            // remove attribute
            if (this.config('removeAttribute')) {
                element.removeAttr('data-src');
            }
        }
        else {
            // use response function for Zepto
            response(false);
        }
    });
})(window.jQuery || window.Zepto);

/*!
 * jQuery & Zepto Lazy - Vimeo Plugin - v1.1
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // load vimeo video iframe, like:
    // <iframe data-loader="vimeo" data-src="176894130" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    $.lazy('vimeo', function(element, response) {
        if (element[0].tagName.toLowerCase() === 'iframe') {
            // pass source to iframe
            element.attr('src', 'https://player.vimeo.com/video/' + element.attr('data-src'));

            // remove attribute
            if (this.config('removeAttribute')) {
                element.removeAttr('data-src');
            }
        }

        else {
            // pass error state
            // use response function for Zepto
            response(false);
        }
    });
})(window.jQuery || window.Zepto);

/*!
 * jQuery & Zepto Lazy - YouTube Plugin - v1.5
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 */
;(function($) {
    // load youtube video iframe, like:
    // <iframe data-loader="yt" data-src="1AYGnw6MwFM" data-nocookie="1" width="560" height="315" frameborder="0" allowfullscreen></iframe>
    $.lazy(['yt', 'youtube'], function(element, response) {
        if (element[0].tagName.toLowerCase() === 'iframe') {
            // pass source to iframe
            var noCookie = /1|true/.test(element.attr('data-nocookie'));
            element.attr('src', 'https://www.youtube' + (noCookie ? '-nocookie' : '') + '.com/embed/' + element.attr('data-src') + '?rel=0&amp;showinfo=0');

            // remove attribute
            if (this.config('removeAttribute')) {
                element.removeAttr('data-src');
            }
        }

        else {
            // pass error state
            response(false);
        }
    });
})(window.jQuery || window.Zepto);
},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvc2NyaXB0cy9mdW5jdGlvbnMvZnVuY3Rpb25zLmpzIiwiYXBwL3NjcmlwdHMvbW9kdWxlcy9sYXp5LmpzIiwibm9kZV9tb2R1bGVzL2pxdWVyeS1sYXp5L2pxdWVyeS5sYXp5LmpzIiwibm9kZV9tb2R1bGVzL2pxdWVyeS1sYXp5L2pxdWVyeS5sYXp5LnBsdWdpbnMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FDQUE7O0FBRUEsSUFBSSxXQUFXLFFBQVEsYUFBUixDQUFmO0FBQUEsSUFDSSxpQkFBaUIsUUFBUSx1REFBUixDQURyQjtBQUFBLElBRUksWUFBWSxRQUFRLGlCQUFSLENBRmhCOztBQUlBLElBQUksYUFBYTtBQUNmLGFBQVcsWUFBWTtBQUNyQixTQUFLLFFBQUw7QUFDRCxHQUhjO0FBSWYsWUFBVSxZQUFZO0FBQ3BCLFNBQUssUUFBTDtBQUNBLFNBQUssT0FBTDtBQUNELEdBUGM7QUFRZixjQUFZLFlBQVUsQ0FDckIsQ0FUYztBQVVmLGlCQUFlLFlBQVUsQ0FDeEIsQ0FYYztBQVlmLGNBQVksWUFBVTtBQUNwQixTQUFLLFdBQUw7QUFDRCxHQWRjO0FBZWYsWUFBVSxZQUFVLENBQ25CLENBaEJjO0FBaUJmLFdBQVMsWUFBVTtBQUNqQixNQUFFLFdBQUYsRUFBZSxJQUFmLENBQW9CLFlBQVc7QUFDN0IsVUFBSSxhQUFhLEVBQUUsSUFBRixFQUFRLE9BQVIsQ0FBZ0IsU0FBaEIsQ0FBakI7QUFDQSxVQUFJLFdBQVcsRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLE1BQWIsQ0FBZjtBQUNBLFVBQUksYUFBYyxPQUFPLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxRQUFiLENBQVAsS0FBZ0MsV0FBakMsR0FBaUQsRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFFBQWIsQ0FBakQsR0FBMEUsRUFBM0Y7QUFDQSxVQUFHLGFBQWEsU0FBYixJQUEwQixhQUFXLGNBQXhDLEVBQXdEO0FBQ3RELG1CQUFXLFFBQVgsQ0FBb0IsU0FBcEIsRUFBK0IsS0FBL0IsQ0FBcUMsRUFBQyxRQUFPLFFBQVIsRUFBaUIsVUFBUyxVQUExQixFQUFyQyxFQUEyRSxVQUFTLENBQVQsRUFBVztBQUNwRixZQUFFLGNBQUY7QUFDQSxjQUFHLEVBQUUsSUFBRixDQUFPLFFBQVAsS0FBa0IsUUFBckIsRUFBOEI7QUFDNUIsbUJBQU8sSUFBUCxDQUFZLEVBQUUsSUFBRixDQUFPLE1BQW5CO0FBQ0QsV0FGRCxNQUVPO0FBQ0wsbUJBQU8sUUFBUCxDQUFnQixJQUFoQixHQUF1QixFQUFFLElBQUYsQ0FBTyxNQUE5QjtBQUNEO0FBQ0YsU0FQRDtBQVFEO0FBQ0YsS0FkRDtBQWVELEdBakNjO0FBa0NmLFlBQVUsWUFBVTtBQUNsQixjQUFVLFFBQVY7QUFDRCxHQXBDYztBQXFDZixlQUFhLFlBQVU7QUFDckIsUUFBSSxFQUFFLE1BQUYsRUFBVSxTQUFWLEtBQXdCLENBQTVCLEVBQStCO0FBQzdCLFFBQUUsU0FBRixFQUFhLFFBQWIsQ0FBc0IsVUFBdEI7QUFDRCxLQUZELE1BRU87QUFDTCxRQUFFLFNBQUYsRUFBYSxXQUFiLENBQXlCLFVBQXpCO0FBQ0Q7QUFDRixHQTNDYztBQTRDZixZQUFVLFlBQVU7QUFDbEIsTUFBRSx3QkFBRixFQUE0QixFQUE1QixDQUErQixPQUEvQixFQUF3QyxZQUFXO0FBQ2pELFFBQUUsdUNBQUYsRUFBMkMsV0FBM0MsQ0FBdUQsZ0JBQXZEO0FBQ0EsUUFBRSxNQUFGLEVBQVUsV0FBVixDQUFzQixXQUF0QjtBQUNELEtBSEQ7QUFJRCxHQWpEYztBQWtEZixvQkFBa0IsWUFBVTtBQUMxQixTQUFLLFFBQUw7QUFDRDtBQXBEYyxDQUFqQjs7QUF1REEsT0FBTyxVQUFQLEdBQW9CLFVBQXBCOztBQUVDLGFBQVk7O0FBRVgsYUFBVyxTQUFYOztBQUVBLElBQUcsTUFBSCxFQUFZLEVBQVosQ0FBZSxNQUFmLEVBQXVCLFlBQVc7QUFDaEMsTUFBRSxrQkFBRixFQUFzQixRQUF0QixDQUErQixRQUEvQjtBQUNBLGVBQVcsUUFBWDtBQUNELEdBSEQ7O0FBS0EsSUFBRSxNQUFGLEVBQVUsTUFBVixDQUFpQixZQUFVO0FBQ3pCLGVBQVcsVUFBWDtBQUNELEdBRkQ7O0FBS0EsTUFBRyxPQUFPLEVBQUUsTUFBRixFQUFVLFNBQWpCLEtBQStCLFVBQWxDLEVBQTZDO0FBQzNDLE1BQUUsTUFBRixFQUFVLFNBQVYsQ0FBb0IsWUFBWTtBQUM5QixpQkFBVyxhQUFYO0FBQ0QsS0FGRDtBQUdELEdBSkQsTUFJTztBQUNMLE1BQUUsTUFBRixFQUFVLE1BQVYsQ0FBaUIsWUFBVTtBQUN6QixpQkFBVyxhQUFYO0FBQ0QsS0FGRDtBQUdEO0FBQ0QsSUFBRSxNQUFGLEVBQVUsTUFBVixDQUFpQixZQUFVO0FBQ3pCLGVBQVcsVUFBWDtBQUNELEdBRkQ7O0FBSUEsSUFBRSxNQUFGLEVBQVUsRUFBVixDQUFhLFFBQWIsRUFBdUIsWUFBVTtBQUMvQixlQUFXLFFBQVg7QUFDRCxHQUZEO0FBR0QsQ0E5QkEsR0FBRDs7Ozs7QUMvREEsT0FBTyxPQUFQLEdBQWlCO0FBQ2hCLFdBQVUsWUFBVztBQUNwQixJQUFFLE9BQUYsRUFBVyxJQUFYLENBQWdCO0FBQ2YsY0FBVyxHQURJO0FBRWYsV0FBUSxRQUZPO0FBR2YsZUFBWSxHQUhHO0FBSWYsYUFBVSxHQUpLO0FBS2YsU0FBTSxPQUxTO0FBTWYsZ0JBQWEsaWJBTkU7QUFPZixZQUFTLFVBQVMsT0FBVCxFQUFrQjtBQUNqQixZQUFRLEdBQVIsQ0FBWSxtQkFBbUIsUUFBUSxJQUFSLENBQWEsS0FBYixDQUEvQjtBQUNIO0FBVFEsR0FBaEI7QUFXQTtBQWJlLENBQWpCOzs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3YyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGxhenlsb2FkID0gcmVxdWlyZSgnanF1ZXJ5LWxhenknKSxcclxuICAgIGxhenlsb2FkUGx1Z2luID0gcmVxdWlyZSgnLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2pxdWVyeS1sYXp5L2pxdWVyeS5sYXp5LnBsdWdpbnMnKSxcclxuICAgIGltZ0xvYWRlciA9IHJlcXVpcmUoJy4uL21vZHVsZXMvbGF6eScpO1xyXG5cclxudmFyIHNpdGVHbG9iYWwgPSB7XHJcbiAgaW5pdFJlYWR5OiBmdW5jdGlvbiAoKSB7XHJcbiAgICB0aGlzLm1lbnVJbml0KCk7IFxyXG4gIH0sXHJcbiAgaW5pdExvYWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgIHRoaXMubGF6eUxvYWQoKTtcclxuICAgIHRoaXMubGlua0FsbCgpO1xyXG4gIH0sXHJcbiAgaW5pdFJlc2l6ZTogZnVuY3Rpb24oKXtcclxuICB9LFxyXG4gIGluaXRSZXNpemVFbmQ6IGZ1bmN0aW9uKCl7XHJcbiAgfSxcclxuICBpbml0U2Nyb2xsOiBmdW5jdGlvbigpe1xyXG4gICAgdGhpcy5oZWFkZXJDbGFzcygpO1xyXG4gIH0sXHJcbiAgb25TY3JvbGw6IGZ1bmN0aW9uKCl7XHJcbiAgfSxcclxuICBsaW5rQWxsOiBmdW5jdGlvbigpe1xyXG4gICAgJCgnLmxpbmtGcm9tJykuZWFjaChmdW5jdGlvbigpIHsgXHJcbiAgICAgIHZhciBlbGVtVG9MaW5rID0gJCh0aGlzKS5wYXJlbnRzKCcubGlua1RvJyk7XHJcbiAgICAgIHZhciBwYWdlTGluayA9ICQodGhpcykuYXR0cignaHJlZicpO1xyXG4gICAgICB2YXIgbGlua1RhcmdldCA9ICh0eXBlb2YgJCh0aGlzKS5hdHRyKCd0YXJnZXQnKSE9PSd1bmRlZmluZWQnKSA/ICAkKHRoaXMpLmF0dHIoJ3RhcmdldCcpIDogJycgO1xyXG4gICAgICBpZihwYWdlTGluayAhPT0gdW5kZWZpbmVkICYmIHBhZ2VMaW5rIT09J2phdmFzY3JpcHQ6OycpIHtcclxuICAgICAgICBlbGVtVG9MaW5rLmFkZENsYXNzKCdwb2ludGVyJykuY2xpY2soe215TGluazpwYWdlTGluayxteVRhcmdldDpsaW5rVGFyZ2V0fSxmdW5jdGlvbihlKXtcclxuICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgIGlmKGUuZGF0YS5teVRhcmdldD09PSdfYmxhbmsnKXtcclxuICAgICAgICAgICAgd2luZG93Lm9wZW4oZS5kYXRhLm15TGluayk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGUuZGF0YS5teUxpbms7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pOyBcclxuICB9LFxyXG4gIGxhenlMb2FkOiBmdW5jdGlvbigpe1xyXG4gICAgaW1nTG9hZGVyLmxhenlJbml0KCk7XHJcbiAgfSxcclxuICBoZWFkZXJDbGFzczogZnVuY3Rpb24oKXtcclxuICAgIGlmICgkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPiAwKSB7XHJcbiAgICAgICQoJy5oZWFkZXInKS5hZGRDbGFzcygnc2Nyb2xsZWQnKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICQoJy5oZWFkZXInKS5yZW1vdmVDbGFzcygnc2Nyb2xsZWQnKTtcclxuICAgIH1cclxuICB9LFxyXG4gIG1lbnVJbml0OiBmdW5jdGlvbigpe1xyXG4gICAgJCgnLmhlYWRlcl9faGFtYnVyZ2VyV3JhcCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAkKCcuaGVhZGVyX19oYW1idXJnZXIsIC5oZWFkZXJfX21lbnVXcmFwJykudG9nZ2xlQ2xhc3MoJ2lzLWFjdGl2ZSBvcGVuJyk7XHJcbiAgICAgICQoJ2h0bWwnKS50b2dnbGVDbGFzcygnbWVudS1vcGVuJyk7XHJcbiAgICB9KVxyXG4gIH0sXHJcbiAgcmVidWlsZEFsbEV2ZW50czogZnVuY3Rpb24oKXtcclxuICAgIHRoaXMuaW5pdExvYWQoKTtcclxuICB9LFxyXG59O1xyXG5cclxuZ2xvYmFsLnNpdGVHbG9iYWwgPSBzaXRlR2xvYmFsO1xyXG5cclxuKGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgc2l0ZUdsb2JhbC5pbml0UmVhZHkoKTtcclxuXHJcbiAgJCggd2luZG93ICkub24oJ2xvYWQnLCBmdW5jdGlvbigpIHtcclxuICAgICQoJ2JvZHksICNwcmVsb2FkZXInKS5hZGRDbGFzcygnbG9hZGVkJyk7XHJcbiAgICBzaXRlR2xvYmFsLmluaXRMb2FkKCk7XHJcbiAgfSk7XHJcblxyXG4gICQod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKXtcclxuICAgIHNpdGVHbG9iYWwuaW5pdFJlc2l6ZSgpO1xyXG4gIH0pO1xyXG5cclxuXHJcbiAgaWYodHlwZW9mICQod2luZG93KS5yZXNpemVlbmQgPT09ICdmdW5jdGlvbicpe1xyXG4gICAgJCh3aW5kb3cpLnJlc2l6ZWVuZChmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHNpdGVHbG9iYWwuaW5pdFJlc2l6ZUVuZCgpO1xyXG4gICAgfSk7XHJcbiAgfSBlbHNlIHtcclxuICAgICQod2luZG93KS5yZXNpemUoZnVuY3Rpb24oKXtcclxuICAgICAgc2l0ZUdsb2JhbC5pbml0UmVzaXplRW5kKCk7XHJcbiAgICB9KTtcclxuICB9XHJcbiAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpe1xyXG4gICAgc2l0ZUdsb2JhbC5pbml0U2Nyb2xsKCk7XHJcbiAgfSk7XHJcblxyXG4gICQod2luZG93KS5vbignc2Nyb2xsJywgZnVuY3Rpb24oKXtcclxuICAgIHNpdGVHbG9iYWwub25TY3JvbGwoKTtcclxuICB9KVxyXG59KCkpO1xyXG5cclxuXHJcblxyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHtcclxuXHRsYXp5SW5pdDogZnVuY3Rpb24gKCl7ICBcclxuXHRcdCQoJy5sYXp5JykuTGF6eSh7XHJcblx0XHRcdHRocmVzaG9sZDogNjAwLFxyXG5cdFx0XHRlZmZlY3Q6ICdmYWRlSW4nLFxyXG5cdFx0XHRlZmZlY3RUaW1lOiA1MDAsXHJcblx0XHRcdHRocm90dGxlOiAxNTAsXHJcblx0XHRcdGJpbmQ6ICdldmVudCcsXHJcblx0XHRcdHBsYWNlaG9sZGVyOiBcImRhdGE6aW1hZ2UvanBlZztiYXNlNjQsLzlqLzRBQVFTa1pKUmdBQkFnQUFaQUJrQUFELzdBQVJSSFZqYTNrQUFRQUVBQUFBUmdBQS8rNEFEa0ZrYjJKbEFHVEFBQUFBQWYvYkFJUUFCQU1EQXdNREJBTURCQVlFQXdRR0J3VUVCQVVIQ0FZR0J3WUdDQW9JQ1FrSkNRZ0tDZ3dNREF3TUNnd01EUTBNREJFUkVSRVJGQlFVRkJRVUZCUVVGQUVFQlFVSUJ3Z1BDZ29QRkE0T0RoUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVLzhBQUVRZ0FDZ0FLQXdFUkFBSVJBUU1SQWYvRUFFc0FBUUVBQUFBQUFBQUFBQUFBQUFBQUFBQUlBUUVBQUFBQUFBQUFBQUFBQUFBQUFBQUFFQUVBQUFBQUFBQUFBQUFBQUFBQUFBQUFFUUVBQUFBQUFBQUFBQUFBQUFBQUFBQUEvOW9BREFNQkFBSVJBeEVBUHdDeWdBQUFmLy9aXCIsXHJcblx0XHRcdG9uRXJyb3I6IGZ1bmN0aW9uKGVsZW1lbnQpIHtcclxuXHQgICAgICAgICAgICBjb25zb2xlLmxvZygnZXJyb3IgbG9hZGluZyAnICsgZWxlbWVudC5kYXRhKCdzcmMnKSk7XHJcblx0ICAgICAgICB9XHJcblx0XHR9KTsgIFxyXG5cdH1cclxufVxyXG5cclxuIiwiLyohXHJcbiAqIGpRdWVyeSAmIFplcHRvIExhenkgLSB2MS43LjEwXHJcbiAqIGh0dHA6Ly9qcXVlcnkuZWlzYmVoci5kZS9sYXp5L1xyXG4gKlxyXG4gKiBDb3B5cmlnaHQgMjAxMiAtIDIwMTgsIERhbmllbCAnRWlzYmVocicgS2VyblxyXG4gKlxyXG4gKiBEdWFsIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgYW5kIEdQTC0yLjAgbGljZW5zZXM6XHJcbiAqIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcbiAqIGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxcclxuICpcclxuICogJChcImltZy5sYXp5XCIpLmxhenkoKTtcclxuICovXHJcblxyXG47KGZ1bmN0aW9uKHdpbmRvdywgdW5kZWZpbmVkKSB7XHJcbiAgICBcInVzZSBzdHJpY3RcIjtcclxuXHJcbiAgICAvLyBub2luc3BlY3Rpb24gSlNVbnJlc29sdmVkVmFyaWFibGVcclxuICAgIC8qKlxyXG4gICAgICogbGlicmFyeSBpbnN0YW5jZSAtIGhlcmUgYW5kIG5vdCBpbiBjb25zdHJ1Y3QgdG8gYmUgc2hvcnRlciBpbiBtaW5pbWl6YXRpb25cclxuICAgICAqIEByZXR1cm4gdm9pZFxyXG4gICAgICovXHJcbiAgICB2YXIgJCA9IHdpbmRvdy5qUXVlcnkgfHwgd2luZG93LlplcHRvLFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogdW5pcXVlIHBsdWdpbiBpbnN0YW5jZSBpZCBjb3VudGVyXHJcbiAgICAgKiBAdHlwZSB7bnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBsYXp5SW5zdGFuY2VJZCA9IDAsXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBoZWxwZXIgdG8gcmVnaXN0ZXIgd2luZG93IGxvYWQgZm9yIGpRdWVyeSAzXHJcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cclxuICAgICAqLyAgICBcclxuICAgIHdpbmRvd0xvYWRlZCA9IGZhbHNlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogbWFrZSBsYXp5IGF2YWlsYWJsZSB0byBqcXVlcnkgLSBhbmQgbWFrZSBpdCBhIGJpdCBtb3JlIGNhc2UtaW5zZW5zaXRpdmUgOilcclxuICAgICAqIEBhY2Nlc3MgcHVibGljXHJcbiAgICAgKiBAdHlwZSB7ZnVuY3Rpb259XHJcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gc2V0dGluZ3NcclxuICAgICAqIEByZXR1cm4ge0xhenlQbHVnaW59XHJcbiAgICAgKi9cclxuICAgICQuZm4uTGF6eSA9ICQuZm4ubGF6eSA9IGZ1bmN0aW9uKHNldHRpbmdzKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBMYXp5UGx1Z2luKHRoaXMsIHNldHRpbmdzKTtcclxuICAgIH07XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBoZWxwZXIgdG8gYWRkIHBsdWdpbnMgdG8gbGF6eSBwcm90b3R5cGUgY29uZmlndXJhdGlvblxyXG4gICAgICogQGFjY2VzcyBwdWJsaWNcclxuICAgICAqIEB0eXBlIHtmdW5jdGlvbn1cclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfEFycmF5fSBuYW1lc1xyXG4gICAgICogQHBhcmFtIHtzdHJpbmd8QXJyYXl8ZnVuY3Rpb259IFtlbGVtZW50c11cclxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IGxvYWRlclxyXG4gICAgICogQHJldHVybiB2b2lkXHJcbiAgICAgKi9cclxuICAgICQuTGF6eSA9ICQubGF6eSA9IGZ1bmN0aW9uKG5hbWVzLCBlbGVtZW50cywgbG9hZGVyKSB7XHJcbiAgICAgICAgLy8gbWFrZSBzZWNvbmQgcGFyYW1ldGVyIG9wdGlvbmFsXHJcbiAgICAgICAgaWYgKCQuaXNGdW5jdGlvbihlbGVtZW50cykpIHtcclxuICAgICAgICAgICAgbG9hZGVyID0gZWxlbWVudHM7XHJcbiAgICAgICAgICAgIGVsZW1lbnRzID0gW107XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBleGl0IGhlcmUgaWYgcGFyYW1ldGVyIGlzIG5vdCBhIGNhbGxhYmxlIGZ1bmN0aW9uXHJcbiAgICAgICAgaWYgKCEkLmlzRnVuY3Rpb24obG9hZGVyKSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBtYWtlIHBhcmFtZXRlcnMgYW4gYXJyYXkgb2YgbmFtZXMgdG8gYmUgc3VyZVxyXG4gICAgICAgIG5hbWVzID0gJC5pc0FycmF5KG5hbWVzKSA/IG5hbWVzIDogW25hbWVzXTtcclxuICAgICAgICBlbGVtZW50cyA9ICQuaXNBcnJheShlbGVtZW50cykgPyBlbGVtZW50cyA6IFtlbGVtZW50c107XHJcblxyXG4gICAgICAgIHZhciBjb25maWcgPSBMYXp5UGx1Z2luLnByb3RvdHlwZS5jb25maWcsXHJcbiAgICAgICAgICAgIGZvcmNlZCA9IGNvbmZpZy5fZiB8fCAoY29uZmlnLl9mID0ge30pO1xyXG5cclxuICAgICAgICAvLyBhZGQgdGhlIGxvYWRlciBwbHVnaW4gZm9yIGV2ZXJ5IG5hbWVcclxuICAgICAgICBmb3IgKHZhciBpID0gMCwgbCA9IG5hbWVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xyXG4gICAgICAgICAgICBpZiAoY29uZmlnW25hbWVzW2ldXSA9PT0gdW5kZWZpbmVkIHx8ICQuaXNGdW5jdGlvbihjb25maWdbbmFtZXNbaV1dKSkge1xyXG4gICAgICAgICAgICAgICAgY29uZmlnW25hbWVzW2ldXSA9IGxvYWRlcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gYWRkIGZvcmNlZCBlbGVtZW50cyBsb2FkZXJcclxuICAgICAgICBmb3IgKHZhciBjID0gMCwgYSA9IGVsZW1lbnRzLmxlbmd0aDsgYyA8IGE7IGMrKykge1xyXG4gICAgICAgICAgICBmb3JjZWRbZWxlbWVudHNbY11dID0gbmFtZXNbMF07XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIGNvbnRhaW5zIGFsbCBsb2dpYyBhbmQgdGhlIHdob2xlIGVsZW1lbnQgaGFuZGxpbmdcclxuICAgICAqIGlzIHBhY2tlZCBpbiBhIHByaXZhdGUgZnVuY3Rpb24gb3V0c2lkZSBjbGFzcyB0byByZWR1Y2UgbWVtb3J5IHVzYWdlLCBiZWNhdXNlIGl0IHdpbGwgbm90IGJlIGNyZWF0ZWQgb24gZXZlcnkgcGx1Z2luIGluc3RhbmNlXHJcbiAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAqIEB0eXBlIHtmdW5jdGlvbn1cclxuICAgICAqIEBwYXJhbSB7TGF6eVBsdWdpbn0gaW5zdGFuY2VcclxuICAgICAqIEBwYXJhbSB7b2JqZWN0fSBjb25maWdcclxuICAgICAqIEBwYXJhbSB7b2JqZWN0fEFycmF5fSBpdGVtc1xyXG4gICAgICogQHBhcmFtIHtvYmplY3R9IGV2ZW50c1xyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWVzcGFjZVxyXG4gICAgICogQHJldHVybiB2b2lkXHJcbiAgICAgKi9cclxuICAgIGZ1bmN0aW9uIF9leGVjdXRlTGF6eShpbnN0YW5jZSwgY29uZmlnLCBpdGVtcywgZXZlbnRzLCBuYW1lc3BhY2UpIHtcclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBhIGhlbHBlciB0byB0cmlnZ2VyIHRoZSAnb25GaW5pc2hlZEFsbCcgY2FsbGJhY2sgYWZ0ZXIgYWxsIG90aGVyIGV2ZW50c1xyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEB0eXBlIHtudW1iZXJ9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgdmFyIF9hd2FpdGluZ0FmdGVyTG9hZCA9IDAsXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIHZpc2libGUgY29udGVudCB3aWR0aFxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEB0eXBlIHtudW1iZXJ9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX2FjdHVhbFdpZHRoID0gLTEsXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIHZpc2libGUgY29udGVudCBoZWlnaHRcclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAdHlwZSB7bnVtYmVyfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9hY3R1YWxIZWlnaHQgPSAtMSxcclxuXHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogZGV0ZXJtaW5lIHBvc3NpYmx5IGRldGVjdGVkIGhpZ2ggcGl4ZWwgZGVuc2l0eVxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEB0eXBlIHtib29sZWFufVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9pc1JldGluYURpc3BsYXkgPSBmYWxzZSwgXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGRpY3Rpb25hcnkgZW50cnkgZm9yIGJldHRlciBtaW5pbWl6YXRpb25cclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAdHlwZSB7c3RyaW5nfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9hZnRlckxvYWQgPSAnYWZ0ZXJMb2FkJyxcclxuXHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogZGljdGlvbmFyeSBlbnRyeSBmb3IgYmV0dGVyIG1pbmltaXphdGlvblxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEB0eXBlIHtzdHJpbmd9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX2xvYWQgPSAnbG9hZCcsXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGRpY3Rpb25hcnkgZW50cnkgZm9yIGJldHRlciBtaW5pbWl6YXRpb25cclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAdHlwZSB7c3RyaW5nfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9lcnJvciA9ICdlcnJvcicsXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGRpY3Rpb25hcnkgZW50cnkgZm9yIGJldHRlciBtaW5pbWl6YXRpb25cclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAdHlwZSB7c3RyaW5nfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9pbWcgPSAnaW1nJyxcclxuXHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogZGljdGlvbmFyeSBlbnRyeSBmb3IgYmV0dGVyIG1pbmltaXphdGlvblxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEB0eXBlIHtzdHJpbmd9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX3NyYyA9ICdzcmMnLFxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBkaWN0aW9uYXJ5IGVudHJ5IGZvciBiZXR0ZXIgbWluaW1pemF0aW9uXHJcbiAgICAgICAgICogQGFjY2VzcyBwcml2YXRlXHJcbiAgICAgICAgICogQHR5cGUge3N0cmluZ31cclxuICAgICAgICAgKi9cclxuICAgICAgICBfc3Jjc2V0ID0gJ3NyY3NldCcsXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGRpY3Rpb25hcnkgZW50cnkgZm9yIGJldHRlciBtaW5pbWl6YXRpb25cclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAdHlwZSB7c3RyaW5nfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9zaXplcyA9ICdzaXplcycsXHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGRpY3Rpb25hcnkgZW50cnkgZm9yIGJldHRlciBtaW5pbWl6YXRpb25cclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAdHlwZSB7c3RyaW5nfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9iYWNrZ3JvdW5kSW1hZ2UgPSAnYmFja2dyb3VuZC1pbWFnZSc7XHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGluaXRpYWxpemUgcGx1Z2luXHJcbiAgICAgICAgICogYmluZCBsb2FkaW5nIHRvIGV2ZW50cyBvciBzZXQgZGVsYXkgdGltZSB0byBsb2FkIGFsbCBpdGVtcyBhdCBvbmNlXHJcbiAgICAgICAgICogQGFjY2VzcyBwcml2YXRlXHJcbiAgICAgICAgICogQHJldHVybiB2b2lkXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX2luaXRpYWxpemUoKSB7XHJcbiAgICAgICAgICAgIC8vIGRldGVjdCBhY3R1YWwgZGV2aWNlIHBpeGVsIHJhdGlvXHJcbiAgICAgICAgICAgIC8vIG5vaW5zcGVjdGlvbiBKU1VucmVzb2x2ZWRWYXJpYWJsZVxyXG4gICAgICAgICAgICBfaXNSZXRpbmFEaXNwbGF5ID0gd2luZG93LmRldmljZVBpeGVsUmF0aW8gPiAxO1xyXG5cclxuICAgICAgICAgICAgLy8gcHJlcGFyZSBhbGwgaW5pdGlhbCBpdGVtc1xyXG4gICAgICAgICAgICBpdGVtcyA9IF9wcmVwYXJlSXRlbXMoaXRlbXMpO1xyXG5cclxuICAgICAgICAgICAgLy8gaWYgZGVsYXkgdGltZSBpcyBzZXQgbG9hZCBhbGwgaXRlbXMgYXQgb25jZSBhZnRlciBkZWxheSB0aW1lXHJcbiAgICAgICAgICAgIGlmIChjb25maWcuZGVsYXkgPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICBfbGF6eUxvYWRJdGVtcyh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH0sIGNvbmZpZy5kZWxheSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGlmIG5vIGRlbGF5IGlzIHNldCBvciBjb21iaW5lIHVzYWdlIGlzIGFjdGl2ZSBiaW5kIGV2ZW50c1xyXG4gICAgICAgICAgICBpZiAoY29uZmlnLmRlbGF5IDwgMCB8fCBjb25maWcuY29tYmluZWQpIHtcclxuICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSB1bmlxdWUgZXZlbnQgZnVuY3Rpb25cclxuICAgICAgICAgICAgICAgIGV2ZW50cy5lID0gX3Rocm90dGxlKGNvbmZpZy50aHJvdHRsZSwgZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyByZXNldCBkZXRlY3RlZCB3aW5kb3cgc2l6ZSBvbiByZXNpemUgZXZlbnRcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQudHlwZSA9PT0gJ3Jlc2l6ZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgX2FjdHVhbFdpZHRoID0gX2FjdHVhbEhlaWdodCA9IC0xO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZXhlY3V0ZSAnbGF6eSBtYWdpYydcclxuICAgICAgICAgICAgICAgICAgICBfbGF6eUxvYWRJdGVtcyhldmVudC5hbGwpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gY3JlYXRlIGZ1bmN0aW9uIHRvIGFkZCBuZXcgaXRlbXMgdG8gaW5zdGFuY2VcclxuICAgICAgICAgICAgICAgIGV2ZW50cy5hID0gZnVuY3Rpb24oYWRkaXRpb25hbEl0ZW1zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWRkaXRpb25hbEl0ZW1zID0gX3ByZXBhcmVJdGVtcyhhZGRpdGlvbmFsSXRlbXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zLnB1c2guYXBwbHkoaXRlbXMsIGFkZGl0aW9uYWxJdGVtcyk7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSBmdW5jdGlvbiB0byBnZXQgYWxsIGluc3RhbmNlIGl0ZW1zIGxlZnRcclxuICAgICAgICAgICAgICAgIGV2ZW50cy5nID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZmlsdGVyIGxvYWRlZCBpdGVtcyBiZWZvcmUgcmV0dXJuIGluIGNhc2UgaW50ZXJuYWwgZmlsdGVyIHdhcyBub3QgcnVubmluZyB1bnRpbCBub3dcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKGl0ZW1zID0gJChpdGVtcykuZmlsdGVyKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gISQodGhpcykuZGF0YShjb25maWcubG9hZGVkTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBjcmVhdGUgZnVuY3Rpb24gdG8gZm9yY2UgbG9hZGluZyBlbGVtZW50c1xyXG4gICAgICAgICAgICAgICAgZXZlbnRzLmYgPSBmdW5jdGlvbihmb3JjZWRJdGVtcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZm9yY2VkSXRlbXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gb25seSBoYW5kbGUgaXRlbSBpZiBhdmFpbGFibGUgaW4gY3VycmVudCBpbnN0YW5jZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c2UgYSBjb21wYXJlIGZ1bmN0aW9uLCBiZWNhdXNlIFplcHRvIGNhbid0IGhhbmRsZSBvYmplY3QgcGFyYW1ldGVyIGZvciBmaWx0ZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdmFyIGl0ZW0gPSBpdGVtcy5maWx0ZXIoZm9yY2VkSXRlbXNbaV0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvKiBqc2hpbnQgbG9vcGZ1bmM6IHRydWUgKi9cclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGl0ZW0gPSBpdGVtcy5maWx0ZXIoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcyA9PT0gZm9yY2VkSXRlbXNbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGl0ZW0ubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfbGF6eUxvYWRJdGVtcyhmYWxzZSwgaXRlbSk7ICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGxvYWQgaW5pdGlhbCBpdGVtc1xyXG4gICAgICAgICAgICAgICAgX2xhenlMb2FkSXRlbXMoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBiaW5kIGxhenkgbG9hZCBmdW5jdGlvbnMgdG8gc2Nyb2xsIGFuZCByZXNpemUgZXZlbnRcclxuICAgICAgICAgICAgICAgIC8vIG5vaW5zcGVjdGlvbiBKU1VucmVzb2x2ZWRWYXJpYWJsZVxyXG4gICAgICAgICAgICAgICAgJChjb25maWcuYXBwZW5kU2Nyb2xsKS5vbignc2Nyb2xsLicgKyBuYW1lc3BhY2UgKyAnIHJlc2l6ZS4nICsgbmFtZXNwYWNlLCBldmVudHMuZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIHByZXBhcmUgaXRlbXMgYmVmb3JlIGhhbmRsZSB0aGVtXHJcbiAgICAgICAgICogQGFjY2VzcyBwcml2YXRlXHJcbiAgICAgICAgICogQHBhcmFtIHtBcnJheXxvYmplY3R8alF1ZXJ5fSBpdGVtc1xyXG4gICAgICAgICAqIEByZXR1cm4ge0FycmF5fG9iamVjdHxqUXVlcnl9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX3ByZXBhcmVJdGVtcyhpdGVtcykge1xyXG4gICAgICAgICAgICAvLyBmZXRjaCB1c2VkIGNvbmZpZ3VyYXRpb25zIGJlZm9yZSBsb29wc1xyXG4gICAgICAgICAgICB2YXIgZGVmYXVsdEltYWdlID0gY29uZmlnLmRlZmF1bHRJbWFnZSxcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyID0gY29uZmlnLnBsYWNlaG9sZGVyLFxyXG4gICAgICAgICAgICAgICAgaW1hZ2VCYXNlID0gY29uZmlnLmltYWdlQmFzZSxcclxuICAgICAgICAgICAgICAgIHNyY3NldEF0dHJpYnV0ZSA9IGNvbmZpZy5zcmNzZXRBdHRyaWJ1dGUsXHJcbiAgICAgICAgICAgICAgICBsb2FkZXJBdHRyaWJ1dGUgPSBjb25maWcubG9hZGVyQXR0cmlidXRlLFxyXG4gICAgICAgICAgICAgICAgZm9yY2VkVGFncyA9IGNvbmZpZy5fZiB8fCB7fTtcclxuXHJcbiAgICAgICAgICAgIC8vIGZpbHRlciBpdGVtcyBhbmQgb25seSBhZGQgdGhvc2Ugd2hvIG5vdCBoYW5kbGVkIHlldCBhbmQgZ290IG5lZWRlZCBhdHRyaWJ1dGVzIGF2YWlsYWJsZVxyXG4gICAgICAgICAgICBpdGVtcyA9ICQoaXRlbXMpLmZpbHRlcihmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgICB0YWcgPSBfZ2V0RWxlbWVudFRhZ05hbWUodGhpcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuICFlbGVtZW50LmRhdGEoY29uZmlnLmhhbmRsZWROYW1lKSAmJiBcclxuICAgICAgICAgICAgICAgICAgICAgICAoZWxlbWVudC5hdHRyKGNvbmZpZy5hdHRyaWJ1dGUpIHx8IGVsZW1lbnQuYXR0cihzcmNzZXRBdHRyaWJ1dGUpIHx8IGVsZW1lbnQuYXR0cihsb2FkZXJBdHRyaWJ1dGUpIHx8IGZvcmNlZFRhZ3NbdGFnXSAhPT0gdW5kZWZpbmVkKTtcclxuICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgIC8vIGFwcGVuZCBwbHVnaW4gaW5zdGFuY2UgdG8gYWxsIGVsZW1lbnRzXHJcbiAgICAgICAgICAgIC5kYXRhKCdwbHVnaW5fJyArIGNvbmZpZy5uYW1lLCBpbnN0YW5jZSk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMCwgbCA9IGl0ZW1zLmxlbmd0aDsgaSA8IGw7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSAkKGl0ZW1zW2ldKSxcclxuICAgICAgICAgICAgICAgICAgICB0YWcgPSBfZ2V0RWxlbWVudFRhZ05hbWUoaXRlbXNbaV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRJbWFnZUJhc2UgPSBlbGVtZW50LmF0dHIoY29uZmlnLmltYWdlQmFzZUF0dHJpYnV0ZSkgfHwgaW1hZ2VCYXNlO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGdlbmVyYXRlIGFuZCB1cGRhdGUgc291cmNlIHNldCBpZiBhbiBpbWFnZSBiYXNlIGlzIHNldFxyXG4gICAgICAgICAgICAgICAgaWYgKHRhZyA9PT0gX2ltZyAmJiBlbGVtZW50SW1hZ2VCYXNlICYmIGVsZW1lbnQuYXR0cihzcmNzZXRBdHRyaWJ1dGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5hdHRyKHNyY3NldEF0dHJpYnV0ZSwgX2dldENvcnJlY3RlZFNyY1NldChlbGVtZW50LmF0dHIoc3Jjc2V0QXR0cmlidXRlKSwgZWxlbWVudEltYWdlQmFzZSkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIGFkZCBsb2FkZXIgdG8gZm9yY2VkIGVsZW1lbnQgdHlwZXNcclxuICAgICAgICAgICAgICAgIGlmIChmb3JjZWRUYWdzW3RhZ10gIT09IHVuZGVmaW5lZCAmJiAhZWxlbWVudC5hdHRyKGxvYWRlckF0dHJpYnV0ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmF0dHIobG9hZGVyQXR0cmlidXRlLCBmb3JjZWRUYWdzW3RhZ10pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIHNldCBkZWZhdWx0IGltYWdlIG9uIGV2ZXJ5IGVsZW1lbnQgd2l0aG91dCBzb3VyY2VcclxuICAgICAgICAgICAgICAgIGlmICh0YWcgPT09IF9pbWcgJiYgZGVmYXVsdEltYWdlICYmICFlbGVtZW50LmF0dHIoX3NyYykpIHtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmF0dHIoX3NyYywgZGVmYXVsdEltYWdlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBzZXQgcGxhY2Vob2xkZXIgb24gZXZlcnkgZWxlbWVudCB3aXRob3V0IGJhY2tncm91bmQgaW1hZ2VcclxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHRhZyAhPT0gX2ltZyAmJiBwbGFjZWhvbGRlciAmJiAoIWVsZW1lbnQuY3NzKF9iYWNrZ3JvdW5kSW1hZ2UpIHx8IGVsZW1lbnQuY3NzKF9iYWNrZ3JvdW5kSW1hZ2UpID09PSAnbm9uZScpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5jc3MoX2JhY2tncm91bmRJbWFnZSwgXCJ1cmwoJ1wiICsgcGxhY2Vob2xkZXIgKyBcIicpXCIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gaXRlbXM7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiB0aGUgJ2xhenkgbWFnaWMnIC0gY2hlY2sgYWxsIGl0ZW1zXHJcbiAgICAgICAgICogQGFjY2VzcyBwcml2YXRlXHJcbiAgICAgICAgICogQHBhcmFtIHtib29sZWFufSBbYWxsSXRlbXNdXHJcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IFtmb3JjZWRdXHJcbiAgICAgICAgICogQHJldHVybiB2b2lkXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX2xhenlMb2FkSXRlbXMoYWxsSXRlbXMsIGZvcmNlZCkge1xyXG4gICAgICAgICAgICAvLyBza2lwIGlmIG5vIGl0ZW1zIHdoZXJlIGxlZnRcclxuICAgICAgICAgICAgaWYgKCFpdGVtcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIC8vIGRlc3Ryb3kgaW5zdGFuY2UgaWYgb3B0aW9uIGlzIGVuYWJsZWRcclxuICAgICAgICAgICAgICAgIGlmIChjb25maWcuYXV0b0Rlc3Ryb3kpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBub2luc3BlY3Rpb24gSlNVbnJlc29sdmVkRnVuY3Rpb25cclxuICAgICAgICAgICAgICAgICAgICBpbnN0YW5jZS5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB2YXIgZWxlbWVudHMgPSBmb3JjZWQgfHwgaXRlbXMsXHJcbiAgICAgICAgICAgICAgICBsb2FkVHJpZ2dlcmVkID0gZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBpbWFnZUJhc2UgPSBjb25maWcuaW1hZ2VCYXNlIHx8ICcnLFxyXG4gICAgICAgICAgICAgICAgc3Jjc2V0QXR0cmlidXRlID0gY29uZmlnLnNyY3NldEF0dHJpYnV0ZSxcclxuICAgICAgICAgICAgICAgIGhhbmRsZWROYW1lID0gY29uZmlnLmhhbmRsZWROYW1lO1xyXG5cclxuICAgICAgICAgICAgLy8gbG9vcCBhbGwgYXZhaWxhYmxlIGl0ZW1zXHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZWxlbWVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIC8vIGl0ZW0gaXMgYXQgbGVhc3QgaW4gbG9hZGFibGUgYXJlYVxyXG4gICAgICAgICAgICAgICAgaWYgKGFsbEl0ZW1zIHx8IGZvcmNlZCB8fCBfaXNJbkxvYWRhYmxlQXJlYShlbGVtZW50c1tpXSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgZWxlbWVudCA9ICQoZWxlbWVudHNbaV0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWcgPSBfZ2V0RWxlbWVudFRhZ05hbWUoZWxlbWVudHNbaV0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhdHRyaWJ1dGUgPSBlbGVtZW50LmF0dHIoY29uZmlnLmF0dHJpYnV0ZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRJbWFnZUJhc2UgPSBlbGVtZW50LmF0dHIoY29uZmlnLmltYWdlQmFzZUF0dHJpYnV0ZSkgfHwgaW1hZ2VCYXNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjdXN0b21Mb2FkZXIgPSBlbGVtZW50LmF0dHIoY29uZmlnLmxvYWRlckF0dHJpYnV0ZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpcyBub3QgYWxyZWFkeSBoYW5kbGVkIFxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghZWxlbWVudC5kYXRhKGhhbmRsZWROYW1lKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBhbmQgaXMgdmlzaWJsZSBvciB2aXNpYmlsaXR5IGRvZXNuJ3QgbWF0dGVyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICghY29uZmlnLnZpc2libGVPbmx5IHx8IGVsZW1lbnQuaXMoJzp2aXNpYmxlJykpICYmIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gYW5kIGltYWdlIHNvdXJjZSBvciBzb3VyY2Ugc2V0IGF0dHJpYnV0ZSBpcyBhdmFpbGFibGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGF0dHJpYnV0ZSB8fCBlbGVtZW50LmF0dHIoc3Jjc2V0QXR0cmlidXRlKSkgJiYgKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gYW5kIGlzIGltYWdlIHRhZyB3aGVyZSBhdHRyaWJ1dGUgaXMgbm90IGVxdWFsIHNvdXJjZSBvciBzb3VyY2Ugc2V0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAodGFnID09PSBfaW1nICYmIChlbGVtZW50SW1hZ2VCYXNlICsgYXR0cmlidXRlICE9PSBlbGVtZW50LmF0dHIoX3NyYykgfHwgZWxlbWVudC5hdHRyKHNyY3NldEF0dHJpYnV0ZSkgIT09IGVsZW1lbnQuYXR0cihfc3Jjc2V0KSkpIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBvciBpcyBub24gaW1hZ2UgdGFnIHdoZXJlIGF0dHJpYnV0ZSBpcyBub3QgZXF1YWwgYmFja2dyb3VuZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHRhZyAhPT0gX2ltZyAmJiBlbGVtZW50SW1hZ2VCYXNlICsgYXR0cmlidXRlICE9PSBlbGVtZW50LmNzcyhfYmFja2dyb3VuZEltYWdlKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBvciBjdXN0b20gbG9hZGVyIGlzIGF2YWlsYWJsZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjdXN0b21Mb2FkZXIpKVxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gbWFyayBlbGVtZW50IGFsd2F5cyBhcyBoYW5kbGVkIGFzIHRoaXMgcG9pbnQgdG8gcHJldmVudCBkb3VibGUgaGFuZGxpbmdcclxuICAgICAgICAgICAgICAgICAgICAgICAgbG9hZFRyaWdnZXJlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZGF0YShoYW5kbGVkTmFtZSwgdHJ1ZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBsb2FkIGl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgX2hhbmRsZUl0ZW0oZWxlbWVudCwgdGFnLCBlbGVtZW50SW1hZ2VCYXNlLCBjdXN0b21Mb2FkZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gd2hlbiBzb21ldGhpbmcgd2FzIGxvYWRlZCByZW1vdmUgdGhlbSBmcm9tIHJlbWFpbmluZyBpdGVtc1xyXG4gICAgICAgICAgICBpZiAobG9hZFRyaWdnZXJlZCkge1xyXG4gICAgICAgICAgICAgICAgaXRlbXMgPSAkKGl0ZW1zKS5maWx0ZXIoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICEkKHRoaXMpLmRhdGEoaGFuZGxlZE5hbWUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGxvYWQgdGhlIGdpdmVuIGVsZW1lbnQgdGhlIGxhenkgd2F5XHJcbiAgICAgICAgICogQGFjY2VzcyBwcml2YXRlXHJcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IGVsZW1lbnRcclxuICAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gdGFnXHJcbiAgICAgICAgICogQHBhcmFtIHtzdHJpbmd9IGltYWdlQmFzZVxyXG4gICAgICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IFtjdXN0b21Mb2FkZXJdXHJcbiAgICAgICAgICogQHJldHVybiB2b2lkXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX2hhbmRsZUl0ZW0oZWxlbWVudCwgdGFnLCBpbWFnZUJhc2UsIGN1c3RvbUxvYWRlcikge1xyXG4gICAgICAgICAgICAvLyBpbmNyZW1lbnQgY291bnQgb2YgaXRlbXMgd2FpdGluZyBmb3IgYWZ0ZXIgbG9hZFxyXG4gICAgICAgICAgICArK19hd2FpdGluZ0FmdGVyTG9hZDtcclxuXHJcbiAgICAgICAgICAgIC8vIGV4dGVuZGVkIGVycm9yIGNhbGxiYWNrIGZvciBjb3JyZWN0ICdvbkZpbmlzaGVkQWxsJyBoYW5kbGluZ1xyXG4gICAgICAgICAgICB2YXIgZXJyb3JDYWxsYmFjayA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgX3RyaWdnZXJDYWxsYmFjaygnb25FcnJvcicsIGVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgX3JlZHVjZUF3YWl0aW5nKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gcHJldmVudCBmdXJ0aGVyIGNhbGxiYWNrIGNhbGxzXHJcbiAgICAgICAgICAgICAgICBlcnJvckNhbGxiYWNrID0gJC5ub29wO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgLy8gdHJpZ2dlciBmdW5jdGlvbiBiZWZvcmUgbG9hZGluZyBpbWFnZVxyXG4gICAgICAgICAgICBfdHJpZ2dlckNhbGxiYWNrKCdiZWZvcmVMb2FkJywgZWxlbWVudCk7XHJcblxyXG4gICAgICAgICAgICAvLyBmZXRjaCBhbGwgZG91YmxlIHVzZWQgZGF0YSBoZXJlIGZvciBiZXR0ZXIgY29kZSBtaW5pbWl6YXRpb25cclxuICAgICAgICAgICAgdmFyIHNyY0F0dHJpYnV0ZSA9IGNvbmZpZy5hdHRyaWJ1dGUsXHJcbiAgICAgICAgICAgICAgICBzcmNzZXRBdHRyaWJ1dGUgPSBjb25maWcuc3Jjc2V0QXR0cmlidXRlLFxyXG4gICAgICAgICAgICAgICAgc2l6ZXNBdHRyaWJ1dGUgPSBjb25maWcuc2l6ZXNBdHRyaWJ1dGUsXHJcbiAgICAgICAgICAgICAgICByZXRpbmFBdHRyaWJ1dGUgPSBjb25maWcucmV0aW5hQXR0cmlidXRlLFxyXG4gICAgICAgICAgICAgICAgcmVtb3ZlQXR0cmlidXRlID0gY29uZmlnLnJlbW92ZUF0dHJpYnV0ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRlZE5hbWUgPSBjb25maWcubG9hZGVkTmFtZSxcclxuICAgICAgICAgICAgICAgIGVsZW1lbnRSZXRpbmEgPSBlbGVtZW50LmF0dHIocmV0aW5hQXR0cmlidXRlKTtcclxuXHJcbiAgICAgICAgICAgIC8vIGhhbmRsZSBjdXN0b20gbG9hZGVyXHJcbiAgICAgICAgICAgIGlmIChjdXN0b21Mb2FkZXIpIHtcclxuICAgICAgICAgICAgICAgIC8vIG9uIGxvYWQgY2FsbGJhY2tcclxuICAgICAgICAgICAgICAgIHZhciBsb2FkQ2FsbGJhY2sgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyByZW1vdmUgYXR0cmlidXRlIGZyb20gZWxlbWVudFxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZW1vdmVBdHRyaWJ1dGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVBdHRyKGNvbmZpZy5sb2FkZXJBdHRyaWJ1dGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbWFyayBlbGVtZW50IGFzIGxvYWRlZFxyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuZGF0YShsb2FkZWROYW1lLCB0cnVlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gY2FsbCBhZnRlciBsb2FkIGV2ZW50XHJcbiAgICAgICAgICAgICAgICAgICAgX3RyaWdnZXJDYWxsYmFjayhfYWZ0ZXJMb2FkLCBlbGVtZW50KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVtb3ZlIGl0ZW0gZnJvbSB3YWl0aW5nIHF1ZXVlIGFuZCBwb3NzaWJseSB0cmlnZ2VyIGZpbmlzaGVkIGV2ZW50XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaXQncyBuZWVkZWQgdG8gYmUgYXN5bmNocm9ub3VzIHRvIHJ1biBhZnRlciBmaWx0ZXIgd2FzIGluIF9sYXp5TG9hZEl0ZW1zXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChfcmVkdWNlQXdhaXRpbmcsIDEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBwcmV2ZW50IGZ1cnRoZXIgY2FsbGJhY2sgY2FsbHNcclxuICAgICAgICAgICAgICAgICAgICBsb2FkQ2FsbGJhY2sgPSAkLm5vb3A7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGJpbmQgZXJyb3IgZXZlbnQgdG8gdHJpZ2dlciBjYWxsYmFjayBhbmQgcmVkdWNlIHdhaXRpbmcgYW1vdW50XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50Lm9mZihfZXJyb3IpLm9uZShfZXJyb3IsIGVycm9yQ2FsbGJhY2spXHJcblxyXG4gICAgICAgICAgICAgICAgLy8gYmluZCBhZnRlciBsb2FkIGNhbGxiYWNrIHRvIGVsZW1lbnRcclxuICAgICAgICAgICAgICAgIC5vbmUoX2xvYWQsIGxvYWRDYWxsYmFjayk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gdHJpZ2dlciBjdXN0b20gbG9hZGVyIGFuZCBoYW5kbGUgcmVzcG9uc2VcclxuICAgICAgICAgICAgICAgIGlmICghX3RyaWdnZXJDYWxsYmFjayhjdXN0b21Mb2FkZXIsIGVsZW1lbnQsIGZ1bmN0aW9uKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5vZmYoX2xvYWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsb2FkQ2FsbGJhY2soKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQub2ZmKF9lcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yQ2FsbGJhY2soKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQudHJpZ2dlcihfZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBoYW5kbGUgaW1hZ2VzXHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gY3JlYXRlIGltYWdlIG9iamVjdFxyXG4gICAgICAgICAgICAgICAgdmFyIGltYWdlT2JqID0gJChuZXcgSW1hZ2UoKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gYmluZCBlcnJvciBldmVudCB0byB0cmlnZ2VyIGNhbGxiYWNrIGFuZCByZWR1Y2Ugd2FpdGluZyBhbW91bnRcclxuICAgICAgICAgICAgICAgIGltYWdlT2JqLm9uZShfZXJyb3IsIGVycm9yQ2FsbGJhY2spXHJcblxyXG4gICAgICAgICAgICAgICAgLy8gYmluZCBhZnRlciBsb2FkIGNhbGxiYWNrIHRvIGltYWdlXHJcbiAgICAgICAgICAgICAgICAub25lKF9sb2FkLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyByZW1vdmUgZWxlbWVudCBmcm9tIHZpZXdcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmhpZGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc2V0IGltYWdlIGJhY2sgdG8gZWxlbWVudFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGRvIGl0IGFzIHNpbmdsZSAnYXR0cicgY2FsbHMsIHRvIGJlIHN1cmUgJ3NyYycgaXMgc2V0IGFmdGVyICdzcmNzZXQnXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRhZyA9PT0gX2ltZykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmF0dHIoX3NpemVzLCBpbWFnZU9iai5hdHRyKF9zaXplcykpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYXR0cihfc3Jjc2V0LCBpbWFnZU9iai5hdHRyKF9zcmNzZXQpKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoX3NyYywgaW1hZ2VPYmouYXR0cihfc3JjKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmNzcyhfYmFja2dyb3VuZEltYWdlLCBcInVybCgnXCIgKyBpbWFnZU9iai5hdHRyKF9zcmMpICsgXCInKVwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGJyaW5nIGl0IGJhY2sgd2l0aCBzb21lIGVmZmVjdCFcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50W2NvbmZpZy5lZmZlY3RdKGNvbmZpZy5lZmZlY3RUaW1lKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVtb3ZlIGF0dHJpYnV0ZSBmcm9tIGVsZW1lbnRcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVtb3ZlQXR0cmlidXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlQXR0cihzcmNBdHRyaWJ1dGUgKyAnICcgKyBzcmNzZXRBdHRyaWJ1dGUgKyAnICcgKyByZXRpbmFBdHRyaWJ1dGUgKyAnICcgKyBjb25maWcuaW1hZ2VCYXNlQXR0cmlidXRlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIG9ubHkgcmVtb3ZlICdzaXplcycgYXR0cmlidXRlLCBpZiBpdCB3YXMgYSBjdXN0b20gb25lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzaXplc0F0dHJpYnV0ZSAhPT0gX3NpemVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoc2l6ZXNBdHRyaWJ1dGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBtYXJrIGVsZW1lbnQgYXMgbG9hZGVkXHJcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5kYXRhKGxvYWRlZE5hbWUsIHRydWUpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBjYWxsIGFmdGVyIGxvYWQgZXZlbnRcclxuICAgICAgICAgICAgICAgICAgICBfdHJpZ2dlckNhbGxiYWNrKF9hZnRlckxvYWQsIGVsZW1lbnQpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBjbGVhbnVwIGltYWdlIG9iamVjdFxyXG4gICAgICAgICAgICAgICAgICAgIGltYWdlT2JqLnJlbW92ZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyByZW1vdmUgaXRlbSBmcm9tIHdhaXRpbmcgcXVldWUgYW5kIHBvc3NpYmx5IHRyaWdnZXIgZmluaXNoZWQgZXZlbnRcclxuICAgICAgICAgICAgICAgICAgICBfcmVkdWNlQXdhaXRpbmcoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIHNldCBzb3VyY2VzXHJcbiAgICAgICAgICAgICAgICAvLyBkbyBpdCBhcyBzaW5nbGUgJ2F0dHInIGNhbGxzLCB0byBiZSBzdXJlICdzcmMnIGlzIHNldCBhZnRlciAnc3Jjc2V0J1xyXG4gICAgICAgICAgICAgICAgdmFyIGltYWdlU3JjID0gKF9pc1JldGluYURpc3BsYXkgJiYgZWxlbWVudFJldGluYSA/IGVsZW1lbnRSZXRpbmEgOiBlbGVtZW50LmF0dHIoc3JjQXR0cmlidXRlKSkgfHwgJyc7XHJcbiAgICAgICAgICAgICAgICBpbWFnZU9iai5hdHRyKF9zaXplcywgZWxlbWVudC5hdHRyKHNpemVzQXR0cmlidXRlKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoX3NyY3NldCwgZWxlbWVudC5hdHRyKHNyY3NldEF0dHJpYnV0ZSkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKF9zcmMsIGltYWdlU3JjID8gaW1hZ2VCYXNlICsgaW1hZ2VTcmMgOiBudWxsKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBjYWxsIGFmdGVyIGxvYWQgZXZlbiBvbiBjYWNoZWQgaW1hZ2VcclxuICAgICAgICAgICAgICAgIGltYWdlT2JqLmNvbXBsZXRlICYmIGltYWdlT2JqLnRyaWdnZXIoX2xvYWQpOyAvLyBqc2hpbnQgaWdub3JlIDogbGluZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBjaGVjayBpZiB0aGUgZ2l2ZW4gZWxlbWVudCBpcyBpbnNpZGUgdGhlIGN1cnJlbnQgdmlld3BvcnQgb3IgdGhyZXNob2xkXHJcbiAgICAgICAgICogQGFjY2VzcyBwcml2YXRlXHJcbiAgICAgICAgICogQHBhcmFtIHtvYmplY3R9IGVsZW1lbnRcclxuICAgICAgICAgKiBAcmV0dXJuIHtib29sZWFufVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGZ1bmN0aW9uIF9pc0luTG9hZGFibGVBcmVhKGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgdmFyIGVsZW1lbnRCb3VuZCA9IGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXHJcbiAgICAgICAgICAgICAgICBkaXJlY3Rpb24gICAgPSBjb25maWcuc2Nyb2xsRGlyZWN0aW9uLFxyXG4gICAgICAgICAgICAgICAgdGhyZXNob2xkICAgID0gY29uZmlnLnRocmVzaG9sZCxcclxuICAgICAgICAgICAgICAgIHZlcnRpY2FsICAgICA9IC8vIGNoZWNrIGlmIGVsZW1lbnQgaXMgaW4gbG9hZGFibGUgYXJlYSBmcm9tIHRvcFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKChfZ2V0QWN0dWFsSGVpZ2h0KCkgKyB0aHJlc2hvbGQpID4gZWxlbWVudEJvdW5kLnRvcCkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNoZWNrIGlmIGVsZW1lbnQgaXMgZXZlbiBpbiBsb2FkYWJsZSBhcmUgZnJvbSBib3R0b21cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICgtdGhyZXNob2xkIDwgZWxlbWVudEJvdW5kLmJvdHRvbSksXHJcbiAgICAgICAgICAgICAgICBob3Jpem9udGFsICAgPSAvLyBjaGVjayBpZiBlbGVtZW50IGlzIGluIGxvYWRhYmxlIGFyZWEgZnJvbSBsZWZ0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoKF9nZXRBY3R1YWxXaWR0aCgpICsgdGhyZXNob2xkKSA+IGVsZW1lbnRCb3VuZC5sZWZ0KSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2hlY2sgaWYgZWxlbWVudCBpcyBldmVuIGluIGxvYWRhYmxlIGFyZWEgZnJvbSByaWdodFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKC10aHJlc2hvbGQgPCBlbGVtZW50Qm91bmQucmlnaHQpO1xyXG5cclxuICAgICAgICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gJ3ZlcnRpY2FsJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZlcnRpY2FsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKGRpcmVjdGlvbiA9PT0gJ2hvcml6b250YWwnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaG9yaXpvbnRhbDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHZlcnRpY2FsICYmIGhvcml6b250YWw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiByZWNlaXZlIHRoZSBjdXJyZW50IHZpZXdlZCB3aWR0aCBvZiB0aGUgYnJvd3NlclxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEByZXR1cm4ge251bWJlcn1cclxuICAgICAgICAgKi9cclxuICAgICAgICBmdW5jdGlvbiBfZ2V0QWN0dWFsV2lkdGgoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfYWN0dWFsV2lkdGggPj0gMCA/IF9hY3R1YWxXaWR0aCA6IChfYWN0dWFsV2lkdGggPSAkKHdpbmRvdykud2lkdGgoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiByZWNlaXZlIHRoZSBjdXJyZW50IHZpZXdlZCBoZWlnaHQgb2YgdGhlIGJyb3dzZXJcclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAcmV0dXJuIHtudW1iZXJ9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX2dldEFjdHVhbEhlaWdodCgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIF9hY3R1YWxIZWlnaHQgPj0gMCA/IF9hY3R1YWxIZWlnaHQgOiAoX2FjdHVhbEhlaWdodCA9ICQod2luZG93KS5oZWlnaHQoKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBnZXQgbG93ZXJjYXNlIHRhZyBuYW1lIG9mIGFuIGVsZW1lbnRcclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAcGFyYW0ge29iamVjdH0gZWxlbWVudFxyXG4gICAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX2dldEVsZW1lbnRUYWdOYW1lKGVsZW1lbnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnQudGFnTmFtZS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogcHJlcGVuZCBpbWFnZSBiYXNlIHRvIGFsbCBzcmNzZXQgZW50cmllc1xyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzcmNzZXRcclxuICAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gaW1hZ2VCYXNlXHJcbiAgICAgICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAgICAgKi9cclxuICAgICAgICBmdW5jdGlvbiBfZ2V0Q29ycmVjdGVkU3JjU2V0KHNyY3NldCwgaW1hZ2VCYXNlKSB7XHJcbiAgICAgICAgICAgIGlmIChpbWFnZUJhc2UpIHtcclxuICAgICAgICAgICAgICAgIC8vIHRyaW0sIHJlbW92ZSB1bm5lY2Vzc2FyeSBzcGFjZXMgYW5kIHNwbGl0IGVudHJpZXNcclxuICAgICAgICAgICAgICAgIHZhciBlbnRyaWVzID0gc3Jjc2V0LnNwbGl0KCcsJyk7XHJcbiAgICAgICAgICAgICAgICBzcmNzZXQgPSAnJztcclxuXHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMCwgbCA9IGVudHJpZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3Jjc2V0ICs9IGltYWdlQmFzZSArIGVudHJpZXNbaV0udHJpbSgpICsgKGkgIT09IGwgLSAxID8gJywnIDogJycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gc3Jjc2V0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogaGVscGVyIGZ1bmN0aW9uIHRvIHRocm90dGxlIGRvd24gZXZlbnQgdHJpZ2dlcmluZ1xyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEBwYXJhbSB7bnVtYmVyfSBkZWxheVxyXG4gICAgICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IGNhbGxiYWNrXHJcbiAgICAgICAgICogQHJldHVybiB7ZnVuY3Rpb259XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX3Rocm90dGxlKGRlbGF5LCBjYWxsYmFjaykge1xyXG4gICAgICAgICAgICB2YXIgdGltZW91dCxcclxuICAgICAgICAgICAgICAgIGxhc3RFeGVjdXRlID0gMDtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihldmVudCwgaWdub3JlVGhyb3R0bGUpIHtcclxuICAgICAgICAgICAgICAgIHZhciBlbGFwc2VkID0gK25ldyBEYXRlKCkgLSBsYXN0RXhlY3V0ZTtcclxuXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBydW4oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGFzdEV4ZWN1dGUgPSArbmV3IERhdGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBub2luc3BlY3Rpb24gSlNVbnJlc29sdmVkRnVuY3Rpb25cclxuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKGluc3RhbmNlLCBldmVudCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGltZW91dCAmJiBjbGVhclRpbWVvdXQodGltZW91dCk7IC8vIGpzaGludCBpZ25vcmUgOiBsaW5lXHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGVsYXBzZWQgPiBkZWxheSB8fCAhY29uZmlnLmVuYWJsZVRocm90dGxlIHx8IGlnbm9yZVRocm90dGxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcnVuKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aW1lb3V0ID0gc2V0VGltZW91dChydW4sIGRlbGF5IC0gZWxhcHNlZCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiByZWR1Y2UgY291bnQgb2YgYXdhaXRpbmcgZWxlbWVudHMgdG8gJ2FmdGVyTG9hZCcgZXZlbnQgYW5kIGZpcmUgJ29uRmluaXNoZWRBbGwnIGlmIHJlYWNoZWQgemVyb1xyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEByZXR1cm4gdm9pZFxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGZ1bmN0aW9uIF9yZWR1Y2VBd2FpdGluZygpIHtcclxuICAgICAgICAgICAgLS1fYXdhaXRpbmdBZnRlckxvYWQ7XHJcblxyXG4gICAgICAgICAgICAvLyBpZiBubyBpdGVtcyB3ZXJlIGxlZnQgdHJpZ2dlciBmaW5pc2hlZCBldmVudFxyXG4gICAgICAgICAgICBpZiAoIWl0ZW1zLmxlbmd0aCAmJiAhX2F3YWl0aW5nQWZ0ZXJMb2FkKSB7XHJcbiAgICAgICAgICAgICAgICBfdHJpZ2dlckNhbGxiYWNrKCdvbkZpbmlzaGVkQWxsJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIHNpbmdsZSBpbXBsZW1lbnRhdGlvbiB0byBoYW5kbGUgY2FsbGJhY2tzLCBwYXNzIGVsZW1lbnQgYW5kIHNldCAndGhpcycgdG8gY3VycmVudCBpbnN0YW5jZVxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfGZ1bmN0aW9ufSBjYWxsYmFja1xyXG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBbZWxlbWVudF1cclxuICAgICAgICAgKiBAcGFyYW0geyp9IFthcmdzXVxyXG4gICAgICAgICAqIEByZXR1cm4ge2Jvb2xlYW59XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZnVuY3Rpb24gX3RyaWdnZXJDYWxsYmFjayhjYWxsYmFjaywgZWxlbWVudCwgYXJncykge1xyXG4gICAgICAgICAgICBpZiAoKGNhbGxiYWNrID0gY29uZmlnW2NhbGxiYWNrXSkpIHtcclxuICAgICAgICAgICAgICAgIC8vIGpRdWVyeSdzIGludGVybmFsICckKGFyZ3VtZW50cykuc2xpY2UoMSknIGFyZSBjYXVzaW5nIHByb2JsZW1zIGF0IGxlYXN0IG9uIG9sZCBpUGFkc1xyXG4gICAgICAgICAgICAgICAgLy8gYmVsb3cgaXMgc2hvcnRoYW5kIG9mICdBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpJ1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2suYXBwbHkoaW5zdGFuY2UsIFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gaWYgZXZlbnQgZHJpdmVuIG9yIHdpbmRvdyBpcyBhbHJlYWR5IGxvYWRlZCBkb24ndCB3YWl0IGZvciBwYWdlIGxvYWRpbmdcclxuICAgICAgICBpZiAoY29uZmlnLmJpbmQgPT09ICdldmVudCcgfHwgd2luZG93TG9hZGVkKSB7XHJcbiAgICAgICAgICAgIF9pbml0aWFsaXplKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBvdGhlcndpc2UgbG9hZCBpbml0aWFsIGl0ZW1zIGFuZCBzdGFydCBsYXp5IGFmdGVyIHBhZ2UgbG9hZFxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBub2luc3BlY3Rpb24gSlNVbnJlc29sdmVkVmFyaWFibGVcclxuICAgICAgICAgICAgJCh3aW5kb3cpLm9uKF9sb2FkICsgJy4nICsgbmFtZXNwYWNlLCBfaW5pdGlhbGl6ZSk7XHJcbiAgICAgICAgfSAgXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBsYXp5IHBsdWdpbiBjbGFzcyBjb25zdHJ1Y3RvclxyXG4gICAgICogQGNvbnN0cnVjdG9yXHJcbiAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAqIEBwYXJhbSB7b2JqZWN0fSBlbGVtZW50c1xyXG4gICAgICogQHBhcmFtIHtvYmplY3R9IHNldHRpbmdzXHJcbiAgICAgKiBAcmV0dXJuIHtvYmplY3R8TGF6eVBsdWdpbn1cclxuICAgICAqL1xyXG4gICAgZnVuY3Rpb24gTGF6eVBsdWdpbihlbGVtZW50cywgc2V0dGluZ3MpIHtcclxuICAgICAgICAvKipcclxuICAgICAgICAgKiB0aGlzIGxhenkgcGx1Z2luIGluc3RhbmNlXHJcbiAgICAgICAgICogQGFjY2VzcyBwcml2YXRlXHJcbiAgICAgICAgICogQHR5cGUge29iamVjdHxMYXp5UGx1Z2lufExhenlQbHVnaW4ucHJvdG90eXBlfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHZhciBfaW5zdGFuY2UgPSB0aGlzLFxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiB0aGlzIGxhenkgcGx1Z2luIGluc3RhbmNlIGNvbmZpZ3VyYXRpb25cclxuICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgKiBAdHlwZSB7b2JqZWN0fVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9jb25maWcgPSAkLmV4dGVuZCh7fSwgX2luc3RhbmNlLmNvbmZpZywgc2V0dGluZ3MpLFxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBpbnN0YW5jZSBnZW5lcmF0ZWQgZXZlbnQgZXhlY3V0ZWQgb24gY29udGFpbmVyIHNjcm9sbCBvciByZXNpemVcclxuICAgICAgICAgKiBwYWNrZWQgaW4gYW4gb2JqZWN0IHRvIGJlIHJlZmVyZW5jZWFibGUgYW5kIHNob3J0IG5hbWVkIGJlY2F1c2UgcHJvcGVydGllcyB3aWxsIG5vdCBiZSBtaW5pZmllZFxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEB0eXBlIHtvYmplY3R9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX2V2ZW50cyA9IHt9LFxyXG5cclxuICAgICAgICAvKipcclxuICAgICAgICAgKiB1bmlxdWUgbmFtZXNwYWNlIGZvciBpbnN0YW5jZSByZWxhdGVkIGV2ZW50c1xyXG4gICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAqIEB0eXBlIHtzdHJpbmd9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX25hbWVzcGFjZSA9IF9jb25maWcubmFtZSArICctJyArICgrK2xhenlJbnN0YW5jZUlkKTtcclxuXHJcbiAgICAgICAgLy8gbm9pbnNwZWN0aW9uIEpTVW5kZWZpbmVkUHJvcGVydHlBc3NpZ25tZW50XHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogd3JhcHBlciB0byBnZXQgb3Igc2V0IGFuIGVudHJ5IGZyb20gcGx1Z2luIGluc3RhbmNlIGNvbmZpZ3VyYXRpb25cclxuICAgICAgICAgKiBtdWNoIHNtYWxsZXIgb24gbWluaWZ5IGFzIGRpcmVjdCBhY2Nlc3NcclxuICAgICAgICAgKiBAYWNjZXNzIHB1YmxpY1xyXG4gICAgICAgICAqIEB0eXBlIHtmdW5jdGlvbn1cclxuICAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gZW50cnlOYW1lXHJcbiAgICAgICAgICogQHBhcmFtIHsqfSBbdmFsdWVdXHJcbiAgICAgICAgICogQHJldHVybiB7TGF6eVBsdWdpbnwqfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9pbnN0YW5jZS5jb25maWcgPSBmdW5jdGlvbihlbnRyeU5hbWUsIHZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX2NvbmZpZ1tlbnRyeU5hbWVdO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBfY29uZmlnW2VudHJ5TmFtZV0gPSB2YWx1ZTtcclxuICAgICAgICAgICAgcmV0dXJuIF9pbnN0YW5jZTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBub2luc3BlY3Rpb24gSlNVbmRlZmluZWRQcm9wZXJ0eUFzc2lnbm1lbnRcclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBhZGQgYWRkaXRpb25hbCBpdGVtcyB0byBjdXJyZW50IGluc3RhbmNlXHJcbiAgICAgICAgICogQGFjY2VzcyBwdWJsaWNcclxuICAgICAgICAgKiBAcGFyYW0ge0FycmF5fG9iamVjdHxzdHJpbmd9IGl0ZW1zXHJcbiAgICAgICAgICogQHJldHVybiB7TGF6eVBsdWdpbn1cclxuICAgICAgICAgKi9cclxuICAgICAgICBfaW5zdGFuY2UuYWRkSXRlbXMgPSBmdW5jdGlvbihpdGVtcykge1xyXG4gICAgICAgICAgICBfZXZlbnRzLmEgJiYgX2V2ZW50cy5hKCQudHlwZShpdGVtcykgPT09ICdzdHJpbmcnID8gJChpdGVtcykgOiBpdGVtcyk7IC8vIGpzaGludCBpZ25vcmUgOiBsaW5lXHJcbiAgICAgICAgICAgIHJldHVybiBfaW5zdGFuY2U7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gbm9pbnNwZWN0aW9uIEpTVW5kZWZpbmVkUHJvcGVydHlBc3NpZ25tZW50XHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogZ2V0IGFsbCBsZWZ0IGl0ZW1zIG9mIHRoaXMgaW5zdGFuY2VcclxuICAgICAgICAgKiBAYWNjZXNzIHB1YmxpY1xyXG4gICAgICAgICAqIEByZXR1cm5zIHtvYmplY3R9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX2luc3RhbmNlLmdldEl0ZW1zID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBfZXZlbnRzLmcgPyBfZXZlbnRzLmcoKSA6IHt9O1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIG5vaW5zcGVjdGlvbiBKU1VuZGVmaW5lZFByb3BlcnR5QXNzaWdubWVudFxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGZvcmNlIGxhenkgdG8gbG9hZCBhbGwgaXRlbXMgaW4gbG9hZGFibGUgYXJlYSByaWdodCBub3dcclxuICAgICAgICAgKiBieSBkZWZhdWx0IHdpdGhvdXQgdGhyb3R0bGVcclxuICAgICAgICAgKiBAYWNjZXNzIHB1YmxpY1xyXG4gICAgICAgICAqIEB0eXBlIHtmdW5jdGlvbn1cclxuICAgICAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFt1c2VUaHJvdHRsZV1cclxuICAgICAgICAgKiBAcmV0dXJuIHtMYXp5UGx1Z2lufVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIF9pbnN0YW5jZS51cGRhdGUgPSBmdW5jdGlvbih1c2VUaHJvdHRsZSkge1xyXG4gICAgICAgICAgICBfZXZlbnRzLmUgJiYgX2V2ZW50cy5lKHt9LCAhdXNlVGhyb3R0bGUpOyAvLyBqc2hpbnQgaWdub3JlIDogbGluZVxyXG4gICAgICAgICAgICByZXR1cm4gX2luc3RhbmNlO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIG5vaW5zcGVjdGlvbiBKU1VuZGVmaW5lZFByb3BlcnR5QXNzaWdubWVudFxyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGZvcmNlIGVsZW1lbnQocykgdG8gbG9hZCBkaXJlY3RseSwgaWdub3JpbmcgdGhlIHZpZXdwb3J0XHJcbiAgICAgICAgICogQGFjY2VzcyBwdWJsaWNcclxuICAgICAgICAgKiBAcGFyYW0ge0FycmF5fG9iamVjdHxzdHJpbmd9IGl0ZW1zXHJcbiAgICAgICAgICogQHJldHVybiB7TGF6eVBsdWdpbn1cclxuICAgICAgICAgKi9cclxuICAgICAgICBfaW5zdGFuY2UuZm9yY2UgPSBmdW5jdGlvbihpdGVtcykge1xyXG4gICAgICAgICAgICBfZXZlbnRzLmYgJiYgX2V2ZW50cy5mKCQudHlwZShpdGVtcykgPT09ICdzdHJpbmcnID8gJChpdGVtcykgOiBpdGVtcyk7IC8vIGpzaGludCBpZ25vcmUgOiBsaW5lXHJcbiAgICAgICAgICAgIHJldHVybiBfaW5zdGFuY2U7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gbm9pbnNwZWN0aW9uIEpTVW5kZWZpbmVkUHJvcGVydHlBc3NpZ25tZW50XHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogZm9yY2UgbGF6eSB0byBsb2FkIGFsbCBhdmFpbGFibGUgaXRlbXMgcmlnaHQgbm93XHJcbiAgICAgICAgICogdGhpcyBjYWxsIGlnbm9yZXMgdGhyb3R0bGluZ1xyXG4gICAgICAgICAqIEBhY2Nlc3MgcHVibGljXHJcbiAgICAgICAgICogQHR5cGUge2Z1bmN0aW9ufVxyXG4gICAgICAgICAqIEByZXR1cm4ge0xhenlQbHVnaW59XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX2luc3RhbmNlLmxvYWRBbGwgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgX2V2ZW50cy5lICYmIF9ldmVudHMuZSh7YWxsOiB0cnVlfSwgdHJ1ZSk7IC8vIGpzaGludCBpZ25vcmUgOiBsaW5lXHJcbiAgICAgICAgICAgIHJldHVybiBfaW5zdGFuY2U7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gbm9pbnNwZWN0aW9uIEpTVW5kZWZpbmVkUHJvcGVydHlBc3NpZ25tZW50XHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogZGVzdHJveSB0aGlzIHBsdWdpbiBpbnN0YW5jZVxyXG4gICAgICAgICAqIEBhY2Nlc3MgcHVibGljXHJcbiAgICAgICAgICogQHR5cGUge2Z1bmN0aW9ufVxyXG4gICAgICAgICAqIEByZXR1cm4gdW5kZWZpbmVkXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgX2luc3RhbmNlLmRlc3Ryb3kgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgLy8gdW5iaW5kIGluc3RhbmNlIGdlbmVyYXRlZCBldmVudHNcclxuICAgICAgICAgICAgLy8gbm9pbnNwZWN0aW9uIEpTVW5yZXNvbHZlZEZ1bmN0aW9uLCBKU1VucmVzb2x2ZWRWYXJpYWJsZVxyXG4gICAgICAgICAgICAkKF9jb25maWcuYXBwZW5kU2Nyb2xsKS5vZmYoJy4nICsgX25hbWVzcGFjZSwgX2V2ZW50cy5lKTtcclxuICAgICAgICAgICAgLy8gbm9pbnNwZWN0aW9uIEpTVW5yZXNvbHZlZFZhcmlhYmxlXHJcbiAgICAgICAgICAgICQod2luZG93KS5vZmYoJy4nICsgX25hbWVzcGFjZSk7XHJcblxyXG4gICAgICAgICAgICAvLyBjbGVhciBldmVudHNcclxuICAgICAgICAgICAgX2V2ZW50cyA9IHt9O1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBzdGFydCB1c2luZyBsYXp5IGFuZCByZXR1cm4gYWxsIGVsZW1lbnRzIHRvIGJlIGNoYWluYWJsZSBvciBpbnN0YW5jZSBmb3IgZnVydGhlciB1c2VcclxuICAgICAgICAvLyBub2luc3BlY3Rpb24gSlNVbnJlc29sdmVkVmFyaWFibGVcclxuICAgICAgICBfZXhlY3V0ZUxhenkoX2luc3RhbmNlLCBfY29uZmlnLCBlbGVtZW50cywgX2V2ZW50cywgX25hbWVzcGFjZSk7XHJcbiAgICAgICAgcmV0dXJuIF9jb25maWcuY2hhaW5hYmxlID8gZWxlbWVudHMgOiBfaW5zdGFuY2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBzZXR0aW5ncyBhbmQgY29uZmlndXJhdGlvbiBkYXRhXHJcbiAgICAgKiBAYWNjZXNzIHB1YmxpY1xyXG4gICAgICogQHR5cGUge29iamVjdHwqfVxyXG4gICAgICovXHJcbiAgICBMYXp5UGx1Z2luLnByb3RvdHlwZS5jb25maWcgPSB7XHJcbiAgICAgICAgLy8gZ2VuZXJhbFxyXG4gICAgICAgIG5hbWUgICAgICAgICAgICAgICA6ICdsYXp5JyxcclxuICAgICAgICBjaGFpbmFibGUgICAgICAgICAgOiB0cnVlLFxyXG4gICAgICAgIGF1dG9EZXN0cm95ICAgICAgICA6IHRydWUsXHJcbiAgICAgICAgYmluZCAgICAgICAgICAgICAgIDogJ2xvYWQnLFxyXG4gICAgICAgIHRocmVzaG9sZCAgICAgICAgICA6IDUwMCxcclxuICAgICAgICB2aXNpYmxlT25seSAgICAgICAgOiBmYWxzZSxcclxuICAgICAgICBhcHBlbmRTY3JvbGwgICAgICAgOiB3aW5kb3csXHJcbiAgICAgICAgc2Nyb2xsRGlyZWN0aW9uICAgIDogJ2JvdGgnLFxyXG4gICAgICAgIGltYWdlQmFzZSAgICAgICAgICA6IG51bGwsXHJcbiAgICAgICAgZGVmYXVsdEltYWdlICAgICAgIDogJ2RhdGE6aW1hZ2UvZ2lmO2Jhc2U2NCxSMGxHT0RsaEFRQUJBSUFBQVAvLy93QUFBQ0g1QkFFQUFBQUFMQUFBQUFBQkFBRUFBQUlDUkFFQU93PT0nLFxyXG4gICAgICAgIHBsYWNlaG9sZGVyICAgICAgICA6IG51bGwsXHJcbiAgICAgICAgZGVsYXkgICAgICAgICAgICAgIDogLTEsXHJcbiAgICAgICAgY29tYmluZWQgICAgICAgICAgIDogZmFsc2UsXHJcblxyXG4gICAgICAgIC8vIGF0dHJpYnV0ZXNcclxuICAgICAgICBhdHRyaWJ1dGUgICAgICAgICAgOiAnZGF0YS1zcmMnLFxyXG4gICAgICAgIHNyY3NldEF0dHJpYnV0ZSAgICA6ICdkYXRhLXNyY3NldCcsXHJcbiAgICAgICAgc2l6ZXNBdHRyaWJ1dGUgICAgIDogJ2RhdGEtc2l6ZXMnLFxyXG4gICAgICAgIHJldGluYUF0dHJpYnV0ZSAgICA6ICdkYXRhLXJldGluYScsXHJcbiAgICAgICAgbG9hZGVyQXR0cmlidXRlICAgIDogJ2RhdGEtbG9hZGVyJyxcclxuICAgICAgICBpbWFnZUJhc2VBdHRyaWJ1dGUgOiAnZGF0YS1pbWFnZWJhc2UnLFxyXG4gICAgICAgIHJlbW92ZUF0dHJpYnV0ZSAgICA6IHRydWUsXHJcbiAgICAgICAgaGFuZGxlZE5hbWUgICAgICAgIDogJ2hhbmRsZWQnLFxyXG4gICAgICAgIGxvYWRlZE5hbWUgICAgICAgICA6ICdsb2FkZWQnLFxyXG5cclxuICAgICAgICAvLyBlZmZlY3RcclxuICAgICAgICBlZmZlY3QgICAgICAgICAgICAgOiAnc2hvdycsXHJcbiAgICAgICAgZWZmZWN0VGltZSAgICAgICAgIDogMCxcclxuXHJcbiAgICAgICAgLy8gdGhyb3R0bGVcclxuICAgICAgICBlbmFibGVUaHJvdHRsZSAgICAgOiB0cnVlLFxyXG4gICAgICAgIHRocm90dGxlICAgICAgICAgICA6IDI1MCxcclxuXHJcbiAgICAgICAgLy8gY2FsbGJhY2tzXHJcbiAgICAgICAgYmVmb3JlTG9hZCAgICAgICAgIDogdW5kZWZpbmVkLFxyXG4gICAgICAgIGFmdGVyTG9hZCAgICAgICAgICA6IHVuZGVmaW5lZCxcclxuICAgICAgICBvbkVycm9yICAgICAgICAgICAgOiB1bmRlZmluZWQsXHJcbiAgICAgICAgb25GaW5pc2hlZEFsbCAgICAgIDogdW5kZWZpbmVkXHJcbiAgICB9O1xyXG5cclxuICAgIC8vIHJlZ2lzdGVyIHdpbmRvdyBsb2FkIGV2ZW50IGdsb2JhbGx5IHRvIHByZXZlbnQgbm90IGxvYWRpbmcgZWxlbWVudHNcclxuICAgIC8vIHNpbmNlIGpRdWVyeSAzLlggcmVhZHkgc3RhdGUgaXMgZnVsbHkgYXN5bmMgYW5kIG1heSBiZSBleGVjdXRlZCBhZnRlciAnbG9hZCcgXHJcbiAgICAkKHdpbmRvdykub24oJ2xvYWQnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICB3aW5kb3dMb2FkZWQgPSB0cnVlO1xyXG4gICAgfSk7XHJcbn0pKHdpbmRvdyk7IiwiLyohXHJcbiAqIGpRdWVyeSAmIFplcHRvIExhenkgLSBBSkFYIFBsdWdpbiAtIHYxLjRcclxuICogaHR0cDovL2pxdWVyeS5laXNiZWhyLmRlL2xhenkvXHJcbiAqXHJcbiAqIENvcHlyaWdodCAyMDEyIC0gMjAxOCwgRGFuaWVsICdFaXNiZWhyJyBLZXJuXHJcbiAqXHJcbiAqIER1YWwgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBhbmQgR1BMLTIuMCBsaWNlbnNlczpcclxuICogaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuICogaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbFxyXG4gKi9cclxuOyhmdW5jdGlvbigkKSB7XHJcbiAgICAvLyBsb2FkIGRhdGEgYnkgYWpheCByZXF1ZXN0IGFuZCBwYXNzIHRoZW0gdG8gZWxlbWVudHMgaW5uZXIgaHRtbCwgbGlrZTpcclxuICAgIC8vIDxkaXYgZGF0YS1sb2FkZXI9XCJhamF4XCIgZGF0YS1zcmM9XCJ1cmwuaHRtbFwiIGRhdGEtbWV0aG9kPVwicG9zdFwiIGRhdGEtdHlwZT1cImh0bWxcIj48L2Rpdj5cclxuICAgICQubGF6eSgnYWpheCcsIGZ1bmN0aW9uKGVsZW1lbnQsIHJlc3BvbnNlKSB7XHJcbiAgICAgICAgYWpheFJlcXVlc3QodGhpcywgZWxlbWVudCwgcmVzcG9uc2UsIGVsZW1lbnQuYXR0cignZGF0YS1tZXRob2QnKSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBsb2FkIGRhdGEgYnkgYWpheCBnZXQgcmVxdWVzdCBhbmQgcGFzcyB0aGVtIHRvIGVsZW1lbnRzIGlubmVyIGh0bWwsIGxpa2U6XHJcbiAgICAvLyA8ZGl2IGRhdGEtbG9hZGVyPVwiZ2V0XCIgZGF0YS1zcmM9XCJ1cmwuaHRtbFwiIGRhdGEtdHlwZT1cImh0bWxcIj48L2Rpdj5cclxuICAgICQubGF6eSgnZ2V0JywgZnVuY3Rpb24oZWxlbWVudCwgcmVzcG9uc2UpIHtcclxuICAgICAgICBhamF4UmVxdWVzdCh0aGlzLCBlbGVtZW50LCByZXNwb25zZSwgJ0dFVCcpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gbG9hZCBkYXRhIGJ5IGFqYXggcG9zdCByZXF1ZXN0IGFuZCBwYXNzIHRoZW0gdG8gZWxlbWVudHMgaW5uZXIgaHRtbCwgbGlrZTpcclxuICAgIC8vIDxkaXYgZGF0YS1sb2FkZXI9XCJwb3N0XCIgZGF0YS1zcmM9XCJ1cmwuaHRtbFwiIGRhdGEtdHlwZT1cImh0bWxcIj48L2Rpdj5cclxuICAgICQubGF6eSgncG9zdCcsIGZ1bmN0aW9uKGVsZW1lbnQsIHJlc3BvbnNlKSB7XHJcbiAgICAgICAgYWpheFJlcXVlc3QodGhpcywgZWxlbWVudCwgcmVzcG9uc2UsICdQT1NUJyk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBsb2FkIGRhdGEgYnkgYWpheCBwdXQgcmVxdWVzdCBhbmQgcGFzcyB0aGVtIHRvIGVsZW1lbnRzIGlubmVyIGh0bWwsIGxpa2U6XHJcbiAgICAvLyA8ZGl2IGRhdGEtbG9hZGVyPVwicHV0XCIgZGF0YS1zcmM9XCJ1cmwuaHRtbFwiIGRhdGEtdHlwZT1cImh0bWxcIj48L2Rpdj5cclxuICAgICQubGF6eSgncHV0JywgZnVuY3Rpb24oZWxlbWVudCwgcmVzcG9uc2UpIHtcclxuICAgICAgICBhamF4UmVxdWVzdCh0aGlzLCBlbGVtZW50LCByZXNwb25zZSwgJ1BVVCcpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBleGVjdXRlIGFqYXggcmVxdWVzdCBhbmQgaGFuZGxlIHJlc3BvbnNlXHJcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gaW5zdGFuY2VcclxuICAgICAqIEBwYXJhbSB7alF1ZXJ5fG9iamVjdH0gZWxlbWVudFxyXG4gICAgICogQHBhcmFtIHtmdW5jdGlvbn0gcmVzcG9uc2VcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBbbWV0aG9kXVxyXG4gICAgICovXHJcbiAgICBmdW5jdGlvbiBhamF4UmVxdWVzdChpbnN0YW5jZSwgZWxlbWVudCwgcmVzcG9uc2UsIG1ldGhvZCkge1xyXG4gICAgICAgIG1ldGhvZCA9IG1ldGhvZCA/IG1ldGhvZC50b1VwcGVyQ2FzZSgpIDogJ0dFVCc7XHJcblxyXG4gICAgICAgIHZhciBkYXRhO1xyXG4gICAgICAgIGlmICgobWV0aG9kID09PSAnUE9TVCcgfHwgbWV0aG9kID09PSAnUFVUJykgJiYgaW5zdGFuY2UuY29uZmlnKCdhamF4Q3JlYXRlRGF0YScpKSB7XHJcbiAgICAgICAgICAgIGRhdGEgPSBpbnN0YW5jZS5jb25maWcoJ2FqYXhDcmVhdGVEYXRhJykuYXBwbHkoaW5zdGFuY2UsIFtlbGVtZW50XSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICB1cmw6IGVsZW1lbnQuYXR0cignZGF0YS1zcmMnKSxcclxuICAgICAgICAgICAgdHlwZTogbWV0aG9kID09PSAnUE9TVCcgfHwgbWV0aG9kID09PSAnUFVUJyA/IG1ldGhvZCA6ICdHRVQnLFxyXG4gICAgICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgICAgICBkYXRhVHlwZTogZWxlbWVudC5hdHRyKCdkYXRhLXR5cGUnKSB8fCAnaHRtbCcsXHJcblxyXG4gICAgICAgICAgICAvKipcclxuICAgICAgICAgICAgICogc3VjY2VzcyBjYWxsYmFja1xyXG4gICAgICAgICAgICAgKiBAYWNjZXNzIHByaXZhdGVcclxuICAgICAgICAgICAgICogQHBhcmFtIHsqfSBjb250ZW50XHJcbiAgICAgICAgICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbihjb250ZW50KSB7XHJcbiAgICAgICAgICAgICAgICAvLyBzZXQgcmVzcG9uZGVkIGRhdGEgdG8gZWxlbWVudCdzIGlubmVyIGh0bWxcclxuICAgICAgICAgICAgICAgIGVsZW1lbnQuaHRtbChjb250ZW50KTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyB1c2UgcmVzcG9uc2UgZnVuY3Rpb24gZm9yIFplcHRvXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZSh0cnVlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyByZW1vdmUgYXR0cmlidXRlc1xyXG4gICAgICAgICAgICAgICAgaWYgKGluc3RhbmNlLmNvbmZpZygncmVtb3ZlQXR0cmlidXRlJykpIHtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoJ2RhdGEtc3JjIGRhdGEtbWV0aG9kIGRhdGEtdHlwZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgLyoqXHJcbiAgICAgICAgICAgICAqIGVycm9yIGNhbGxiYWNrXHJcbiAgICAgICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAgICAgKiBAcmV0dXJuIHt2b2lkfVxyXG4gICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgLy8gcGFzcyBlcnJvciBzdGF0ZSB0byBsYXp5XHJcbiAgICAgICAgICAgICAgICAvLyB1c2UgcmVzcG9uc2UgZnVuY3Rpb24gZm9yIFplcHRvXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSkod2luZG93LmpRdWVyeSB8fCB3aW5kb3cuWmVwdG8pO1xyXG5cclxuLyohXHJcbiAqIGpRdWVyeSAmIFplcHRvIExhenkgLSBBViBQbHVnaW4gLSB2MS40XHJcbiAqIGh0dHA6Ly9qcXVlcnkuZWlzYmVoci5kZS9sYXp5L1xyXG4gKlxyXG4gKiBDb3B5cmlnaHQgMjAxMiAtIDIwMTgsIERhbmllbCAnRWlzYmVocicgS2VyblxyXG4gKlxyXG4gKiBEdWFsIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgYW5kIEdQTC0yLjAgbGljZW5zZXM6XHJcbiAqIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcbiAqIGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxcclxuICovXHJcbjsoZnVuY3Rpb24oJCkge1xyXG4gICAgLy8gbG9hZHMgYXVkaW8gYW5kIHZpZGVvIHRhZ3MgaW5jbHVkaW5nIHRyYWNrcyBieSB0d28gd2F5cywgbGlrZTpcclxuICAgIC8vIDxhdWRpbz5cclxuICAgIC8vICAgPGRhdGEtc3JjIHNyYz1cImF1ZGlvLm9nZ1wiIHR5cGU9XCJ2aWRlby9vZ2dcIj48L2RhdGEtc3JjPlxyXG4gICAgLy8gICA8ZGF0YS1zcmMgc3JjPVwiYXVkaW8ubXAzXCIgdHlwZT1cInZpZGVvL21wM1wiPjwvZGF0YS1zcmM+XHJcbiAgICAvLyA8L2F1ZGlvPlxyXG4gICAgLy8gPHZpZGVvIGRhdGEtcG9zdGVyPVwicG9zdGVyLmpwZ1wiPlxyXG4gICAgLy8gICA8ZGF0YS1zcmMgc3JjPVwidmlkZW8ub2d2XCIgdHlwZT1cInZpZGVvL29ndlwiPjwvZGF0YS1zcmM+XHJcbiAgICAvLyAgIDxkYXRhLXNyYyBzcmM9XCJ2aWRlby53ZWJtXCIgdHlwZT1cInZpZGVvL3dlYm1cIj48L2RhdGEtc3JjPlxyXG4gICAgLy8gICA8ZGF0YS1zcmMgc3JjPVwidmlkZW8ubXA0XCIgdHlwZT1cInZpZGVvL21wNFwiPjwvZGF0YS1zcmM+XHJcbiAgICAvLyAgIDxkYXRhLXRyYWNrIGtpbmQ9XCJjYXB0aW9uc1wiIHNyYz1cImNhcHRpb25zLnZ0dFwiIHNyY2xhbmc9XCJlblwiPjwvZGF0YS10cmFjaz5cclxuICAgIC8vICAgPGRhdGEtdHJhY2sga2luZD1cImRlc2NyaXB0aW9uc1wiIHNyYz1cImRlc2NyaXB0aW9ucy52dHRcIiBzcmNsYW5nPVwiZW5cIj48L2RhdGEtdHJhY2s+XHJcbiAgICAvLyAgIDxkYXRhLXRyYWNrIGtpbmQ9XCJzdWJ0aXRsZXNcIiBzcmM9XCJzdWJ0aXRsZXMudnR0XCIgc3JjbGFuZz1cImRlXCI+PC9kYXRhLXRyYWNrPlxyXG4gICAgLy8gPC92aWRlbz5cclxuICAgIC8vXHJcbiAgICAvLyBvcjpcclxuICAgIC8vIDxhdWRpbyBkYXRhLXNyYz1cImF1ZGlvLm9nZ3x2aWRlby9vZ2csdmlkZW8ubXAzfHZpZGVvL21wM1wiPjwvdmlkZW8+XHJcbiAgICAvLyA8dmlkZW8gZGF0YS1wb3N0ZXI9XCJwb3N0ZXIuanBnXCIgZGF0YS1zcmM9XCJ2aWRlby5vZ3Z8dmlkZW8vb2d2LHZpZGVvLndlYm18dmlkZW8vd2VibSx2aWRlby5tcDR8dmlkZW8vbXA0XCI+XHJcbiAgICAvLyAgIDxkYXRhLXRyYWNrIGtpbmQ9XCJjYXB0aW9uc1wiIHNyYz1cImNhcHRpb25zLnZ0dFwiIHNyY2xhbmc9XCJlblwiPjwvZGF0YS10cmFjaz5cclxuICAgIC8vICAgPGRhdGEtdHJhY2sga2luZD1cImRlc2NyaXB0aW9uc1wiIHNyYz1cImRlc2NyaXB0aW9ucy52dHRcIiBzcmNsYW5nPVwiZW5cIj48L2RhdGEtdHJhY2s+XHJcbiAgICAvLyAgIDxkYXRhLXRyYWNrIGtpbmQ9XCJzdWJ0aXRsZXNcIiBzcmM9XCJzdWJ0aXRsZXMudnR0XCIgc3JjbGFuZz1cImRlXCI+PC9kYXRhLXRyYWNrPlxyXG4gICAgLy8gPC92aWRlbz5cclxuICAgICQubGF6eShbJ2F2JywgJ2F1ZGlvJywgJ3ZpZGVvJ10sIFsnYXVkaW8nLCAndmlkZW8nXSwgZnVuY3Rpb24oZWxlbWVudCwgcmVzcG9uc2UpIHtcclxuICAgICAgICB2YXIgZWxlbWVudFRhZ05hbWUgPSBlbGVtZW50WzBdLnRhZ05hbWUudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICAgICAgaWYgKGVsZW1lbnRUYWdOYW1lID09PSAnYXVkaW8nIHx8IGVsZW1lbnRUYWdOYW1lID09PSAndmlkZW8nKSB7XHJcbiAgICAgICAgICAgIHZhciBzcmNBdHRyID0gJ2RhdGEtc3JjJyxcclxuICAgICAgICAgICAgICAgIHNvdXJjZXMgPSBlbGVtZW50LmZpbmQoc3JjQXR0ciksXHJcbiAgICAgICAgICAgICAgICB0cmFja3MgPSBlbGVtZW50LmZpbmQoJ2RhdGEtdHJhY2snKSxcclxuICAgICAgICAgICAgICAgIHNvdXJjZXNJbkVycm9yID0gMCxcclxuXHJcbiAgICAgICAgICAgIC8vIGNyZWF0ZSBvbiBlcnJvciBjYWxsYmFjayBmb3Igc291cmNlc1xyXG4gICAgICAgICAgICBvbkVycm9yID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoKytzb3VyY2VzSW5FcnJvciA9PT0gc291cmNlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAvLyBjcmVhdGUgY2FsbGJhY2sgdG8gaGFuZGxlIGEgc291cmNlIG9yIHRyYWNrIGVudHJ5XHJcbiAgICAgICAgICAgIGhhbmRsZVNvdXJjZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHNvdXJjZSA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZSA9IHNvdXJjZVswXS50YWdOYW1lLnRvTG93ZXJDYXNlKCksXHJcbiAgICAgICAgICAgICAgICAgICAgYXR0cmlidXRlcyA9IHNvdXJjZS5wcm9wKCdhdHRyaWJ1dGVzJyksXHJcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0ID0gJCh0eXBlID09PSBzcmNBdHRyID8gJzxzb3VyY2U+JyA6ICc8dHJhY2s+Jyk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGUgPT09IHNyY0F0dHIpIHtcclxuICAgICAgICAgICAgICAgICAgICB0YXJnZXQub25lKCdlcnJvcicsIG9uRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICQuZWFjaChhdHRyaWJ1dGVzLCBmdW5jdGlvbihpbmRleCwgYXR0cmlidXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0LmF0dHIoYXR0cmlidXRlLm5hbWUsIGF0dHJpYnV0ZS52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBzb3VyY2UucmVwbGFjZVdpdGgodGFyZ2V0KTtcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIC8vIGNyZWF0ZSBldmVudCBmb3Igc3VjY2Vzc2Z1bGwgbG9hZFxyXG4gICAgICAgICAgICBlbGVtZW50Lm9uZSgnbG9hZGVkbWV0YWRhdGEnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlKHRydWUpO1xyXG4gICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgLy8gcmVtb3ZlIGRlZmF1bHQgY2FsbGJhY2tzIHRvIGlnbm9yZSBsb2FkaW5nIHBvc3RlciBpbWFnZVxyXG4gICAgICAgICAgICAub2ZmKCdsb2FkIGVycm9yJylcclxuXHJcbiAgICAgICAgICAgIC8vIGxvYWQgcG9zdGVyIGltYWdlXHJcbiAgICAgICAgICAgIC5hdHRyKCdwb3N0ZXInLCBlbGVtZW50LmF0dHIoJ2RhdGEtcG9zdGVyJykpO1xyXG5cclxuICAgICAgICAgICAgLy8gbG9hZCBieSBjaGlsZCB0YWdzXHJcbiAgICAgICAgICAgIGlmIChzb3VyY2VzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgc291cmNlcy5lYWNoKGhhbmRsZVNvdXJjZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGxvYWQgYnkgYXR0cmlidXRlXHJcbiAgICAgICAgICAgIGVsc2UgaWYgKGVsZW1lbnQuYXR0cihzcmNBdHRyKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gc3BsaXQgZm9yIGV2ZXJ5IGVudHJ5IGJ5IGNvbW1hXHJcbiAgICAgICAgICAgICAgICAkLmVhY2goZWxlbWVudC5hdHRyKHNyY0F0dHIpLnNwbGl0KCcsJyksIGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNwbGl0IGFnYWluIGZvciBmaWxlIGFuZCBmaWxlIHR5cGVcclxuICAgICAgICAgICAgICAgICAgICB2YXIgcGFydHMgPSB2YWx1ZS5zcGxpdCgnfCcpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBjcmVhdGUgYSBzb3VyY2UgZW50cnlcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmFwcGVuZCgkKCc8c291cmNlPicpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC5vbmUoJ2Vycm9yJywgb25FcnJvcilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoe3NyYzogcGFydHNbMF0udHJpbSgpLCB0eXBlOiBwYXJ0c1sxXS50cmltKCl9KSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyByZW1vdmUgbm93IG9ic29sZXRlIGF0dHJpYnV0ZVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuY29uZmlnKCdyZW1vdmVBdHRyaWJ1dGUnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlQXR0cihzcmNBdHRyKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBwYXNzIGVycm9yIHN0YXRlXHJcbiAgICAgICAgICAgICAgICAvLyB1c2UgcmVzcG9uc2UgZnVuY3Rpb24gZm9yIFplcHRvXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGxvYWQgb3B0aW9uYWwgdHJhY2tzXHJcbiAgICAgICAgICAgIGlmICh0cmFja3MubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICB0cmFja3MuZWFjaChoYW5kbGVTb3VyY2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgLy8gcGFzcyBlcnJvciBzdGF0ZVxyXG4gICAgICAgICAgICAvLyB1c2UgcmVzcG9uc2UgZnVuY3Rpb24gZm9yIFplcHRvXHJcbiAgICAgICAgICAgIHJlc3BvbnNlKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufSkod2luZG93LmpRdWVyeSB8fCB3aW5kb3cuWmVwdG8pO1xyXG5cclxuLyohXHJcbiAqIGpRdWVyeSAmIFplcHRvIExhenkgLSBpRnJhbWUgUGx1Z2luIC0gdjEuNVxyXG4gKiBodHRwOi8vanF1ZXJ5LmVpc2JlaHIuZGUvbGF6eS9cclxuICpcclxuICogQ29weXJpZ2h0IDIwMTIgLSAyMDE4LCBEYW5pZWwgJ0Vpc2JlaHInIEtlcm5cclxuICpcclxuICogRHVhbCBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGFuZCBHUEwtMi4wIGxpY2Vuc2VzOlxyXG4gKiBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxyXG4gKiBodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXHJcbiAqL1xyXG47KGZ1bmN0aW9uKCQpIHtcclxuICAgIC8vIGxvYWQgaWZyYW1lIGNvbnRlbnQsIGxpa2U6XHJcbiAgICAvLyA8aWZyYW1lIGRhdGEtc3JjPVwiaWZyYW1lLmh0bWxcIj48L2lmcmFtZT5cclxuICAgIC8vXHJcbiAgICAvLyBlbmFibGUgY29udGVudCBlcnJvciBjaGVjayB3aXRoOlxyXG4gICAgLy8gPGlmcmFtZSBkYXRhLXNyYz1cImlmcmFtZS5odG1sXCIgZGF0YS1lcnJvci1kZXRlY3Q9XCJ0cnVlXCI+PC9pZnJhbWU+XHJcbiAgICAkLmxhenkoWydmcmFtZScsICdpZnJhbWUnXSwgJ2lmcmFtZScsIGZ1bmN0aW9uKGVsZW1lbnQsIHJlc3BvbnNlKSB7XHJcbiAgICAgICAgdmFyIGluc3RhbmNlID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKGVsZW1lbnRbMF0udGFnTmFtZS50b0xvd2VyQ2FzZSgpID09PSAnaWZyYW1lJykge1xyXG4gICAgICAgICAgICB2YXIgc3JjQXR0ciA9ICdkYXRhLXNyYycsXHJcbiAgICAgICAgICAgICAgICBlcnJvckRldGVjdEF0dHIgPSAnZGF0YS1lcnJvci1kZXRlY3QnLFxyXG4gICAgICAgICAgICAgICAgZXJyb3JEZXRlY3QgPSBlbGVtZW50LmF0dHIoZXJyb3JEZXRlY3RBdHRyKTtcclxuXHJcbiAgICAgICAgICAgIC8vIGRlZmF1bHQgd2F5LCBqdXN0IHJlcGxhY2UgdGhlICdzcmMnIGF0dHJpYnV0ZVxyXG4gICAgICAgICAgICBpZiAoZXJyb3JEZXRlY3QgIT09ICd0cnVlJyAmJiBlcnJvckRldGVjdCAhPT0gJzEnKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBzZXQgaWZyYW1lIHNvdXJjZVxyXG4gICAgICAgICAgICAgICAgZWxlbWVudC5hdHRyKCdzcmMnLCBlbGVtZW50LmF0dHIoc3JjQXR0cikpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIHJlbW92ZSBhdHRyaWJ1dGVzXHJcbiAgICAgICAgICAgICAgICBpZiAoaW5zdGFuY2UuY29uZmlnKCdyZW1vdmVBdHRyaWJ1dGUnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlQXR0cihzcmNBdHRyICsgJyAnICsgZXJyb3JEZXRlY3RBdHRyKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gZXh0ZW5kZWQgd2F5LCBldmVuIGNoZWNrIGlmIHRoZSBkb2N1bWVudCBpcyBhdmFpbGFibGVcclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgICAgIHVybDogZWxlbWVudC5hdHRyKHNyY0F0dHIpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGFUeXBlOiAnaHRtbCcsXHJcbiAgICAgICAgICAgICAgICAgICAgY3Jvc3NEb21haW46IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgeGhyRmllbGRzOiB7d2l0aENyZWRlbnRpYWxzOiB0cnVlfSxcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLyoqXHJcbiAgICAgICAgICAgICAgICAgICAgICogc3VjY2VzcyBjYWxsYmFja1xyXG4gICAgICAgICAgICAgICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAgICAgICAgICAgICAqIEBwYXJhbSB7Kn0gY29udGVudFxyXG4gICAgICAgICAgICAgICAgICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oY29udGVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBzZXQgcmVzcG9uZGVkIGRhdGEgdG8gZWxlbWVudCdzIGlubmVyIGh0bWxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5odG1sKGNvbnRlbnQpXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjaGFuZ2UgaWZyYW1lIHNyY1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignc3JjJywgZWxlbWVudC5hdHRyKHNyY0F0dHIpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlbW92ZSBhdHRyaWJ1dGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpbnN0YW5jZS5jb25maWcoJ3JlbW92ZUF0dHJpYnV0ZScpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoc3JjQXR0ciArICcgJyArIGVycm9yRGV0ZWN0QXR0cik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAvKipcclxuICAgICAgICAgICAgICAgICAgICAgKiBlcnJvciBjYWxsYmFja1xyXG4gICAgICAgICAgICAgICAgICAgICAqIEBhY2Nlc3MgcHJpdmF0ZVxyXG4gICAgICAgICAgICAgICAgICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBwYXNzIGVycm9yIHN0YXRlIHRvIGxhenlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXNlIHJlc3BvbnNlIGZ1bmN0aW9uIGZvciBaZXB0b1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBwYXNzIGVycm9yIHN0YXRlIHRvIGxhenlcclxuICAgICAgICAgICAgLy8gdXNlIHJlc3BvbnNlIGZ1bmN0aW9uIGZvciBaZXB0b1xyXG4gICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn0pKHdpbmRvdy5qUXVlcnkgfHwgd2luZG93LlplcHRvKTtcclxuXHJcbi8qIVxyXG4gKiBqUXVlcnkgJiBaZXB0byBMYXp5IC0gTk9PUCBQbHVnaW4gLSB2MS4yXHJcbiAqIGh0dHA6Ly9qcXVlcnkuZWlzYmVoci5kZS9sYXp5L1xyXG4gKlxyXG4gKiBDb3B5cmlnaHQgMjAxMiAtIDIwMTgsIERhbmllbCAnRWlzYmVocicgS2VyblxyXG4gKlxyXG4gKiBEdWFsIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgYW5kIEdQTC0yLjAgbGljZW5zZXM6XHJcbiAqIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcbiAqIGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxcclxuICovXHJcbjsoZnVuY3Rpb24oJCkge1xyXG4gICAgLy8gd2lsbCBkbyBub3RoaW5nLCB1c2VkIHRvIGRpc2FibGUgZWxlbWVudHMgb3IgZm9yIGRldmVsb3BtZW50XHJcbiAgICAvLyB1c2UgbGlrZTpcclxuICAgIC8vIDxkaXYgZGF0YS1sb2FkZXI9XCJub29wXCI+PC9kaXY+XHJcblxyXG4gICAgLy8gZG9lcyBub3QgZG8gYW55dGhpbmcsIGp1c3QgYSAnbm8tb3BlcmF0aW9uJyBoZWxwZXIgOylcclxuICAgICQubGF6eSgnbm9vcCcsIGZ1bmN0aW9uKCkge30pO1xyXG5cclxuICAgIC8vIGRvZXMgbm90aGluZywgYnV0IHJlc3BvbnNlIGEgc3VjY2Vzc2Z1bGwgbG9hZGluZ1xyXG4gICAgJC5sYXp5KCdub29wLXN1Y2Nlc3MnLCBmdW5jdGlvbihlbGVtZW50LCByZXNwb25zZSkge1xyXG4gICAgICAgIC8vIHVzZSByZXNwb25zZSBmdW5jdGlvbiBmb3IgWmVwdG9cclxuICAgICAgICByZXNwb25zZSh0cnVlKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIGRvZXMgbm90aGluZywgYnV0IHJlc3BvbnNlIGEgZmFpbGVkIGxvYWRpbmdcclxuICAgICQubGF6eSgnbm9vcC1lcnJvcicsIGZ1bmN0aW9uKGVsZW1lbnQsIHJlc3BvbnNlKSB7XHJcbiAgICAgICAgLy8gdXNlIHJlc3BvbnNlIGZ1bmN0aW9uIGZvciBaZXB0b1xyXG4gICAgICAgIHJlc3BvbnNlKGZhbHNlKTtcclxuICAgIH0pO1xyXG59KSh3aW5kb3cualF1ZXJ5IHx8IHdpbmRvdy5aZXB0byk7XHJcblxyXG4vKiFcclxuICogalF1ZXJ5ICYgWmVwdG8gTGF6eSAtIFBpY3R1cmUgUGx1Z2luIC0gdjEuM1xyXG4gKiBodHRwOi8vanF1ZXJ5LmVpc2JlaHIuZGUvbGF6eS9cclxuICpcclxuICogQ29weXJpZ2h0IDIwMTIgLSAyMDE4LCBEYW5pZWwgJ0Vpc2JlaHInIEtlcm5cclxuICpcclxuICogRHVhbCBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGFuZCBHUEwtMi4wIGxpY2Vuc2VzOlxyXG4gKiBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxyXG4gKiBodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvZ3BsLTIuMC5odG1sXHJcbiAqL1xyXG47KGZ1bmN0aW9uKCQpIHtcclxuICAgIHZhciBzcmNBdHRyID0gJ2RhdGEtc3JjJyxcclxuICAgICAgICBzcmNzZXRBdHRyID0gJ2RhdGEtc3Jjc2V0JyxcclxuICAgICAgICBtZWRpYUF0dHIgPSAnZGF0YS1tZWRpYScsXHJcbiAgICAgICAgc2l6ZXNBdHRyID0gJ2RhdGEtc2l6ZXMnLFxyXG4gICAgICAgIHR5cGVBdHRyID0gJ2RhdGEtdHlwZSc7XHJcblxyXG4gICAgLy8gbG9hZHMgcGljdHVyZSBlbGVtZW50cyBsaWtlOlxyXG4gICAgLy8gPHBpY3R1cmU+XHJcbiAgICAvLyAgIDxkYXRhLXNyYyBzcmNzZXQ9XCIxeC5qcGcgMXgsIDJ4LmpwZyAyeCwgM3guanBnIDN4XCIgbWVkaWE9XCIobWluLXdpZHRoOiA2MDBweClcIiB0eXBlPVwiaW1hZ2UvanBlZ1wiPjwvZGF0YS1zcmM+XHJcbiAgICAvLyAgIDxkYXRhLXNyYyBzcmNzZXQ9XCIxeC5qcGcgMXgsIDJ4LmpwZyAyeCwgM3guanBnIDN4XCIgbWVkaWE9XCIobWluLXdpZHRoOiA0MDBweClcIiB0eXBlPVwiaW1hZ2UvanBlZ1wiPjwvZGF0YS1zcmM+XHJcbiAgICAvLyAgIDxkYXRhLWltZyBzcmM9XCJkZWZhdWx0LmpwZ1wiID5cclxuICAgIC8vIDwvcGljdHVyZT5cclxuICAgIC8vXHJcbiAgICAvLyBvcjpcclxuICAgIC8vIDxwaWN0dXJlIGRhdGEtc3JjPVwiZGVmYXVsdC5qcGdcIj5cclxuICAgIC8vICAgPGRhdGEtc3JjIHNyY3NldD1cIjF4LmpwZyAxeCwgMnguanBnIDJ4LCAzeC5qcGcgM3hcIiBtZWRpYT1cIihtaW4td2lkdGg6IDYwMHB4KVwiIHR5cGU9XCJpbWFnZS9qcGVnXCI+PC9kYXRhLXNyYz5cclxuICAgIC8vICAgPGRhdGEtc3JjIHNyY3NldD1cIjF4LmpwZyAxeCwgMnguanBnIDJ4LCAzeC5qcGcgM3hcIiBtZWRpYT1cIihtaW4td2lkdGg6IDQwMHB4KVwiIHR5cGU9XCJpbWFnZS9qcGVnXCI+PC9kYXRhLXNyYz5cclxuICAgIC8vIDwvcGljdHVyZT5cclxuICAgIC8vXHJcbiAgICAvLyBvciBqdXN0IHdpdGggYXR0cmlidXRlcyBpbiBvbmUgbGluZTpcclxuICAgIC8vIDxwaWN0dXJlIGRhdGEtc3JjPVwiZGVmYXVsdC5qcGdcIiBkYXRhLXNyY3NldD1cIjF4LmpwZyAxeCwgMnguanBnIDJ4LCAzeC5qcGcgM3hcIiBkYXRhLW1lZGlhPVwiKG1pbi13aWR0aDogNjAwcHgpXCIgZGF0YS1zaXplcz1cIlwiIGRhdGEtdHlwZT1cImltYWdlL2pwZWdcIiAvPlxyXG4gICAgJC5sYXp5KFsncGljJywgJ3BpY3R1cmUnXSwgWydwaWN0dXJlJ10sIGZ1bmN0aW9uKGVsZW1lbnQsIHJlc3BvbnNlKSB7XHJcbiAgICAgICAgdmFyIGVsZW1lbnRUYWdOYW1lID0gZWxlbWVudFswXS50YWdOYW1lLnRvTG93ZXJDYXNlKCk7XHJcblxyXG4gICAgICAgIGlmIChlbGVtZW50VGFnTmFtZSA9PT0gJ3BpY3R1cmUnKSB7XHJcbiAgICAgICAgICAgIHZhciBzb3VyY2VzID0gZWxlbWVudC5maW5kKHNyY0F0dHIpLFxyXG4gICAgICAgICAgICAgICAgaW1hZ2UgPSBlbGVtZW50LmZpbmQoJ2RhdGEtaW1nJyksXHJcbiAgICAgICAgICAgICAgICBpbWFnZUJhc2UgPSB0aGlzLmNvbmZpZygnaW1hZ2VCYXNlJykgfHwgJyc7XHJcblxyXG4gICAgICAgICAgICAvLyBoYW5kbGUgYXMgY2hpbGQgZWxlbWVudHNcclxuICAgICAgICAgICAgaWYgKHNvdXJjZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBzb3VyY2VzLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVuYW1lRWxlbWVudFRhZygkKHRoaXMpLCAnc291cmNlJywgaW1hZ2VCYXNlKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSBpbWcgdGFnIGZyb20gY2hpbGRcclxuICAgICAgICAgICAgICAgIGlmIChpbWFnZS5sZW5ndGggPT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICBpbWFnZSA9IHJlbmFtZUVsZW1lbnRUYWcoaW1hZ2UsICdpbWcnLCBpbWFnZUJhc2UpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBiaW5kIGV2ZW50IGNhbGxiYWNrcyB0byBuZXcgaW1hZ2UgdGFnXHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2Uub24oJ2xvYWQnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSkub24oJ2Vycm9yJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaW1hZ2UuYXR0cignc3JjJywgaW1hZ2UuYXR0cihzcmNBdHRyKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmNvbmZpZygncmVtb3ZlQXR0cmlidXRlJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2UucmVtb3ZlQXR0cihzcmNBdHRyKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gY3JlYXRlIGltZyB0YWcgZnJvbSBhdHRyaWJ1dGVcclxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKGVsZW1lbnQuYXR0cihzcmNBdHRyKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSBpbWFnZSB0YWdcclxuICAgICAgICAgICAgICAgICAgICBjcmVhdGVJbWFnZU9iamVjdChlbGVtZW50LCBpbWFnZUJhc2UgKyBlbGVtZW50LmF0dHIoc3JjQXR0ciksIHJlc3BvbnNlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuY29uZmlnKCdyZW1vdmVBdHRyaWJ1dGUnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoc3JjQXR0cik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vIHBhc3MgZXJyb3Igc3RhdGVcclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHVzZSByZXNwb25zZSBmdW5jdGlvbiBmb3IgWmVwdG9cclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGhhbmRsZSBhcyBhdHRyaWJ1dGVzXHJcbiAgICAgICAgICAgIGVsc2UgaWYoIGVsZW1lbnQuYXR0cihzcmNzZXRBdHRyKSApIHtcclxuICAgICAgICAgICAgICAgIC8vIGNyZWF0ZSBzb3VyY2UgZWxlbWVudHMgYmVmb3JlIGltZyB0YWdcclxuICAgICAgICAgICAgICAgICQoJzxzb3VyY2U+JykuYXR0cih7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVkaWE6IGVsZW1lbnQuYXR0cihtZWRpYUF0dHIpLFxyXG4gICAgICAgICAgICAgICAgICAgIHNpemVzOiBlbGVtZW50LmF0dHIoc2l6ZXNBdHRyKSxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBlbGVtZW50LmF0dHIodHlwZUF0dHIpLFxyXG4gICAgICAgICAgICAgICAgICAgIHNyY3NldDogZ2V0Q29ycmVjdGVkU3JjU2V0KGVsZW1lbnQuYXR0cihzcmNzZXRBdHRyKSwgaW1hZ2VCYXNlKVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5hcHBlbmRUbyhlbGVtZW50KTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBjcmVhdGUgaW1hZ2UgdGFnXHJcbiAgICAgICAgICAgICAgICBjcmVhdGVJbWFnZU9iamVjdChlbGVtZW50LCBpbWFnZUJhc2UgKyBlbGVtZW50LmF0dHIoc3JjQXR0ciksIHJlc3BvbnNlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyByZW1vdmUgYXR0cmlidXRlcyBmcm9tIHBhcmVudCBwaWN0dXJlIGVsZW1lbnRcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmNvbmZpZygncmVtb3ZlQXR0cmlidXRlJykpIHtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoc3JjQXR0ciArICcgJyArIHNyY3NldEF0dHIgKyAnICcgKyBtZWRpYUF0dHIgKyAnICcgKyBzaXplc0F0dHIgKyAnICcgKyB0eXBlQXR0cik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHBhc3MgZXJyb3Igc3RhdGVcclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyB1c2UgcmVzcG9uc2UgZnVuY3Rpb24gZm9yIFplcHRvXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBwYXNzIGVycm9yIHN0YXRlXHJcbiAgICAgICAgICAgIC8vIHVzZSByZXNwb25zZSBmdW5jdGlvbiBmb3IgWmVwdG9cclxuICAgICAgICAgICAgcmVzcG9uc2UoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogY3JlYXRlIGEgbmV3IGNoaWxkIGVsZW1lbnQgYW5kIGNvcHkgYXR0cmlidXRlc1xyXG4gICAgICogQHBhcmFtIHtqUXVlcnl8b2JqZWN0fSBlbGVtZW50XHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdG9UeXBlXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gaW1hZ2VCYXNlXHJcbiAgICAgKiBAcmV0dXJuIHtqUXVlcnl8b2JqZWN0fVxyXG4gICAgICovXHJcbiAgICBmdW5jdGlvbiByZW5hbWVFbGVtZW50VGFnKGVsZW1lbnQsIHRvVHlwZSwgaW1hZ2VCYXNlKSB7XHJcbiAgICAgICAgdmFyIGF0dHJpYnV0ZXMgPSBlbGVtZW50LnByb3AoJ2F0dHJpYnV0ZXMnKSxcclxuICAgICAgICAgICAgdGFyZ2V0ID0gJCgnPCcgKyB0b1R5cGUgKyAnPicpO1xyXG5cclxuICAgICAgICAkLmVhY2goYXR0cmlidXRlcywgZnVuY3Rpb24oaW5kZXgsIGF0dHJpYnV0ZSkge1xyXG4gICAgICAgICAgICAvLyBidWlsZCBzcmNzZXQgd2l0aCBpbWFnZSBiYXNlXHJcbiAgICAgICAgICAgIGlmIChhdHRyaWJ1dGUubmFtZSA9PT0gJ3NyY3NldCcgfHwgYXR0cmlidXRlLm5hbWUgPT09IHNyY0F0dHIpIHtcclxuICAgICAgICAgICAgICAgIGF0dHJpYnV0ZS52YWx1ZSA9IGdldENvcnJlY3RlZFNyY1NldChhdHRyaWJ1dGUudmFsdWUsIGltYWdlQmFzZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRhcmdldC5hdHRyKGF0dHJpYnV0ZS5uYW1lLCBhdHRyaWJ1dGUudmFsdWUpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBlbGVtZW50LnJlcGxhY2VXaXRoKHRhcmdldCk7XHJcbiAgICAgICAgcmV0dXJuIHRhcmdldDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGNyZWF0ZSBhIG5ldyBpbWFnZSBlbGVtZW50IGluc2lkZSBwYXJlbnQgZWxlbWVudFxyXG4gICAgICogQHBhcmFtIHtqUXVlcnl8b2JqZWN0fSBwYXJlbnRcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBzcmNcclxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb259IHJlc3BvbnNlXHJcbiAgICAgKiBAcmV0dXJuIHZvaWRcclxuICAgICAqL1xyXG4gICAgZnVuY3Rpb24gY3JlYXRlSW1hZ2VPYmplY3QocGFyZW50LCBzcmMsIHJlc3BvbnNlKSB7XHJcbiAgICAgICAgLy8gY3JlYXRlIGltYWdlIHRhZ1xyXG4gICAgICAgIHZhciBpbWFnZU9iaiA9ICQoJzxpbWc+JylcclxuXHJcbiAgICAgICAgLy8gY3JlYXRlIGltYWdlIHRhZyBhbiBiaW5kIGNhbGxiYWNrcyBmb3IgY29ycmVjdCByZXNwb25zZVxyXG4gICAgICAgIC5vbmUoJ2xvYWQnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgcmVzcG9uc2UodHJ1ZSk7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAub25lKCdlcnJvcicsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICByZXNwb25zZShmYWxzZSk7XHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAgICAgLy8gc2V0IGludG8gcGljdHVyZSBlbGVtZW50XHJcbiAgICAgICAgLmFwcGVuZFRvKHBhcmVudClcclxuXHJcbiAgICAgICAgLy8gc2V0IHNyYyBhdHRyaWJ1dGUgYXQgbGFzdCB0byBwcmV2ZW50IGVhcmx5IGtpY2staW5cclxuICAgICAgICAuYXR0cignc3JjJywgc3JjKTtcclxuXHJcbiAgICAgICAgLy8gY2FsbCBhZnRlciBsb2FkIGV2ZW4gb24gY2FjaGVkIGltYWdlXHJcbiAgICAgICAgaW1hZ2VPYmouY29tcGxldGUgJiYgaW1hZ2VPYmoubG9hZCgpOyAvLyBqc2hpbnQgaWdub3JlIDogbGluZVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogcHJlcGVuZCBpbWFnZSBiYXNlIHRvIGFsbCBzcmNzZXQgZW50cmllc1xyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNyY3NldFxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGltYWdlQmFzZVxyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgZnVuY3Rpb24gZ2V0Q29ycmVjdGVkU3JjU2V0KHNyY3NldCwgaW1hZ2VCYXNlKSB7XHJcbiAgICAgICAgaWYgKGltYWdlQmFzZSkge1xyXG4gICAgICAgICAgICAvLyB0cmltLCByZW1vdmUgdW5uZWNlc3Nhcnkgc3BhY2VzIGFuZCBzcGxpdCBlbnRyaWVzXHJcbiAgICAgICAgICAgIHZhciBlbnRyaWVzID0gc3Jjc2V0LnNwbGl0KCcsJyk7XHJcbiAgICAgICAgICAgIHNyY3NldCA9ICcnO1xyXG5cclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDAsIGwgPSBlbnRyaWVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgc3Jjc2V0ICs9IGltYWdlQmFzZSArIGVudHJpZXNbaV0udHJpbSgpICsgKGkgIT09IGwgLSAxID8gJywnIDogJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gc3Jjc2V0O1xyXG4gICAgfVxyXG59KSh3aW5kb3cualF1ZXJ5IHx8IHdpbmRvdy5aZXB0byk7XHJcblxyXG4vKiFcclxuICogalF1ZXJ5ICYgWmVwdG8gTGF6eSAtIFNjcmlwdCBQbHVnaW4gLSB2MS4yXHJcbiAqIGh0dHA6Ly9qcXVlcnkuZWlzYmVoci5kZS9sYXp5L1xyXG4gKlxyXG4gKiBDb3B5cmlnaHQgMjAxMiAtIDIwMTgsIERhbmllbCAnRWlzYmVocicgS2VyblxyXG4gKlxyXG4gKiBEdWFsIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgYW5kIEdQTC0yLjAgbGljZW5zZXM6XHJcbiAqIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXHJcbiAqIGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy9ncGwtMi4wLmh0bWxcclxuICovXHJcbjsoZnVuY3Rpb24oJCkge1xyXG4gICAgLy8gbG9hZHMgamF2YXNjcmlwdCBmaWxlcyBmb3Igc2NyaXB0IHRhZ3MsIGxpa2U6XHJcbiAgICAvLyA8c2NyaXB0IGRhdGEtc3JjPVwiZmlsZS5qc1wiIHR5cGU9XCJ0ZXh0L2phdmFzY3JpcHRcIj48L3NjcmlwdD5cclxuICAgICQubGF6eShbJ2pzJywgJ2phdmFzY3JpcHQnLCAnc2NyaXB0J10sICdzY3JpcHQnLCBmdW5jdGlvbihlbGVtZW50LCByZXNwb25zZSkge1xyXG4gICAgICAgIGlmIChlbGVtZW50WzBdLnRhZ05hbWUudG9Mb3dlckNhc2UoKSA9PT0gJ3NjcmlwdCcpIHtcclxuICAgICAgICAgICAgZWxlbWVudC5hdHRyKCdzcmMnLCBlbGVtZW50LmF0dHIoJ2RhdGEtc3JjJykpO1xyXG5cclxuICAgICAgICAgICAgLy8gcmVtb3ZlIGF0dHJpYnV0ZVxyXG4gICAgICAgICAgICBpZiAodGhpcy5jb25maWcoJ3JlbW92ZUF0dHJpYnV0ZScpKSB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoJ2RhdGEtc3JjJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIHVzZSByZXNwb25zZSBmdW5jdGlvbiBmb3IgWmVwdG9cclxuICAgICAgICAgICAgcmVzcG9uc2UoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59KSh3aW5kb3cualF1ZXJ5IHx8IHdpbmRvdy5aZXB0byk7XHJcblxyXG4vKiFcclxuICogalF1ZXJ5ICYgWmVwdG8gTGF6eSAtIFZpbWVvIFBsdWdpbiAtIHYxLjFcclxuICogaHR0cDovL2pxdWVyeS5laXNiZWhyLmRlL2xhenkvXHJcbiAqXHJcbiAqIENvcHlyaWdodCAyMDEyIC0gMjAxOCwgRGFuaWVsICdFaXNiZWhyJyBLZXJuXHJcbiAqXHJcbiAqIER1YWwgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBhbmQgR1BMLTIuMCBsaWNlbnNlczpcclxuICogaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuICogaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbFxyXG4gKi9cclxuOyhmdW5jdGlvbigkKSB7XHJcbiAgICAvLyBsb2FkIHZpbWVvIHZpZGVvIGlmcmFtZSwgbGlrZTpcclxuICAgIC8vIDxpZnJhbWUgZGF0YS1sb2FkZXI9XCJ2aW1lb1wiIGRhdGEtc3JjPVwiMTc2ODk0MTMwXCIgd2lkdGg9XCI2NDBcIiBoZWlnaHQ9XCIzNjBcIiBmcmFtZWJvcmRlcj1cIjBcIiB3ZWJraXRhbGxvd2Z1bGxzY3JlZW4gbW96YWxsb3dmdWxsc2NyZWVuIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT5cclxuICAgICQubGF6eSgndmltZW8nLCBmdW5jdGlvbihlbGVtZW50LCByZXNwb25zZSkge1xyXG4gICAgICAgIGlmIChlbGVtZW50WzBdLnRhZ05hbWUudG9Mb3dlckNhc2UoKSA9PT0gJ2lmcmFtZScpIHtcclxuICAgICAgICAgICAgLy8gcGFzcyBzb3VyY2UgdG8gaWZyYW1lXHJcbiAgICAgICAgICAgIGVsZW1lbnQuYXR0cignc3JjJywgJ2h0dHBzOi8vcGxheWVyLnZpbWVvLmNvbS92aWRlby8nICsgZWxlbWVudC5hdHRyKCdkYXRhLXNyYycpKTtcclxuXHJcbiAgICAgICAgICAgIC8vIHJlbW92ZSBhdHRyaWJ1dGVcclxuICAgICAgICAgICAgaWYgKHRoaXMuY29uZmlnKCdyZW1vdmVBdHRyaWJ1dGUnKSkge1xyXG4gICAgICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVBdHRyKCdkYXRhLXNyYycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgLy8gcGFzcyBlcnJvciBzdGF0ZVxyXG4gICAgICAgICAgICAvLyB1c2UgcmVzcG9uc2UgZnVuY3Rpb24gZm9yIFplcHRvXHJcbiAgICAgICAgICAgIHJlc3BvbnNlKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufSkod2luZG93LmpRdWVyeSB8fCB3aW5kb3cuWmVwdG8pO1xyXG5cclxuLyohXHJcbiAqIGpRdWVyeSAmIFplcHRvIExhenkgLSBZb3VUdWJlIFBsdWdpbiAtIHYxLjVcclxuICogaHR0cDovL2pxdWVyeS5laXNiZWhyLmRlL2xhenkvXHJcbiAqXHJcbiAqIENvcHlyaWdodCAyMDEyIC0gMjAxOCwgRGFuaWVsICdFaXNiZWhyJyBLZXJuXHJcbiAqXHJcbiAqIER1YWwgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBhbmQgR1BMLTIuMCBsaWNlbnNlczpcclxuICogaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcclxuICogaHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzL2dwbC0yLjAuaHRtbFxyXG4gKi9cclxuOyhmdW5jdGlvbigkKSB7XHJcbiAgICAvLyBsb2FkIHlvdXR1YmUgdmlkZW8gaWZyYW1lLCBsaWtlOlxyXG4gICAgLy8gPGlmcmFtZSBkYXRhLWxvYWRlcj1cInl0XCIgZGF0YS1zcmM9XCIxQVlHbnc2TXdGTVwiIGRhdGEtbm9jb29raWU9XCIxXCIgd2lkdGg9XCI1NjBcIiBoZWlnaHQ9XCIzMTVcIiBmcmFtZWJvcmRlcj1cIjBcIiBhbGxvd2Z1bGxzY3JlZW4+PC9pZnJhbWU+XHJcbiAgICAkLmxhenkoWyd5dCcsICd5b3V0dWJlJ10sIGZ1bmN0aW9uKGVsZW1lbnQsIHJlc3BvbnNlKSB7XHJcbiAgICAgICAgaWYgKGVsZW1lbnRbMF0udGFnTmFtZS50b0xvd2VyQ2FzZSgpID09PSAnaWZyYW1lJykge1xyXG4gICAgICAgICAgICAvLyBwYXNzIHNvdXJjZSB0byBpZnJhbWVcclxuICAgICAgICAgICAgdmFyIG5vQ29va2llID0gLzF8dHJ1ZS8udGVzdChlbGVtZW50LmF0dHIoJ2RhdGEtbm9jb29raWUnKSk7XHJcbiAgICAgICAgICAgIGVsZW1lbnQuYXR0cignc3JjJywgJ2h0dHBzOi8vd3d3LnlvdXR1YmUnICsgKG5vQ29va2llID8gJy1ub2Nvb2tpZScgOiAnJykgKyAnLmNvbS9lbWJlZC8nICsgZWxlbWVudC5hdHRyKCdkYXRhLXNyYycpICsgJz9yZWw9MCZhbXA7c2hvd2luZm89MCcpO1xyXG5cclxuICAgICAgICAgICAgLy8gcmVtb3ZlIGF0dHJpYnV0ZVxyXG4gICAgICAgICAgICBpZiAodGhpcy5jb25maWcoJ3JlbW92ZUF0dHJpYnV0ZScpKSB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHIoJ2RhdGEtc3JjJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBwYXNzIGVycm9yIHN0YXRlXHJcbiAgICAgICAgICAgIHJlc3BvbnNlKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufSkod2luZG93LmpRdWVyeSB8fCB3aW5kb3cuWmVwdG8pOyJdfQ==
