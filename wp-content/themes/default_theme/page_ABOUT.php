<?php

/*
	Template Name: ABOUT
*/

get_header();

include_once(WP_TEMPLATE_PATH.'/assets/site_parts/page/page-visual.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/page/page-intro.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/page/page-blocks.php');

include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/newsletter-block.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/social-block.php');

//include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-content.php');

get_footer(); 

//Includo chiusura documento
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/__HTML-doc-close.php');
?>