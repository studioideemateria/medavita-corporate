<?php

/*
	Template Name: PRODUCT LINE - LC
*/

get_header();

$line_color_1 = get_field('line_color_1');
$line_color_2 = get_field('line_color_2');

?>

<style>
	.line_color_1{ color: <?php echo $line_color_1; ?>; }
	.line_color_1_bg{ background-color: <?php echo $line_color_1; ?>; }
	.line_color_1_border{ border-color: <?php echo $line_color_1; ?>; }
	.line_color_2{ color: <?php echo $line_color_2; ?>; }
	.line_color_2_bg{ background-color: <?php echo $line_color_2; ?>; }
	.line_color_2_border{ border-color: <?php echo $line_color_2; ?>; }
</style>

<?php 

include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-nav.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-visual.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-LC-intro.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-test.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-feat-prod-1.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-principles.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-LC-blocks.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-ritual.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/product-line/product-line-feat-prod-2.php');

include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/newsletter-block.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/social-block.php');

get_footer(); 

//Includo chiusura documento
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/__HTML-doc-close.php');
?>