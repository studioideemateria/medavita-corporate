<?php

// Registro posizioni menu in modo da poter associare ciascun menu ad una posizione sul template
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
		'headerMenu' => 'Menu Header',
		'footerMenu' => 'Menu Footer',
		)
	);
}

?>