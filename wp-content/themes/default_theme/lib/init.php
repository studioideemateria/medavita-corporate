<?php
function MM_wp_init(){
	$librariesDir = scandir(get_template_directory().'/lib/');
	$libraries = array_diff($librariesDir, array('.', '..'));
	foreach($libraries as $library) {
		if($library=='init.php') continue;
		if(substr($library, 0, 2)=='__') continue;
		include get_template_directory().'/lib/'.$library;
	}
}
?>