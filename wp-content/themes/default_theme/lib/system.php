<?php

/**
 * Clean up wp_head() | Rimuovo ogni riferimento di wordpress adi tag <head>
 */
function clean_head () {
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_shortlink_wp_head');
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);

    add_filter('the_generator', '__return_false');
    add_filter('show_admin_bar','__return_false');

    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
}
add_action('after_setup_theme', 'clean_head');

//Remove WPML generator tag from head
if (!empty( $GLOBALS['sitepress'])){
    add_action( 'wp_head', function(){
        remove_action(
            current_filter(),
            array ( $GLOBALS['sitepress'], 'meta_generator_tag' )
        );
    },0);
}


//Rimuovo il jquery di default e lo aggiungo io
function remove_default_jquery()   
{
    if (!is_admin())   
    {
        wp_deregister_script('jquery');
        // wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"), false, '3.3.1', true);
        // wp_register_script('jquery', ("/scripts/jquery-3.3.1.min.js"), false, '3.1.1', true);
        // wp_enqueue_script('jquery');
    }
}  
add_action('init', 'remove_default_jquery');




// THEMEADDON
//Aggiungo supporto per le featured image al tema
if (function_exists('add_theme_support')) { 
  add_theme_support( 'post-thumbnails' );
}

// Aggiungo supporto a vari template degli articoli (post)
add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.

//Aggiungo il supporto per i riassunti delle pagine
function aggiungi_excerpts_alle_pagine() { 
    add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'aggiungi_excerpts_alle_pagine' ); 

/*-----------------------------------------------------------------------------------
 *
 * Rimozione delle emojicons
 *
 -----------------------------------------------------------------------------------*/

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}
function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );


/*-----------------------------------------------------------------------------------
 *
 * Inclusione js
 *
 -----------------------------------------------------------------------------------*/

// function include_dist_js_file()
// { 
//   if (ONLINE == TRUE) {
//     if(file_exists(WP_TEMPLATE_PATH."/dist/scripts/functions-min.js")){
//       wp_register_script( 'custom_wp_dis_js', '/dist/scripts/functions-min.js');
//     } else {
//       wp_register_script( 'custom_wp_dis_js', '/dist/scripts/functions.js');
//     }
//   } else {
//       wp_register_script( 'custom_wp_dis_js', '/dist/scripts/functions.js');
//   }
  
//   wp_enqueue_script ( 'custom_wp_dis_js' );
// }
// add_action( 'wp_footer',  'include_dist_js_file' );

//* Remove query strings from scripts
function _remove_script_version($src){
    $parts = explode( '?ver', $src );
    return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


/**
 * Funzioni per la pagina di ricerca
 */
function fb_change_search_url_rewrite() {
  if ( is_search() && ! empty( $_GET['s'] ) ) {
    wp_redirect( home_url( "/".__('search_url', 'default_translations')."/" ) . urlencode( get_query_var( 's' ) ) );
    exit();
  } 
}
add_action( 'template_redirect', 'fb_change_search_url_rewrite' );


function custom_search_url( $search_rewrite ) {
  if( !is_array( $search_rewrite ) ) { return $search_rewrite; }

  $new_array = array();
  foreach( $search_rewrite as $pattern => $s_query_string ) {
    $new_array[ str_replace( 'search/', __('search_url', 'default_translations').'/', $pattern ) ] = $s_query_string;
  }
  $search_rewrite = $new_array;
  unset( $new_array );
  return $search_rewrite;
}
add_filter("search_rewrite_rules", "custom_search_url");

// Browser detection

add_filter('body_class','browser_body_class');
function browser_body_class($classes) {
  global $is_IE, $is_opera,  $is_safari, $is_chrome, $is_iphone, $is_gecko;

  if($is_gecko) $classes[] = 'firefox';
  elseif($is_opera) $classes[] = 'opera';
  elseif($is_safari) $classes[] = 'safari';
  elseif($is_chrome) $classes[] = 'chrome';
  elseif($is_IE) $classes[] = 'ie';
  else $classes[] = 'unknown';

  if($is_iphone) $classes[] = 'iphone';
  return $classes;
}

/* Enable SVG media upload */

function cc_mime_types($mimes) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/* Deregister Gutenberg style library */

add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}


function webp_upload_mimes( $existing_mimes ) {
  // add webp to the list of mime types
  $existing_mimes['webp'] = 'image/webp';

  // return the array back to the function with our added mime type
  return $existing_mimes;
}
add_filter( 'mime_types', 'webp_upload_mimes' );

?>