<?php

//Registrazione sidebar del sito
function add_configuration_data_sidebar() {
    register_sidebar(array(
        'name'          => 'Configurazione dati',
        'id'            => 'configurazione_dati',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ));

    register_sidebar(array(
        'name'          => 'Social network',
        'id'            => 'social_network',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ));
}
add_action('widgets_init', 'add_configuration_data_sidebar');

?>