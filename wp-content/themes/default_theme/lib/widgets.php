<?php

$widgetsDir = scandir(get_template_directory().'/widgets/');
$widgets= array_diff($widgetsDir, array('.', '..'));

foreach($widgets as $widget) {
	if(is_dir(get_template_directory().'/widgets/' . $widget)){
		$subFolder = scandir(get_template_directory().'/widgets/' . $widget);

		$subWidgets = array_diff($subFolder, array('.', '..'));

		foreach($subWidgets as $subWidget) {

			$ext = substr($subWidget, strrpos($subWidget, '.') + 1);
		
			if($ext != "php") continue;
			include get_template_directory().'/widgets/'.$widget . '/' . $subWidget;
		}
	}
	else{
		$ext = substr($widget, strrpos($widget, '.') + 1);
		
		if($ext != "php") continue;
		include get_template_directory().'/widgets/'.$widget;
	}
}

?>