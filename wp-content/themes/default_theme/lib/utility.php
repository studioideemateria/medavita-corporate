<?php 

//Funzione che printa la struttura una sidebar (cicla tutti i widget inseriti)
function get_sidebar_as_array($sidebar_name) {
    global $wp_registered_sidebars, $wp_registered_widgets;
    
    $output = array();
    
    // Loop over all of the registered sidebars looking for the one with the same name as $sidebar_name
    $sidebar_id = false;
    foreach( $wp_registered_sidebars as $sidebar ) {
        if( $sidebar['name'] == $sidebar_name ) {
            // We now have the Sidebar ID, we can stop our loop and continue.
            $sidebar_id = $sidebar['id'];
            break;
        }
    }
    
    if( !$sidebar_id ) {
        // There is no sidebar registered with the name provided.
        return $output;
    } 
    
    // A nested array in the format $sidebar_id => array( 'widget_id-1', 'widget_id-2' ... );
    $sidebars_widgets = wp_get_sidebars_widgets();
    $widget_ids = $sidebars_widgets[$sidebar_id];
    
    if( !$widget_ids ) {
        // Without proper widget_ids we can't continue. 
        return array();
    }
    
    // Loop over each widget_id so we can fetch the data out of the wp_options table.
    $counter = 1;
    foreach( $widget_ids as $id ) {

        //Se sono sulla sidebar di configurazione dei dati modifico la chiave di questo tipo "dati_aziendali-3" in "dati_aziendali" per accedervi più facilmente
        if($sidebar_name == 'Configurazione dati' || $sidebar_name == 'Social network'){
            $tmp_ = explode('-', $id);
            array_pop($tmp_);
            $arrayKey = implode('-', $tmp_);
        }else{
            $arrayKey = $id;
        }

        // The name of the option in the database is the name of the widget class.  
        $option_name = $wp_registered_widgets[$id]['callback'][0]->option_name;
        
        // Widget data is stored as an associative array. To get the right data we need to get the right key which is stored in $wp_registered_widgets
        $key = $wp_registered_widgets[$id]['params'][0]['number'];
        
        $widget_data = get_option($option_name);

        //Controllo se la chiave esiste, in caso aggiungo come stringa il valore della variabile $counter;
        if(array_key_exists($arrayKey, $output)){
            $arrayKey = $arrayKey."-".$counter;
        }
        
        // Add the widget data on to the end of the output array.
        $output[$arrayKey] = $widget_data[$key];

        $counter++;
    }
    
    return $output;
}

function get_sidebar_configurazione_dati(){
    global $configurazione_dati;
    $configurazione_dati = get_sidebar_as_array('Configurazione dati');
}
function get_sidebar_social_network(){
    global $social_network_list;
    $social_network_list = get_sidebar_as_array('Social network');
}

// Funzione che trimma il testo a una data lunghezza, se maggiore inserisce i puntini (...)
function MM_trim_text($text, $length){
    $tmp_ = trim(strip_tags($text));
    $rtn_ = strlen($tmp_) > $length ? substr($tmp_,0,$length)."..." : $tmp_;
    return $rtn_;
}


function debug( $avariable, $astring = "", $print = true, $atag = "pre" ) {
    $title = '';
    foreach($GLOBALS as $var_name => $value) {
        if ($value === $avariable) {
            $title = $var_name;
        }
    }

    $string = '<'.$atag.' title="'.$title.'">';

    if ( $astring != "" )  {
        $string .= "<strong>";
        $string .= $astring;
        $string .= "</strong>: ";
    }

    if ( isset( $avariable ) ) {
        $string .= '('.gettype( $avariable ).') ';
        
        if ( is_string( $avariable ) ) {
            $string .= $avariable;
        }
        elseif ( is_bool( $avariable ) ) {
            $avariable ? $string .= "TRUE" : $string .= "FALSE";
        }
        else {
            $string .= print_r( $avariable, true );
        }
    }
    else {
        $avariable === null ? $string .= "NULL" : $string .= "NOT DEFINED";
    }

    $string .= "</".$atag.">";

    if ( $print ) {
        print $string;
    }

    return $string;
}


// Aggiungo classe ad ogni elemento del menu con la tipologia di elemento e l'ID; questa implementazione serve per associare i link interni alla pagina con gli elementi del menu tramite data-attribute, per rendere attivi gli elementi anche dai click nelle pagine.

add_filter( 'nav_menu_css_class', 'th_nav_menu_css_class', 10, 2 );

function th_nav_menu_css_class( $classes, $item ) {
    $classes[] = 'menu-'.$item->object.'-'.$item->object_id;
    return $classes;
}

/* Custom order on posts */

add_action( 'admin_init', 'posts_order_wpse_91866' );

function posts_order_wpse_91866() {
    add_post_type_support( 'post', 'page-attributes' );
}

function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

// funzione preview news
function newsBlock($post,$news_layout){
    $news_block = "";
    $feat = get_the_post_thumbnail_url($post->ID,'medium');
    $textDate = get_the_date('j F Y', $post->ID);
    $link = get_permalink($post->ID);
    $post_cat = get_the_category($post->ID);

    $news_block.= '<div class="archNews__item archNews__item--'.$news_layout.'">';
        $news_block.= '<a class="archNews__itemWrap itemWrap" href="'.$link.'" title="'.$post->post_title.'">';
            $news_block.= '<div class="archNews__itemFeat lazy" data-src="'.$feat.'"></div>';

            $news_block.= '<div class="archNews__itemTextWrap">';
                
                $news_block.= '<div class="archNews__itemCat text-uppercase">'.$post_cat[0]->cat_name.'</div>';
                //$news_block.= '<div class="archNews__itemDate">'.$textDate.'</div>';
                $news_block.= '<h4 class="archNews__itemTit title title--md">'.$post->post_title.'</h4>';
                $news_block.= '<div class="archNews__itemExc text">'.wpautop($post->post_excerpt).'</div>';

                //$news_block.= '<span class="archNews__itemButton button">'.__('Leggi tutto','default_translations').'</span>';

                // $news_block.= do_shortcode('[addtoany url="'.get_permalink($post->ID).'" title="'.$post->post_title.'"]');

            $news_block.= '</div>';

        $news_block.= '</a>';
    $news_block.= '</div>';
    return $news_block;
}


?>