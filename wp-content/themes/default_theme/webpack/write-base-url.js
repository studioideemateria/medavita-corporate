const fs = require('fs-extra')
const path = require('path');
const inquirer = require('inquirer');
const replace = require('replace-in-file');

const root = path.resolve(`${__dirname}/../../../../`)
const defaultFolder = root.split('/').pop();

const folderToRemove = `${root}/.git`;
fs.remove(folderToRemove, err => {
  console.error("ATTENZIONE: CARTELLA .GIT NON ELIMINATA")
})

var questions = [
	{
	    type: 'input',
	    name: 'base_url',
	    message: 'What\'s the base url of the project?',
	    default: defaultFolder,
	},
]

inquirer.prompt(questions).then((answers) => {
	const aswersObj = answers;
	const { base_url } = aswersObj;

	const results = replace.sync({
		files: [
			`${root}/.htaccess`,
			`${root}/sftp-config.json`,
			`${root}/wp-content/themes/default_theme/assets/site_parts/actionFooter.php`,
			`${root}/wp-content/themes/default_theme/webpack/webpack.config.js`,
		],
		from: /wp-base/g,
		to: base_url,
	});

	console.log(results);
	console.log('DONE!');

});
