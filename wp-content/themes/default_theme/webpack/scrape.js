const fs = require('fs-extra')
const inquirer = require('inquirer');

// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra')

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

let links = [];
var questions = [
	{
	    type: 'input',
	    name: 'siteUrl',
	    message: 'Di quale sito desideri eseguire la ricerca delle url?',
	    default: 'www.inmateria.net'
	},
	{
	    type: 'input',
	    name: 'pages',
	    message: 'Quante pagine dei risultati vuoi analizzare?',
	    validate: function (value) {
	    	var valid = !isNaN(parseFloat(value));
	    	return valid || 'Per favore inserisci un numero';
	    },
	    filter: Number,
	    default: 3
	},
	{
	    type: 'confirm',
	    name: 'debug',
	    message: 'Vuoi entrare in modalità di debug?',
	    default: false
	},
	{
	    type: 'confirm',
	    name: 'captcha',
	    message: 'Hai bisogno di superare un recaptcha manualmente?',
	    default: false
	},
]

inquirer.prompt(questions).then((answers) => {
	let pagesNum = answers.pages;
	const siteUrl = answers.siteUrl;
	const TIMEOUT = answers.captcha ? 60*1000 : 5*1000
	const debug = answers.captcha ? true : answers.debug;
	const options = {
		args: ['--lang=it-IT,it'],
		headless: !debug,
		slowMo: (debug) ? 100 : 0,
		devtools: debug
	}

	puppeteer.launch(options).then(async browser => {
		console.log('Inizio dei test... 🤘')
		// Creo il file CSV
		fs.writeFile('scrape.csv', 'source,target,regex,type,code,match,hits,title\r\n', 'utf8', function (err) {
		  console.log('Creo file CSV... 📝')
		})

		// Accedo alla pagina di google
		const page = await browser.newPage()
		await page.goto('https://www.google.it')

		// Aspetto iframe per privacy e clicco su accetto.
		const elementHandle = await page.$('iframe');
		const frame = await elementHandle.contentFrame();
		await frame.waitForXPath("//span[contains(text(), 'Accetto')]");
		const buttonInFrame = await frame.$x("//span[contains(text(), 'Accetto')]")
		await buttonInFrame[0].click()

		// Inserisco la chiave di ricerca e ci vado
		await page.waitForSelector('input[name="q"]', { timeout: TIMEOUT })
		await page.$eval('input[name="q"]', (el, siteUrl) => el.value = `site:${siteUrl}`, siteUrl);
		await page.keyboard.press(String.fromCharCode(13));

		// Controllo la prima pagina, se non trovo il selettore probabilmente mi trovo davanti a un captcha
		try {
			console.log('Analisi pagina 1... ⏰')
			await page.waitForSelector('#search', { timeout: TIMEOUT })
			let tempLink = await page.$$eval('#search a[ping]:not(.fl)', anchors => [].map.call(anchors, a => a.href));
			links.push(tempLink);
		} catch (error) {
			console.log("❌ Errore nella ricerca: probabilmente si richiede l'inserimento di un captcha. Riprovare con la modalità recaptcha attiva. ❌")
			process.exit()
		}

		// Loop basato sul numero di pagine da analizzare inserito
		for(var i = 2; i <= pagesNum; i++ ) {
			try {
				// Controllo la presenza del bottone next per sapere se ci sono altre pagine
				await page.waitForSelector('#pnnext', { timeout: TIMEOUT })
				console.log(`Analisi pagina ${i}... ⏰`)
				const navigationButton = await page.$eval(`#pnnext`,  el => el.click());

				// Aspetto caricamento della nuova pagina e cerco i risultati
				await page.waitForSelector('#search', { timeout: TIMEOUT })
				let tempLink = await page.$$eval('#search a[ping]:not(.fl)', anchors => [].map.call(anchors, a => a.href));
		 		links.push(tempLink);
			} catch (error) {
				console.log("Non ci sono ulteriori pagine di risultati di ricerca... ❌")
				break;
			}
		}

		// Scrivo tutto su un array monodimensionale
		links = links.reduce((a,b) => a.concat(b), []);

		// Rimuovo le voci duplicate
		let uniqueLinks = [...new Set(links)];

		// Compilo il csv da importare
		for(var i = 0; i < uniqueLinks.length; i++ ) {
			fs.appendFile('scrape.csv', `${uniqueLinks[i]}, , 0, url, 301, url, 1, ,\r\n`, 'utf8', function (err) {
				if(err){
					console.log(err)
				}
			})
		}
		console.log('CSV scritto correttamente... 📝')

		await browser.close()

		console.log(`Tutto fatto, l'export si trova nella cartella webpack ✨🦄`)

	})
});