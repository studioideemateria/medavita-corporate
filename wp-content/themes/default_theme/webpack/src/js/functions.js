import $ from 'jquery'
import '../sass/main.scss';
import menu from './menu';
import lazyLoad from './lazyLoad';
import smoothState from './smoothState';
import 'slick-carousel';

window.$ = require('jquery');
window.jQuery = require('jquery');

const siteGlobal = {
  initReady() {
    smoothState.initReady();
  },
  initLoad() {
  	lazyLoad.initLoad();
    menu.initLoad();
    this.homeSlider();
    this.homeSolutions();
    this.princSlider();
  },
  initScroll( ){
  },
  initResize() {
  },
  homeSlider: function(){
    if ($('.homeSlider__slider').length) {
      $('.homeSlider__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        speed: 400,
        infinite: false,
        autoplay: false,
        autoplaySpeed: 5000,
        lazyLoad: 'progressive'
      });
    }
  },
  homeSolutions: function () {
    if ($('.homeSolutions__list').length) {
      $('.homeSolutions__list').slick({
        centerMode: true,
        centerPadding: '35px',
        slidesToShow: 7,
        arrows: true,
        dots: false,
        variableWidth: true,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
                slidesToShow: 5
            }
          }
        ]
      });
    }
  },
  
  princSlider: function(){
    if ($('.princSlider').length) {
      $('.princSlider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        speed: 300,
        infinite: false,
        autoplay: false,
        autoplaySpeed: 5000,
        lazyLoad: 'progressive',
        responsive: [
          {
            breakpoint: 1200,
            settings: {
                slidesToShow: 2,
            }
          },
          {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
            }
          }
        ]
      });
    }
  },
  rebuildAllEvents() {
    siteGlobal.initLoad();
  },
};

$(document).ready(() => {
  siteGlobal.initReady();
});

$(document).on('rebuild', () => {
  siteGlobal.rebuildAllEvents();
});

$(window).on('load', () => {
  $('#preloader').addClass('loaded');
  siteGlobal.initLoad();
});

$(window).on('resize', () => {
  siteGlobal.initResize();
});

$(window).on('scroll', () => {
  siteGlobal.initScroll();
});