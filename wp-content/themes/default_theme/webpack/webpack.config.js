const webpack = require('webpack');
const path = require('path'),
      MiniCssExtractPlugin = require("mini-css-extract-plugin"),
      BrowserSyncPlugin = require('browser-sync-webpack-plugin');
      DeployPlugin = require('deploy-kit/plugins/ftp-webpack-plugin')

module.exports = (env, argv) => ({
  mode: argv.mode,
  watch: true,
  devtool: 'source-map',
  entry: {
    functions: './src/js/functions.js'
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].js',
  },
  optimization: {
    /* prevent duplicates */
    splitChunks: {
      chunks: 'all'
    },
  },
  module: {
    rules: [
      /* ES6 babel */
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      /* Sass compiler */
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
               {
                 loader: MiniCssExtractPlugin.loader
               },
               {
                 loader: "css-loader",
               },
               {
                 loader: "postcss-loader"
               },
               {
                 loader: "sass-loader",
                 options: {
                   implementation: require("sass")
                 }
               }
             ]
      },
      /* BG images from sass. It creates a directory inside dist with all images imported in css files */
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
               {
                 loader: "file-loader",
                 options: {
                   outputPath: 'images'
                 }
               }
             ]
      },
      /* Fonts directory. It creates a directory inside dist with all fontsimported in css files */
      {
        test: /\.(woff|woff2|ttf|otf|eot)$/,
        use: [
               {
                 loader: "file-loader",
                 options: {
                   outputPath: 'fonts'
                 }
               }
             ]
      }
    ]
  },
  plugins: [
    /* Extracts CSS from sass files and compiles it into a file */
    new MiniCssExtractPlugin({ filename: "main.css" }),
    
    /* Browsersync */
    new BrowserSyncPlugin({
      proxy: 'http://localhost:8888/medavita-corporate/',
      files: [{ match: [ '../**/*.php' ],
        fn: function(event, file) {
            if (event === "change") {
                const bs = require('browser-sync').get('bs-webpack-plugin');
                bs.reload();
            }
      }}]
    }),

    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),

    /*** DA DECOMMENTARE QUANDO SI E' IN DEMO O PRODUZIONE **/
    // new DeployPlugin({
    //   // server: 'user:pwd@10.133.3.4:22',
    //   server: 'andrea@inmateria.net:Pelacchia1981@it38.siteground.eu:21',
    //   // deploy all files in the directory
    //   workspace: __dirname + '../dist',
    //   // where the files are placed on the server
    //   deployTo: '/public_html/test/wp-content/themes/default_theme/dist/',
    // })

  ]
});