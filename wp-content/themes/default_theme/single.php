<?php

get_header();
echo '<div class="singlePost__wrap container">';
	include_once(WP_TEMPLATE_PATH.'/assets/site_parts/single/single-content.php');
	include_once(WP_TEMPLATE_PATH.'/assets/site_parts/single/single-sidebar.php');
echo '</div>';
get_footer(); 

//Includo chiusura documento
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/__HTML-doc-close.php');
?>