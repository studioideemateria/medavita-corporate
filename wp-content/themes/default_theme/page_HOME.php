<?php

/*
	Template Name: HOME
*/

get_header();

include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-slider.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-intro.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-solutions-feat.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-solutions.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-products-feat.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-magazine.php');

include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/newsletter-block.php');
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/social-block.php');

//include_once(WP_TEMPLATE_PATH.'/assets/site_parts/home/home-content.php');

get_footer(); 

//Includo chiusura documento
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/__HTML-doc-close.php');
?>