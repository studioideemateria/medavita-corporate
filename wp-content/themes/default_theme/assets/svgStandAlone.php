<?php 
header('Content-type: image/svg+xml');  
echo '<?xml version="1.0" encoding="iso-8859-1"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
if(isset($_GET['url'])){
  $svg = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$_GET['url']);

  // Colore
  if(isset($_GET['color'])){
    $svg = str_replace('<g', '<g fill="#'.$_GET['color'].'"', $svg);
  }

  // Larghezza
  if(isset($_GET['w'])){
    $svg = preg_replace('/width="(\\d+)px"/im', 'width="'.$_GET['w'].'px"', $svg);
  }

  // Altezza
  if(isset($_GET['h'])){
    $svg = preg_replace('/height="(\\d+)px"/im', 'height="'.$_GET['h'].'px"', $svg);  
  }
  echo $svg;
}
?>