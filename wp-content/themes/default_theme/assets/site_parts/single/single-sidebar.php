<?php 
	$args = array(
		'posts_per_page' => 4, 
		//'category' => ID_C_BLOG,
		'exclude' => array($post->ID),
	);
	$post_list = get_posts($args);

	$single_sidebar = "";
	$single_sidebar.= '<aside class="singlePost__sidebar col-md-3">';

		$single_sidebar.= '<h3 class="singlePost__sidebarTitle text-uppercase">'.__('Altri dal blog','default_translations').'</h3>';

		$blog_count = 0;
		foreach ($post_list as $post) {

			$news_layout = 'default';

			$single_sidebar.= newsBlock($post,$news_layout);
			$blog_count++;
		}

	if ( is_active_sidebar( 'blog-sidebar' ) ) {
        //$single_sidebar .= get_sidebar('blog-sidebar');
	}

	$single_sidebar.= '</aside>';
	echo $single_sidebar;
?>