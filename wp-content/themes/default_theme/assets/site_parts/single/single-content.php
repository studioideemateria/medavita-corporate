<?php 
	$post_date = get_the_date( 'd.m.Y', $post->ID);
	$feat = get_the_post_thumbnail_url($post->ID,'large');
	//$sub = get_the_subtitle($post->ID,'','',false);
	$post_cat = get_the_category($post->ID);

	$single_cont = "";
	$single_cont.= '<article class="singlePost__content col-md-9">';
		
		$single_cont.= '<a class="singlePost__back" href="'.get_category_link(1).'" title="'.__('Tutti gli articoli').'">'.__('Tutti gli articoli').'</a>';
		//$single_cont.= '<span class="newsList__date">'.$post_date.'</span>';
		$single_cont.= '<div class="singlePost__cat text-uppercase">'.$post_cat[0]->cat_name.'</div>';
		$single_cont.= '<h1 class="singlePost__tit title title--md text-uppercase">'.$post->post_title.'</h1>';
		
		// if ($sub) {
		// 	$single_cont.= '<div class="singlePost__sub text">'.$sub.'</div>';
		// }

		if ($feat) {
			$single_cont.= '<img class="singlePost__feat lazy" data-src="'.$feat.'" alt="'.$post->post_title.'" />';
		}
		$single_cont.= '<div class="singlePost__cont text">';

			$single_cont.= wpautop($post->post_content);

			/*
			$single_cont.='<div class="singlePost__blocks pageBlocks">';
				$single_cont.= include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/page-blocks.php');
			$single_cont.= '</div>';
			*/

		$single_cont.= '</div>';		

	$single_cont.= '</article>';
	echo $single_cont;
?>