<?php 

	$line_test_text = get_field('line_test_text');
	$line_test_notes = get_field('line_test_notes');
	
	$prod_line_test = "";
	$prod_line_test.= '<section class="productLine__test">';
	    
		$prod_line_test.= '<div class="container">';
				
			if( have_rows('line_test_percentage') ):

	        	$prod_line_test.= '<div class="productLine__testPerc container">';
				    
				    while( have_rows('line_test_percentage') ): the_row(); 
				        
				        $test_percentage_image = get_sub_field('test_percentage_image');
				        $test_percentage_image_url = $test_percentage_image['url'];
				        $test_percentage_image_w = $test_percentage_image['width'] / 2;

				        $prod_line_test.= '<div class="productLine__testPerc__el text-center">';
				        	$prod_line_test.= '<img class="productLine__testPerc__elImg" src="'.esc_url($test_percentage_image_url).'" width="'.$test_percentage_image_w.'">';
				        $prod_line_test.= '</div>';
				        
				    endwhile;
				
				$prod_line_test.= '</div>';

	    	endif;

	    	if($line_test_text) $prod_line_test.= '<h2 class="productLine__testTitle title title--sm text-uppercase text-center line_color_1 container">'.$line_test_text.'</h2>';

			if( have_rows('line_test_list') ):

	        	$prod_line_test.= '<div class="productLine__testList container line_color_1 line_color_2_border">';
				    
				    while( have_rows('line_test_list') ): the_row(); 
				        
				        $test_list_image = get_sub_field('test_list_image');
				        $test_list_image_url = $test_list_image['url'];
				        $test_list_image_w = $test_list_image['width'] / 2;
				        $test_list_desc = get_sub_field('test_list_desc');

				        $prod_line_test.= '<div class="productLine__testList__elWrap line_color_2_border">';
				        	$prod_line_test.= '<div class="productLine__testList__el">';
					        	$prod_line_test.= '<img class="productLine__testList__elImg" src="'.esc_url($test_list_image_url).'" width="'.$test_list_image_w.'">';
					        	$prod_line_test.= '<h5 class="productLine__princList__elName title title--xs ">'.$test_list_desc.'</h5>';
					        $prod_line_test.= '</div>';
					    $prod_line_test.= '</div>';
				        
				    endwhile;
				
				$prod_line_test.= '</div>';

	    	endif;

	    	if($line_test_notes) $prod_line_test.= '<div class="productLine__testNotes text text-center container">'.$line_test_notes.'</h2>';

        $prod_line_test.= '</div>';
	      
	$prod_line_test.= '</section>';

	echo $prod_line_test;
?>