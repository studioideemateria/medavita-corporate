<?php 

	$line_ritual_text = get_field('line_ritual_text');
	$line_ritual_video = get_field('line_ritual_video');

	$prod_line_ritual = "";
	$prod_line_ritual.= '<section class="productLine__ritual">';

		$prod_line_ritual.= '<h3 class="productLine__ritualTitle title title--sm text-uppercase text-center">'.__('IL RITUALE', 'default_translations').'</h3>';
		$prod_line_ritual.= '<div class="productLine__ritualText text text-center">'.$line_ritual_text.'</div>';

		$prod_line_ritual.= '<div class="productLine__ritualVideo__wrap container">';
			if ($line_ritual_video) {
				$prod_line_ritual.= '<video muted loop playsinline controls class="productLine__ritualVideo">';
					$prod_line_ritual.= '<source src="'.$line_ritual_video['url'].'" type="video/mp4">';
				$prod_line_ritual.= '</video>';
			}
		$prod_line_ritual.= '</div>';
	      
	$prod_line_ritual.= '</section>';

	echo $prod_line_ritual;
?>