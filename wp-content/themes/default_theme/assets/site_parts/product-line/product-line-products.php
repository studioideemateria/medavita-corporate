<?php 

	$line_ritual_products = get_field('line_ritual_products');

	$prod_line_prod = "";
	$prod_line_prod.= '<section class="productLine__prods">';

		$prod_line_prod.= '<h3 class="productLine__prodsTitle title title--sm title--underline text-uppercase text-center">'.__('Prodotti', 'default_translations').'</h3>';

		$prod_line_prod.= '<div class="productLine__prodsList container">'.do_shortcode($line_ritual_products).'</div>';
	      
	$prod_line_prod.= '</section>';

	echo $prod_line_prod;
?>