<?php 
	
	$prod_line_princ = "";
	$prod_line_princ.= '<section class="productLine__princ line_color_2_bg">';
	    
		$prod_line_princ.= '<div class="container">';
				
			$prod_line_princ.= '<h3 class="productLine__princSubtitle title--underline text-uppercase text-center">'.__('PRINCIPI ATTIVI', 'default_translations').'</h3>';
			$prod_line_princ.= '<h2 class="productLine__princTitle title title--sm text-uppercase text-center">'.__('Il cuore della linea', 'default_translations').'</h2>';

			if( have_rows('line_principles_list') ):

	        	$prod_line_princ.= '<div class="productLine__princList princSlider">';
				    
				    while( have_rows('line_principles_list') ): the_row(); 
				        
				        $principle_name = get_sub_field('principle_name');
				        $principle_name_latin = get_sub_field('principle_name_latin');
				        $principle_desc = get_sub_field('principle_desc');
				        $principle_image = get_sub_field('principle_image');

				        $prod_line_princ.= '<div class="productLine__princList__el">';
				        	$prod_line_princ.= '<img class="productLine__princList__elImg" src="'.esc_url($principle_image['url']).'">';
				        	$prod_line_princ.= '<h5 class="productLine__princList__elName title title--sm text-uppercase">'.$principle_name.'</h5>';
				        	if($principle_name_latin) $prod_line_princ.= '<h5 class="productLine__princList__elNameLatin text-uppercase">'.$principle_name_latin.'</h5>';
				        	$prod_line_princ.= '<div class="productLine__princList__elDesc">'.$principle_desc.'</div>';
				        $prod_line_princ.= '</div>';
				        
				    endwhile;
				
				$prod_line_princ.= '</div>';

	    	endif;

        $prod_line_princ.= '</div>';
	      
	$prod_line_princ.= '</section>';

	echo $prod_line_princ;
?>