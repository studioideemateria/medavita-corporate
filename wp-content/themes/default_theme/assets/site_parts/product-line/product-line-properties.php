<?php 
	
	$line_property_title = get_field('line_property_title');
	$line_property_text = get_field('line_property_text');
	$line_property_image = get_field('line_property_image');

	$prod_line_prop = "";
	$prod_line_prop.= '<section class="productLine__prop container">';
	      
		$prod_line_prop.= '<div class="productLine__propText__wrap">';
			$prod_line_prop.= '<h2 class="productLine__propTitle title title--lg text-uppercase line_color_1">'.$line_property_title.'</h2>';
			$prod_line_prop.= '<div class="productLine__propText text">'.$line_property_text.'</div>';
		$prod_line_prop.= '</div>';

		$prod_line_prop.= '<div class="productLine__propFeat__wrap">';
			$prod_line_prop.= '<img class="productLine__propFeat lazy" data-src="'.esc_url($line_property_image['url']).'">';
		$prod_line_prop.= '</div>';
	      
	$prod_line_prop.= '</section>';

	echo $prod_line_prop;
?>