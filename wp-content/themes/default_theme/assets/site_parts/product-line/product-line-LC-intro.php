<?php 
	$line_intro_text = get_field('line_intro_text');
	$line_intro_solution_text = get_field('line_intro_solution_text');

	$line_intro_video = get_field('line_intro_video');

	$prod_line_intro = "";
	$prod_line_intro.= '<section class="productLine__intro productLine__intro--LC">';
	      
		$prod_line_intro.= '<div class="productLine__introWrap white container">';

			$prod_line_intro.= '<div class="productLine__introText productLine__text line_color_1_bg text text-center">'.$line_intro_text.'</div>';
			$prod_line_intro.= '<div class="productLine__introText__sol line_color_2 text">'.$line_intro_solution_text.'</div>';

		$prod_line_intro.= '</div>';
	      
	$prod_line_intro.= '</section>';

	echo $prod_line_intro;
?>