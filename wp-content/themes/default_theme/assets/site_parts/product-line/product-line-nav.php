<?php 

	$prod_line_nav = "";
	$prod_line_nav.= '<nav class="productLine__nav line_color_2_bg">';
	      
		$prod_line_nav.= '<div class="productLine__navTitle text-uppercase">'.$post->post_title.'</div>';
		
		$prod_line_nav.= '<ul class="productLine__navMenu">';
			$prod_line_nav.= '<li class="productLine__navItem text-uppercase">'.__('Sezione 1', 'default_translations').'</li>';
			$prod_line_nav.= '<li class="productLine__navItem text-uppercase">'.__('Sezione 2', 'default_translations').'</li>';
			$prod_line_nav.= '<li class="productLine__navItem text-uppercase">'.__('Sezione 3', 'default_translations').'</li>';
		$prod_line_nav.= '</ul>';

		$prod_line_nav.= '<a href="#" class="productLine__navShop btn btn--black">'.__('Acquista', 'default_translations').'</a>';
	      
	$prod_line_nav.= '</nav>';

	echo $prod_line_nav;
?>