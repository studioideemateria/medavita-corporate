<?php 
	
	$line_feat_product_1_image = get_field('line_feat_product_1_image');
	$line_feat_product_1_image_url = $line_feat_product_1_image['url'];
	$line_feat_product_1_image_w = $line_feat_product_1_image['width'] / 2;

	$line_feat_product_1_title = get_field('line_feat_product_1_title');
	$line_feat_product_1_text = get_field('line_feat_product_1_text');
	$line_feat_product_1_button = get_field('line_feat_product_1_button');

	$prod_line_feat_1.= '<section class="pageBlocks__cib container text-center center-block ">';
	    
	    $prod_line_feat_1.= '<img class="pageBlocks__cibImage lazy" data-src="'.esc_url($line_feat_product_1_image_url).'" alt="" width="'.$line_feat_product_1_image_w.'" alt="'.$line_feat_product_1_title.'">';
		$prod_line_feat_1.= '<h2 class="pageBlocks__cibTitle title title--sm text-center text-uppercase line_color_1">'.$line_feat_product_1_title.'</h2>';
		if($line_feat_product_1_text) $prod_line_feat_1.= '<div class="pageBlocks__cibText text text-center">'.$line_feat_product_1_text.'</div>';
		$prod_line_feat_1.= '<a class="pageBlocks__cibBtn btn btn--white btn--whiteOutline text-uppercase" href="'.$line_feat_product_1_button['url'].'" title="'.$line_feat_product_1_button['title'].'">'.$line_feat_product_1_button['title'].'</a>';
	    
	$prod_line_feat_1.= '</section>';

	echo $prod_line_feat_1;
?>