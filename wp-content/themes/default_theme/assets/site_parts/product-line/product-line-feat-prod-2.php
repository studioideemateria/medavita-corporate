<?php 
	
	$line_feat_product_2_image = get_field('line_feat_product_2_image');
	$line_feat_product_2_image_url = $line_feat_product_2_image['url'];
	$line_feat_product_2_image_w = $line_feat_product_2_image['width'] / 2;

	$line_feat_product_2_title = get_field('line_feat_product_2_title');
	$line_feat_product_2_text = get_field('line_feat_product_2_text');
	$line_feat_product_2_button = get_field('line_feat_product_2_button');

	$prod_line_feat_2.= '<section class="pageBlocks__cib container text-center center-block ">';
	    
	    $prod_line_feat_2.= '<img class="pageBlocks__cibImage lazy" data-src="'.esc_url($line_feat_product_2_image_url).'" alt="" width="'.$line_feat_product_2_image_w.'" alt="'.$line_feat_product_2_title.'">';
		$prod_line_feat_2.= '<h2 class="pageBlocks__cibTitle title title--sm text-center text-uppercase line_color_2">'.$line_feat_product_2_title.'</h2>';
		if($line_feat_product_2_text) $prod_line_feat_2.= '<div class="pageBlocks__cibText text text-center">'.$line_feat_product_2_text.'</div>';
		$prod_line_feat_2.= '<a class="pageBlocks__cibBtn btn btn--white btn--whiteOutline text-uppercase" href="'.$line_feat_product_2_button['url'].'" title="'.$line_feat_product_2_button['title'].'">'.$line_feat_product_2_button['title'].'</a>';
	    
	$prod_line_feat_2.= '</section>';

	echo $prod_line_feat_2;
?>