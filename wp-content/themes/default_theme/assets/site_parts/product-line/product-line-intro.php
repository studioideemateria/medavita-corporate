<?php 
	$line_intro_text = get_field('line_intro_text');
	$line_intro_solution_text = get_field('line_intro_solution_text');

	$line_intro_video = get_field('line_intro_video');

	$prod_line_intro = "";
	$prod_line_intro.= '<section class="productLine__intro line_color_1_bg">';
	      
		$prod_line_intro.= '<div class="productLine__introWrap container">';

			$prod_line_intro.= '<div class="productLine__introText productLine__text line_color_2_bg text">'.$line_intro_text.'</div>';
			$prod_line_intro.= '<div class="productLine__introText__sol line_color_2 text">'.$line_intro_solution_text.'</div>';

			$prod_line_intro.= '<div class="productLine__introVideo text-center">';
			
				$prod_line_intro.= '<div class="title title--md line_color_2">'.$line_title.'</div>';

				if ($line_intro_video) {
					$prod_line_intro.= '<video muted loop playsinline controls class="productLine__ritualVideo">';
						$prod_line_intro.= '<source src="'.$line_intro_video['url'].'" type="video/mp4">';
					$prod_line_intro.= '</video>';
				}

			$prod_line_intro.= '</div>';

		$prod_line_intro.= '</div>';
	      
	$prod_line_intro.= '</section>';

	echo $prod_line_intro;
?>