<?php 
	$line_full_name = get_field('line_full_name');
	$line_full_name ? $line_title = $line_full_name : $line_title = $post->post_title;

	$line_visual_image = get_field('line_visual_image');

	$prod_line_visual = "";
	$prod_line_visual.= '<section class="productLine__visual lazy" data-src="'.esc_url($line_visual_image['url']).'">';
	      
		$prod_line_visual.= '<h1 class="productLine__title title title--xl text-uppercase white text-center">'.$line_title.'</h1>';
	      
	$prod_line_visual.= '</section>';

	echo $prod_line_visual;
?>