<!DOCTYPE html>
<?php 
	('ICL_LANGUAGE_CODE'!="" && 'ICL_LANGUAGE_CODE'!="ICL_LANGUAGE_CODE") ? $lang = 'ICL_LANGUAGE_CODE': $lang = 'en';
?>
<html lang="<?php echo $lang; ?>">
<head>
<?php
	include_once(WP_TEMPLATE_PATH.'/assets/site_parts/head.php');    
	wp_head();
?>

<link rel="preconnect" href="https://cdnjs.cloudflare.com/" />
<link rel="preconnect" href="https://ajax.googleapis.com/" />

<style>
	#preloader{position:fixed;top:0;left:0;width:100%;height:100%;max-width:100%;max-height:100%;background-color:#ffffff;z-index:100000;overflow:hidden}#preloader.loaded{opacity:0;z-index:-1;transition:opacity .25s linear .1s,z-index .1s linear .3s;-webkit-transition:opacity .25s linear .1s,z-index .1s linear .3s;-moz-transition:opacity .25s linear .1s,z-index .1s linear .3s;-o-transition:opacity .25s linear .1s,z-index .1s linear .3s}
	}
</style>

</head>
<body <?php global $bodyAddClass; body_class($bodyAddClass); ?>>
	<div id="preloader"></div>
	
	<main id="main">
		<?php include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/header.php'); ?>
		<div id="main_cont">