<?php include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/footer-block.php'); ?>

</div> <!-- chiusura contenuto per animazione -->
</main> <!-- chiusura smoothstate -->

<!--link href="/medavita-corporate/css/fonts.css" rel="stylesheet" rel="preload" as="style"-->
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;800&family=Playfair+Display&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo site_url() ?>/dist/main.css">

<script src="/medavita-corporate/dist/vendors~functions.js"></script>
<script src="/medavita-corporate/dist/functions.js"></script>

<?php 
	//Includo chiusura documento
	include_once(WP_TEMPLATE_PATH.'/assets/site_parts/__HTML-doc-close.php');
?>