<?php 
    $error.= '<div class="commonPage errorPage">';
	    $error.= '<div class="commonPage__wrap">';
	        $error.= '<h2 class="commonPage__title title">'.__('Errore 404: pagina non trovata' , 'default_translations').'</h2><br />';
	        $error.= '<a href="'.get_site_url().'" class="button error__link">'.__('Torna alla home page' , 'default_translations').'</a>';
	    $error.= '</div>';
    $error.= '</div>';
    echo $error;
?>