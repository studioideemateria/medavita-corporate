<?php 

// contenuto flessibile pagine

if( have_rows('page_blocks') ):

	$page_blocks = "";

    while( have_rows('page_blocks') ): the_row();

        // blocco contenuto testuale

        if( get_row_layout() == 'image_text_block' ):
	
			$text = get_sub_field('text');
			$image = get_sub_field('image');
			$image_position = get_sub_field('image_position');

			$page_blocks.= '<section class="pageBlocks__itb pageBlocks__itb--image'.$image_position.' container">';
			      
				$page_blocks.= '<div class="pageBlocks__itbText__wrap">';
					$page_blocks.= '<h2 class="pageBlocks__itbTitle title title--lg text-uppercase line_color_1">'.$title.'</h2>';
					$page_blocks.= '<div class="pageBlocks__itbText pageBlocks__text text">'.$text.'</div>';
				$page_blocks.= '</div>';

				$page_blocks.= '<div class="pageBlocks__itbImage__wrap">';
					$page_blocks.= '<img class="pageBlocks__itbImage lazy" data-src="'.esc_url($image['url']).'">';
				$page_blocks.= '</div>';
			      
			$page_blocks.= '</section>';

		elseif( get_row_layout() == 'visual_block' ):
	
			$visual_image = get_sub_field('visual_image');
			$visual_video = get_sub_field('visual_video');
			$visual_title = get_sub_field('visual_title');

			$page_blocks.= '<section class="pageBlocks__vb lazy" data-src="'.esc_url($visual_image['url']).'">';

				if ($visual_video) {
					$page_blocks.= '<video muted loop playsinline autoplay="autoplay" class="pageBlocks__vbVideo">';
						$page_blocks.= '<source src="'.$visual_video['url'].'" type="video/mp4">';
					$page_blocks.= '</video>';
				} 
			      
				$page_blocks.= '<h2 class="pageBlocks__vbTitle title title--xl text-uppercase white text-center">'.$visual_title.'</h2>';
			      
			$page_blocks.= '</section>';

		elseif( get_row_layout() == 'center_image_block' ):

			$image = get_sub_field('image');
			$title = get_sub_field('title');
			$button = get_sub_field('button');

			$page_blocks.= '<section class="pageBlocks__cib container text-center center-block ">';
			    
			    $page_blocks.= '<img class="pageBlocks__cibImage lazy" data-src="'.esc_url($image['url']).'">';
				$page_blocks.= '<h2 class="pageBlocks__cibTitle title title--sm text-center text-uppercase">'.$title.'</h2>';
				$page_blocks.= '<a class="pageBlocks__cibBtn btn btn--white btn--whiteOutline" href="'.$button['url'].'" title="scopri">'.$button['title'].'</a>';
			    
			$page_blocks.= '</section>';

		endif;

	endwhile;

endif;

	echo $page_blocks;
?>