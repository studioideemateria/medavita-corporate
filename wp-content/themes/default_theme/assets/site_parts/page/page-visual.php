<?php 
	$feat = get_the_post_thumbnail_url($post->ID,'large');

	$page_full_title = get_field('page_full_title');
	$page_full_title ? $page_title = $page_full_title : $page_title = $post->post_title;

	$page_visual_image = get_field('page_visual_image');

	$page_visual = "";
	$page_visual.= '<section class="page__visual lazy" data-src="'.esc_url($page_visual_image['url']).'">';
	      
		$page_visual.= '<h1 class="page__title title title--xl text-uppercase white text-center">'.$page_title.'</h1>';
	      
	$page_visual.= '</section>';

	echo $page_visual;
?>