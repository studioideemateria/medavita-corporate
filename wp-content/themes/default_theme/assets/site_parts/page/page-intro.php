<?php 
	$page_intro_text = get_field('page_intro_text');

	$page_intro = "";
	$page_intro.= '<section class="page__intro">';
	      
		$page_intro.= '<div class="page__introWrap container">';

			$page_intro.= '<div class="page__introText page__text text">'.$page_intro_text.'</div>';

		$page_intro.= '</div>';
	      
	$page_intro.= '</section>';

	echo $page_intro;
?>