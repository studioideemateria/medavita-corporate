<?php 

	$curr_id = get_queried_object();
	$args = array('posts_per_page' => -1,'category'=> $curr_id->term_id, 'paged' => 1);
	$news_list = get_posts($args);

	$news_count = count($news_list);
	$news = "";
	$news.= '<div class="archNews">';
		$news.= '<h1 class="archNews__tit title title--sm">'.$curr_id->name.'</h1>';

		$news.= '<div class="archNews__list">';
			
			$news_count = 0;

			foreach ($news_list as $np) {

				// griglia a 6 blocchi
				if($news_count == 6) { $news_count = 1; } else { $news_count++; } // se arrivo a 7 riparto, else ++
				
				if($news_count == 1){
					$news_layout = 'featured';
				} else if ( $news_count == 5 || $news_count == 6 ) {
					$news_layout = 'grey';
				} else {
					$news_layout = 'default';
				}

				$news.= newsBlock($np,$news_layout);



			}

		$news.= '</div>';

		$news.= '<span class="loader"></span>';

		$news.= '<div class="button" data-page="2">'.__('Carica altre','default_translations').'</div>';
	$news.= '</div>';
	
?>

<?php echo $news; ?>