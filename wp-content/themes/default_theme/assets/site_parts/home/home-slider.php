<?php 
	
	if (have_rows('slide_list')) :

			$home_slider = "";
			$home_slider.= '<section class="homeSlider container">';
				$home_slider.= '<div class="homeSlider__slider">';
			    
				while (have_rows('slide_list')) : the_row();

					$slide_mode = get_sub_field('slide_mode');
					$slide_background = get_sub_field('slide_background');
					$slide_video = get_sub_field('slide_video');
					$slide_product_image = get_sub_field('slide_product_image');
					$slide_title = get_sub_field('slide_title');
					$slide_text = get_sub_field('slide_text');
					$slide_link = get_sub_field('slide_link');
					$slide_link_text = get_sub_field('slide_link_text');
					

					if($slide_background) { $slide_background_image = $slide_background['url']; } else { $slide_background_image = 'none'; }

				      $home_slider.= '<div class="slide">';

				      	$home_slider.= '<div class="slide__wrap" style="background-image: url('.$slide_background_image.');">';

				      		if($slide_video) {

					      		$home_slider.= '<video muted loop playsinline autoplay class="slide__video">';
					      			$home_slider.= '<source src="'.$slide_video['url'].'" type="video/mp4" />';
					    		$home_slider.= '</video>';

					    	}
					    
					        $home_slider.= '<div class="slide__cont">';
					          $home_slider.= '<div class="pull-right white">';
					            if($slide_product_image) $home_slider.= '<img class="slide__prod" src="'.$slide_product_image['url'].'" alt="'.$slide_product_image['alt'].'">';
					            if($slide_title) $home_slider.= '<h3 class="slide__title title title--xl text-uppercase">'.$slide_title.'</h3>';
					            if($slide_text) $home_slider.= '<p class="slide__text text text-uppercase white">'.$slide_text.'</p>';
					            
					            if($slide_link) {
					            	$home_slider.= '<a class="slide__btn btn btn--white" href="'.$slide_link.'" title="'.$slide_link_text.'">';
					            		if($slide_link_text) { $home_slider .= $slide_link_text; } else { $home_slider .= __('Scopri di più','default_translations'); }
					            	$home_slider.= '</a>';
					            }

					          $home_slider.= '</div>';
					        $home_slider.= '</div>';

					    $home_slider.= '</div>';

					$home_slider.= '</div>';
			    
				endwhile;

			      $home_slider.= '</div>';
			$home_slider.= '</section>';

			echo $home_slider;

	endif;
?>