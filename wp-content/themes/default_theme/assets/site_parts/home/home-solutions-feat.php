<?php 

	$home_sol_feat = "";
	$home_sol_feat.= '<section class="homeSolutionsFeat homeSec container">';
	    
		$home_sol_feat.= '<h2 class="homeSec__title title title--sm title--underline text-center text-uppercase">La soluzione anticaduta</h2>';

	    $home_sol_feat.= '<div class="homeSolutionsFeat__wrap">';
			$home_sol_feat.= '<div class="homeSolutionsFeat__img" style="background-image: url(/medavita-corporate/images/uploads/home/slider_lotion-concentree_img.jpg);"></div>';

		    $home_sol_feat.= '<div class="homeSolutionsFeat__cont">';
		        $home_sol_feat.= '<img class="homeSolutionsFeat__prod" src="/medavita-corporate/images/uploads/home/slider_lotion-concentree_feat.png" alt="Lotion concentrée">';
		        $home_sol_feat.= '<h3 class="homeSolutionsFeat__title title title--md">Lotion concentrée</h3>';
		        $home_sol_feat.= '<p class="homeSolutionsFeat__text text text-uppercase">Il nuovo ed esclusivo trattamento per la cura istantanea dei capelli danneggiati</p>';
		        $home_sol_feat.= '<a class="homeSolutionsFeat__btn btn btn--white btn--whiteOutline" href="#" title="scopri">Scopri</a>';
		    $home_sol_feat.= '</div>';

		$home_sol_feat.= '</div>';
	    
	$home_sol_feat.= '</section>';

	echo $home_sol_feat;
?>