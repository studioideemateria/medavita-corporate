<?php 

	$home_intro = "";
	$home_intro.= '<section class="homeIntro homeSec container">';
	    
	    $home_intro.= '<div class="homeIntro__wrap">';

		    $home_intro.= '<div class="homeIntro__cont">';
		        $home_intro.= '<h3 class="homeIntro__title title title--md text-uppercase">Scienza che controlla la natura. Con amore.</h3>';
		        $home_intro.= '<p class="homeIntro__text text text-uppercase">Il nuovo ed esclusivo trattamento per la cura istantanea dei capelli danneggiati</p>';
		        $home_intro.= '<a class="homeIntro__btn btn btn--white" href="#" title="scopri">Scopri</a>';
		    $home_intro.= '</div>';

		    $home_intro.= '<div class="homeIntro__imgWrap">';
			    $home_intro.= '<img class="homeIntro__img" src="/medavita-corporate/images/uploads/home/homeIntro_img_01.jpg" alt="Lotion concentrée">';
			    $home_intro.= '<img class="homeIntro__img" src="/medavita-corporate/images/uploads/home/homeIntro_img_02.jpg" alt="Lotion concentrée">';	
			$home_intro.= '</div>';

		$home_intro.= '</div>';
	    
	$home_intro.= '</section>';

	echo $home_intro;
?>