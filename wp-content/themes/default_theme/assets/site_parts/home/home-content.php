<?php 
	$feat = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

	$page_content = "";
	$page_content.= '<div class="commonPage commonPage--'.$post->post_type.'">';
		$page_content.= '<div class="commonPage__wrap container">';
			$page_content.= '<h1 class="commonPage__title title">'.$post->post_title.'</h1>';

			if(isset($feat) && $feat!="") $page_content.= '<img class="commonPage__feat lazy" data-src="'.$feat.'" alt="'.$post->post_title.'">';

				$page_content.= '<div class="commonPage__text text">'.apply_filters('the_content', $post->post_content).'</div>';
		$page_content.= '</div>';
	$page_content.= '</div>';

	echo $page_content;
?>