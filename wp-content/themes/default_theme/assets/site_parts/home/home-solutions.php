<?php 

	$home_sol = "";
	$home_sol.= '<section class="homeSolutions homeSec container">';
	    
		$home_sol.= '<h2 class="homeSec__title title title--sm title--underline text-center text-uppercase">Hair solutions</h2>';

	    $home_sol.= '<div class="homeSolutions__wrap" style="background-image: url(/medavita-corporate/images/uploads/home/homeSolutions_bg.jpg);">';

			$home_sol.= '<ul class="homeSolutions__list">';        
	            
	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Anticaduta</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Anticaduta Maschile</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento antiforfora</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento sebo-equilibrante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento calmante, tranquillante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento detossinante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento condizionante, idratante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Anticaduta</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Anticaduta Maschile</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento antiforfora</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento sebo-equilibrante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento calmante, tranquillante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento detossinante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';

	            $home_sol.= '<li class="homeSolutions__el">';
	              $home_sol.= '<div class="homeSolutions__elCont">';
	                $home_sol.= '<a href="#" title="">';
	                  $home_sol.= '<span class="homeSolutions__elName">Trattamento condizionante, idratante</span>';
	                $home_sol.= '</a>';
	              $home_sol.= '</div>';
	            $home_sol.= '</li>';


	        $home_sol.= '</ul>';

	        $home_sol.= '<div class="homeSolutions__text text text--md text-center">Trova il prodotto giusto per le tue esigenze.</div>';

		$home_sol.= '</div>';
	    
	$home_sol.= '</section>';

	echo $home_sol;
?>