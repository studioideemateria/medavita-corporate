<?php 
	global $social_network_list;
	if (isset($social_network_list['social_widget']) && $social_network_list['social_widget']!= "") {
		$social = "";
		$soc = $social_network_list['social_widget'];
		$social.= '<div class="social">';
			$social.= '<ul class="social__list">';
			foreach ($soc as $key => $value) {
				if ($value!="") {
					$social.= '<li>';
						$social.= '<a title="'.$value.'" href="'.$value.'" target="_blank" rel="noopener noreferrer">';
							$social.= file_get_contents(WP_TEMPLATE_PATH.'/images/svg/social-icons/'.$key.'.svg');
						$social.= '</a>';
					$social.= '</li>';
				}
			}
			$social.= '</ul>';
		$social.= '</div>';
	}
	return $social;
?>