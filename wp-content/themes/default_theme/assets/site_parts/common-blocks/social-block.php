<?php 

	$social_block = "";
	$social_block.= '<section class="socialBlock homeSec container">';
	    
		$social_block.= '<h2 class="homeSec__title title title--sm title--underline text-center text-uppercase">Segui medavita</h2>';

	    $social_block.= '<div class="socialBlock__wrap">';

	        $social_block.= include_once(WP_TEMPLATE_PATH.'/assets/site_parts/common-blocks/social-widget.php');

		$social_block.= '</div>';
	    
	$social_block.= '</section>';

	echo $social_block;
?>