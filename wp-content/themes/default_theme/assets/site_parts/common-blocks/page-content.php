<?php 
	$feat = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

	$page_content = "";
	$page_content.= '<div class="page__content commonPage commonPage--'.$post->post_type.'">';
		$page_content.= '<div class="commonPage__wrap">';
			$page_content.= '<h1 class="page__tit commonPage__title title">'.$post->post_title.'</h1>';

			$page_content.= '<div class="page__cont commonPage__text text">'.apply_filters('the_content', $post->post_content).'</div>';

		$page_content.= '</div>';
	$page_content.= '</div>';

	echo $page_content;
?>