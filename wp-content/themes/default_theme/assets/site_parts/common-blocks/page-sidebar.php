<?php 

	$page_sidebar = "";
	$page_sidebar.= '<aside class="page__sidebar col-md-3">';

		$page_sidebar.= '<h3 class="page__sidebarTitle text-uppercase">'.$post->post_title.'</h3>';
		$page_sidebar.= '<ul class="page__sidebarNav">';
			$page_sidebar.= '<li class="current_page_item">Page example 1</li>';
			$page_sidebar.= '<li>Page example 2</li>';
			$page_sidebar.= '<li>Page example 3</li>';
		$page_sidebar.= '</ul>';

	$page_sidebar.= '</aside>';
	echo $page_sidebar;
?>