<?php 

	$nl_block = "";
	$nl_block.= '<section class="nlBlock homeSec container">';
	    
		$nl_block.= '<h2 class="homeSec__title title title--sm title--underline text-center text-uppercase">Il tuo aggiornamento di bellezza</h2>';
		$nl_block.= '<p class="text text--sm text-center">Tutte le novità su prodotti, styling e tendenze moda</p>';

	    $nl_block.= '<div class="nlBlock__wrap">';

	    	$nl_block.= '<form class="nlBlock__form" action="http://inmateria.net/medavita-statico/index-popup-coupon.html">
		        <div class="nlBlock__inputs">
		          <input name="email" type="email" id="newsletter" placeholder="Inserisci la tua email...">
		          <span class="privacy-acceptance">
		              <label class="check-styled">
		                <input type="checkbox" name="privacy-acceptance" value="1" aria-invalid="false">
		                <span class="check-styled__check"></span>
		                <span class="check-styled__label">Confermo di aver preso visione dell\'informativa sul <a href="#" class=" title="Privacy Policy">trattamento dei dati</a>.</span>
		              </label>
		          </span>
		        </div>
		        <div class="nlBlock__submitWrap">
		          <button class="btn btn--black" title="Iscriviti ora" type="submit">Iscriviti</button>
		        </div>
		      </form>';

		$nl_block.= '</div>';
	    
	$nl_block.= '</section>';

	echo $nl_block;
?>