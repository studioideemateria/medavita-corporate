<?php

//Includo file con le variabili globali
include_once get_template_directory().'/wp_master.php';

//Includo librerie di sistema
include_once get_template_directory().'/lib/init.php';

//Funzione che lancia la collect delle librerie di sistema
MM_wp_init();

add_action('wp_loaded', 'get_sidebar_configurazione_dati');
add_action('wp_loaded', 'get_sidebar_social_network');

?>