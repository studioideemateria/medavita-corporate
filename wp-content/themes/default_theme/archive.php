<?php

get_header();
$curr_obj = get_queried_object();

?>

<div class="arch container">
	
	<!--div class="text grey"><?php echo do_shortcode('[wpseo_breadcrumb]') ?></div-->
	<?php include_once(WP_TEMPLATE_PATH.'/assets/site_parts/archive/archive-news.php'); ?>
	
	<!--aside class="arch__side sidebar">
		<?php // include_once(WP_TEMPLATE_PATH.'/assets/site_parts/page/archive-sidebar.php') ?>
	</aside-->
</div>

<?php 
get_footer(); 

//Includo chiusura documento
include_once(WP_TEMPLATE_PATH.'/assets/site_parts/__HTML-doc-close.php');
?>