Thank you for buying our extension at ModernModules.com

Combine the powerful e-commerce solution Magento 2 with the excellent CMS capabilities of WordPress.
The Magento 2 WordPress Integration Plugin integrates Magento 2 with WordPress, so users will have an unified user experience.
Share session/cart data, navigation menus, header, footer, products, layout elements and static blocks by using shortcodes or functions.

This plugin is not meant to replace Magento 2, instead it will allow you to create a seamless user experience for your visitors by integrating the design of Magento and WordPress.

If you will need support, please contact us at https://modernmodules.com/support with description of the issue(s) and access information.

**Version 1.3.1**

FEATURES
===========

- Include Magento 2 blocks in your WordPress theme
- Use shortcodes to include products, cart, layout blocks, and static blocks in the WordPress editor ("Add Magento2 Shortcode" button)
- Share session and cart data between Magento 2 and WordPress
- Include product information in your WordPress editor
- Seamlessly integrate your Magento 2 and WordPress theme
- Display cart/mini cart with session data
- 4 Widgets to display static blocks, product slider, single product or cart
- Slider and Single Product templates can be overridden in current theme
- Auto adding possibility of header and footer from your Magento 2 (only if your theme is using HTML5 approach)

INSTALLATION
===========

1. Purchase and download the WordPress plugin
2. Upload the contents of the ZIP to /wp-content/plugins/
3. Enable the plugin in the plugins settings page
4. Apply the following patch to avoid conflicts between WordPress and Magento 2:
5. Set the right settings in Settings -> Magento 2 Integration

-> Locate {WORDPRESS_ROOT}/wp-includes/l10n.php
WordPress __() function is used for translation but is in conflict with Magento 2. Therefore please find this function at around line 202 and 

REPLACE

```php
function __( $text, $domain = 'default' ) {
	return translate( $text, $domain );
}
```

WITH

```php
function __( $text, $domain = 'default' ) {
    if ( defined( 'M2I_MAGE_DIR' ) && class_exists( 'M2I_External' ) && M2I_External::$needs_mage_translate ) {
        return M2I_External::translate( func_get_args() );
    } else {
        return translate( $text, $domain );
    }
}
```

## Session sharing additional setup

If you want to be sure about session sharing (especially when a user is logged in) between Magento and WordPress, you need to add the next code to Magento index.php file before Bootstrap line:

```php
if ( isset( $_COOKIE['PHPSESSID'] ) ) {
	/* To share logged in user session with WP frontend */
	setcookie( 'PHPSESSID', $_COOKIE['PHPSESSID'], time() + 3600, '/', '.local.wordpress', 0 );
	/* To share logged in user session with WP backend */
	setcookie( 'PHPSESSID', $_COOKIE['PHPSESSID'], time() + 3600, '/wp-admin', '.local.wordpress', 0 );
}
```

Where you have to replace *local.wordpress* domain with your one and */* with your WordPress path (path relative to the url). wp-admin part is optional. 

USING THE PLUGIN
=============
    
You can use the following functions in your theme files:

* Get HTML content (as string) of the header: m2i_get_header()
* Get HTML content (as string) of the footer: m2i_get_footer()
* Get HTML content (as string) for CSS files: m2i_get_links_css_tags()
* Get HTML content (as string) for JS files: m2i_get_scripts_from_head() or m2i_get_scripts_from_body()
* Get HTML content (as string) of the parts (elements) of the site by CSS selector: m2i_get_els_by_css_selector($css_selector)
* Get HTML content (as string) of the part (element) of the site by CSS selector: m2i_get_el_by_css_selector($css_selector)
* Get Product Data as array: m2i_get_product($sku_or_id, $by_id = false)
* Get Categories List as array: m2i_get_cat_collection($is_active = true, $level = false, $sort_by = false, $page_size = false)
* Get Store Views List as array: m2i_get_stores()
* Get Store Views List as array: m2i_get_blocks()
* Get Customer Info as array: m2i_get_customer_info()
* Get HTML content (as string) of the CMS block : m2i_get_cms_block($attrs), where $attrs['name'] - name of the CMS block in your Magento2
* Get HTML content (as string) of the Cart: m2i_get_cart($attrs), if in $attrs is set key 'hide_if_empty' it will be hidden if empty.



You can use the following shortcodes in your WordPress editor:

* Show Block: [m2i_cms_block name="name_in_layout"]
* Show Slider: [m2i_category_slider dom_id="unique-id" cats_ids="categories_ids_separated_by_comas" qty="quantity_of_products" margin="in_number"]
* Show Single Product: [m2i_product_block id="product id"] or [m2i_product_block sku="product sku"]
* Show Cart: [m2i_cart] or [m2i_cart hide_if_empty]

You can override Slider and Single Product templates in your current theme directory
CURRENT_THEME_DIR/m2i-templates/product-view.php or slider.php

