## TODO LIST

- [x] Upgrade to ES6
- [x] Add WP Base into his own repository
- [x] Move SmoothState from actionFooter.php to a separate js file
- [x] Split function js in multiple files
- [x] Task to autocomplete base url in whole directory
- [x] Scss rewrite to avoid same import in every file
- [x] Remove base wp themes